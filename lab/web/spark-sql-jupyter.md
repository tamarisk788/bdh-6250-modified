---
layout: post
title: Spark Sql (Jupyter)
categories: [section]
navigation:
    section: [3, 3]
---

```scala
%AddDeps com.databricks spark-csv_2.11 1.5.0 --transitive
```

    Marking com.databricks:spark-csv_2.11:1.5.0 for download
    Preparing to fetch from:
    -> file:/var/folders/qb/gs4c5dz139g_9tnzwzhcbf64xdz4jh/T/toree_add_deps8460235008841579923/
    -> https://repo1.maven.org/maven2
    -> New file at /var/folders/qb/gs4c5dz139g_9tnzwzhcbf64xdz4jh/T/toree_add_deps8460235008841579923/https/repo1.maven.org/maven2/org/apache/commons/commons-csv/1.1/commons-csv-1.1.jar
    -> New file at /var/folders/qb/gs4c5dz139g_9tnzwzhcbf64xdz4jh/T/toree_add_deps8460235008841579923/https/repo1.maven.org/maven2/com/univocity/univocity-parsers/1.5.1/univocity-parsers-1.5.1.jar
    -> New file at /var/folders/qb/gs4c5dz139g_9tnzwzhcbf64xdz4jh/T/toree_add_deps8460235008841579923/https/repo1.maven.org/maven2/com/databricks/spark-csv_2.11/1.5.0/spark-csv_2.11-1.5.0.jar



```scala
import org.apache.spark.sql.SQLContext
val sqlContext = new SQLContext(sc)
import sqlContext.implicits._
```


```scala
val patientEvents = (sqlContext.load("input/", "com.databricks.spark.csv")
    .toDF("patientId", "eventId", "date", "rawvalue")
    .withColumn("value", 'rawvalue.cast("Double")))
```


```scala
patientEvents.registerTempTable("events")
```


```scala
sqlContext.sql("""
        select patientId, eventId, count(*) count from events
        where eventId like 'DIAG%' group by patientId, eventId
        order by count desc
    """).collect
```




    Array([00291F39917544B1,DIAG28521,16], [00291F39917544B1,DIAG58881,16], [00291F39917544B1,DIAG2809,13], [00824B6D595BAFB8,DIAG4019,11], [0085B4F55FFA358D,DIAG28521,9], [0124E58C3460D3F8,DIAG4019,8], [6A8F2B98C1F6F5DA,DIAG58881,8], [019E4729585EF3DD,DIAG4019,8], [2D5D3D5F03C8C176,DIAG4019,8], [01BE015FAF3D32D1,DIAG4019,7], [019E4729585EF3DD,DIAG25000,7], [01A999551906C787,DIAG4019,7], [020F7FEDB230CEAB,DIAG4019,6], [014BAF52258B1C69,DIAG4019,6], [01CE3095AFF436D3,DIAG42731,6], [01434D73376AD345,DIAGV5869,6], [01A999551906C787,DIAG42731,6], [335D0E4618C2C824,DIAG4019,6], [6A8F2B98C1F6F5DA,DIAG2809,6], [0085B4F55FFA358D,DIAG4019,6], [011BF695C016AB71,DIAG4019,6], [01D90330AF8E123D,DIAG4019,6], [49200ED048897DB5,DIAGV5861,6], [008C0A4FEB2D5501,DIAG272...




```scala
(patientEvents
    .filter($"eventId".startsWith("DIAG"))
    .groupBy("patientId", "eventId")
    .count
    .orderBy($"count".desc).show)
```

    +----------------+---------+-----+
    |       patientId|  eventId|count|
    +----------------+---------+-----+
    |00291F39917544B1|DIAG28521|   16|
    |00291F39917544B1|DIAG58881|   16|
    |00291F39917544B1| DIAG2809|   13|
    |00824B6D595BAFB8| DIAG4019|   11|
    |0085B4F55FFA358D|DIAG28521|    9|
    |6A8F2B98C1F6F5DA|DIAG58881|    8|
    |019E4729585EF3DD| DIAG4019|    8|
    |0124E58C3460D3F8| DIAG4019|    8|
    |2D5D3D5F03C8C176| DIAG4019|    8|
    |01A999551906C787| DIAG4019|    7|
    |019E4729585EF3DD|DIAG25000|    7|
    |01BE015FAF3D32D1| DIAG4019|    7|
    |01D90330AF8E123D| DIAG4019|    6|
    |014BAF52258B1C69| DIAG4019|    6|
    |0085B4F55FFA358D| DIAG4019|    6|
    |01434D73376AD345|DIAGV5869|    6|
    |335D0E4618C2C824| DIAG4019|    6|
    |020F7FEDB230CEAB| DIAG4019|    6|
    |01CE3095AFF436D3|DIAG42731|    6|
    |01A999551906C787|DIAG42731|    6|
    +----------------+---------+-----+
    only showing top 20 rows




```scala
(patientEvents
    .filter($"eventId".startsWith("DIAG"))
    .groupBy("patientId", "eventId")
    .count
    .orderBy($"count".desc)
    .save("aggregated.json", "json"))
```


```scala
(patientEvents.filter($"eventId".startsWith("DIAG"))
    .groupBy("patientId", "eventId")
    .count.orderBy($"count".desc)
    .save("aggregated.csv"))
```


```scala
 sqlContext.udf.register("getEventType", (s: String) => s match {
    case diagnostics if diagnostics.startsWith("DIAG") => "diagnostics"
    case "PAYMENT" => "payment"
    case drug if drug.startsWith("DRUG") => "drug"
    case procedure if procedure.startsWith("PROC") => "procedure"
    case "heartfailure" => "heart failure"
    case _ => "unknown"
})
```




    UserDefinedFunction(<function1>,StringType,List(StringType))




```scala
sqlContext.sql("""select getEventType(eventId) type, count(*) count
    from events group by getEventType(eventId)
    order by count desc
    """).show
```

    +-------------+-----+
    |         type|count|
    +-------------+-----+
    |         drug|16251|
    |  diagnostics|10820|
    |      payment| 3259|
    |    procedure|  514|
    |heart failure|  300|
    +-------------+-----+
