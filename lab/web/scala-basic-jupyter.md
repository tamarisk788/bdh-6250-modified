---
layout: post
title: Scala Basics (Jupyter)
categories: [section]
navigation:
    section: [3, 1]
---

```scala
val myInt = 1 + 1
myInt
```




    2




```scala
myInt = 3
```




    Name: Compile Error
    Message: <console>:20: error: reassignment to val
             myInt = 3
                   ^
    StackTrace:




```scala
val myDouble: Double = 3
myDouble
```




    3.0




```scala
val myList: List[String] = List("this", "is", "a", "list", "of", "string")
myList
```




    List(this, is, a, list, of, string)




```scala
val myTuple: (Double, Double) = (1.0, 2.0)
myTuple
```




    (1.0,2.0)




```scala
def triple(x: Int): Int = {
    x*3
}
triple(2)
```




    6




```scala
def triple(x: Int) = x*3
triple(2)
```




    6




```scala
val myString = "Hello Healthcare"
myString
```




    Hello Healthcare




```scala
myString.lastIndexOf("Healthcare")
```




    6




```scala
val myInt = 2
myInt.toString
```




    2




```scala
val increaseOne = (x: Int) => x + 1
increaseOne(3)
```




    4




```scala
myList.foreach{item: String => println(item)}
```

    this
    is
    a
    list
    of
    string



```scala
myList.foreach(println(_))
```

    this
    is
    a
    list
    of
    string



```scala
myList.foreach(println)
```

    this
    is
    a
    list
    of
    string



```scala
val payments = List(1, 2, 3, 4, 5, 6)
payments
```




    List(1, 2, 3, 4, 5, 6)




```scala
payments.reduce(_ + _)
```




    21




```scala
payments.reduce((a, b) => a+b)
```




    21




```scala
var totalPayment = 0
```


```scala
for (payment <- payments) {
    totalPayment += payment
}
```


```scala
totalPayment
```




    21




```scala
class Patient(val name: String, val id: Int)
```


```scala
val patient = new Patient("Bob", 1)
patient
```




    Patient(Bob,1)




```scala
patient.name
```




    Bob




```scala
case class Patient(val name: String, val id: Int)
```


```scala
val payment:Any = 21
payment match {
    case p: String => println("payment is a String")
    case p: Int if p > 30 => println("payment > 30")
    case p: Int if p == 0 => println("zero payment")
    case _ => println("otherwise")
}
```

    otherwise



```scala
val p = new Patient("Abc", 1)
p match {case Patient("Abc", id) => println(s"matching id is $id")}
```

    matching id is 1
