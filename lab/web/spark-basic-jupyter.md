---
layout: post
title: Spark Basics (Jupyter)
categories: [section]
navigation:
    section: [3, 2]
---

```scala
sc
```




    org.apache.spark.SparkContext@7c5a1d26




```scala
val data = Array(1, 2, 3, 4, 5)
data
```




    Array(1, 2, 3, 4, 5)




```scala
val distData = sc.parallelize(data)
distData
```




    ParallelCollectionRDD[1] at parallelize at <console>:22




```scala
val lines = sc.textFile("input/case.csv")
lines
```




    input/case.csv MapPartitionsRDD[3] at textFile at <console>:20




```scala
lines.count()
```




    14046




```scala
lines.take(5)
```




    Array(00013D2EFD8E45D1,DIAG78820,1166,1.0, 00013D2EFD8E45D1,DIAGV4501,1166,1.0, 00013D2EFD8E45D1,heartfailure,1166,1.0, 00013D2EFD8E45D1,DIAG2720,1166,1.0, 00013D2EFD8E45D1,DIAG4019,1166,1.0)




```scala
lines.take(5).foreach(println)
```

    00013D2EFD8E45D1,DIAG78820,1166,1.0
    00013D2EFD8E45D1,DIAGV4501,1166,1.0
    00013D2EFD8E45D1,heartfailure,1166,1.0
    00013D2EFD8E45D1,DIAG2720,1166,1.0
    00013D2EFD8E45D1,DIAG4019,1166,1.0



```scala
lines.map(line => line.split(",")(0))
```




    MapPartitionsRDD[4] at map at <console>:23




```scala
lines.map{line =>
  val s = line.split(",")
  (s(0), s(1))
}
```




    MapPartitionsRDD[5] at map at <console>:23




```scala
lines.filter(line => line.contains("00013D2EFD8E45D1")).count()
```




    200




```scala
lines.map(line => line.split(",")(0)).distinct().count()
```




    100




```scala
val patientIdEventPair = lines.map{line =>
  val patientId = line.split(",")(0)
  (patientId, line)
}
val groupedPatientData = patientIdEventPair.groupByKey
groupedPatientData.take(1)
```




    Array((0102353632C5E0D0,CompactBuffer(0102353632C5E0D0,DIAG29181,562,1.0, 0102353632C5E0D0,DIAG29212,562,1.0, 0102353632C5E0D0,DIAG34590,562,1.0, 0102353632C5E0D0,DIAG30000,562,1.0, 0102353632C5E0D0,DIAG2920,562,1.0, 0102353632C5E0D0,DIAG412,562,1.0, 0102353632C5E0D0,DIAG28800,562,1.0, 0102353632C5E0D0,DIAG30391,562,1.0, 0102353632C5E0D0,DIAGRG894,562,1.0, 0102353632C5E0D0,PAYMENT,562,6000.0, 0102353632C5E0D0,DIAG5781,570,1.0, 0102353632C5E0D0,DIAG53010,570,1.0, 0102353632C5E0D0,DIAGE8490,570,1.0, 0102353632C5E0D0,DIAG27651,570,1.0, 0102353632C5E0D0,DIAG78559,570,1.0, 0102353632C5E0D0,DIAG56210,570,1.0, 0102353632C5E0D0,DIAG5856,570,1.0, 0102353632C5E0D0,heartfailure,570,1.0, 0102353632C5E0D0,DIAG5070,570,1.0, 0102353632C5E0D0,DIAGRG346,570,1.0...




```scala
val payment_events = lines.filter(line => line.contains("PAYMENT"))
val payments = payment_events.map{ x =>
                                   val s = x.split(",")
                                   (s(0), s(3).toFloat)
                                 }
val paymentPerPatient = payments.reduceByKey(_+_)
```


```scala
paymentPerPatient.sortBy(_._2, false).take(3).foreach(println)
```

    (0085B4F55FFA358D,139880.0)
    (019E4729585EF3DD,108980.0)
    (01AC552BE839AB2B,108530.0)



```scala
val payment_values = paymentPerPatient.map(payment => payment._2).cache()
```


```scala
payment_values.max()
```




    139880.0




```scala
payment_values.min()
```




    3910.0




```scala
payment_values.sum()
```




    2842480.0




```scala
payment_values.mean()
```




    28424.800000000003




```scala
 payment_values.stdev()
```




    26337.091771112464




```scala
val linesControl = sc.textFile("input/control.csv")
lines.union(linesControl).count()
```




    31144
