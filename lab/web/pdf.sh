pandoc upr.md -f markdown -t latex -o 1.pdf
pandoc hdfs-basic.md -f markdown -t latex -o 2.pdf
pandoc scala-basic.md -f markdown -t latex -o 3.pdf
pandoc spark-basic.md -f markdown -t latex -o 4.pdf
pandoc spark-sql.md -f markdown -t latex -o 5.pdf
pandoc spark-application.md -f markdown -t latex -o 6.pdf
pandoc spark-mllib.md -f markdown -t latex -o 7.pdf
pandoc spark-graphx.md -f markdown -t latex -o 8.pdf
pandoc data.md -f markdown -t latex -o 9.pdf

gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=training.pdf 1.pdf 2.pdf 3.pdf 4.pdf 5.pdf 6.pdf 7.pdf 8.pdf 9.pdf
