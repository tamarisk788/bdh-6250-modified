#!/bin/bash
# run_all.sh
#
# Grades all submissions located in the current folder
# - Should contain student archives or already extracted folders
# - The process copies the respective “src/main” folder for each student and grades. 
# - All other folders are not used from student submission and we use the standard ones in bitbucket. 
# - This auto graders grades - Jaccards, Random Walk and Graph Edges/Vertices Count

# CONFIGURATION (change this to match your setup)
GRADING_DIR=/autograder/source

# Create new log file
RECORD_FILE=/autograder/source/grading/MASTER_RECORD.log
echo "BEGIN MASTER LOG..." > $RECORD_FILE

# Loop through and grade every student directory
for STUDENT_DIR in /autograder/source/grading/*/; do
    echo "Grading: ${STUDENT_DIR}"
    STUDENT_GTID=$(echo "$STUDENT_DIR" | cut -d '-' -f 1)
    STUDENT_USERNAME=$(echo "$STUDENT_DIR" | cut -d '-' -f 2)
    pushd $STUDENT_DIR
        # Copy test and data folder into students folder
        cp -rf $GRADING_DIR/code/src/test $STUDENT_DIR\src
        cp -rf $GRADING_DIR/code/data $STUDENT_DIR\data
        $GRADING_DIR/code/sbt/sbt -Dsbt.log.noformat=true test | tee output.log
        scala $GRADING_DIR/grading/parser.scala $RECORD_FILE $STUDENT_GTID $STUDENT_USERNAME
    popd
done

exit 0



