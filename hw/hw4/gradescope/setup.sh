#!/usr/bin/env bash

# install java
apt-get update
apt-get install -y openjdk-8-jdk

# install scala
wget https://downloads.lightbend.com/scala/2.11.8/scala-2.11.8.deb
dpkg -i scala-2.11.8.deb

# install sbt
wget --progress=dot:mega https://github.com/sbt/sbt/releases/download/v1.1.0/sbt-1.1.0.tgz
tar -xzf sbt-1.1.0.tgz
rm -rf sbt-1.1.0.tgz 
mv sbt /usr/lib/ 
ln -sf /usr/lib/sbt/bin/sbt /usr/bin/