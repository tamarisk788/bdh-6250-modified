package edu.gatech.cse6250.graphconstruct

import edu.gatech.cse6250.model._
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD

object GraphLoader {
  /**
   * Generate Bipartite Graph using RDDs
   *
   * @input: RDDs for Patient, LabResult, Medication, and Diagnostic
   * @return: Constructed Graph
   *
   */
  var vertexPatient: RDD[(VertexId, VertexProperty)] = _
  var vertexLabResult: RDD[(VertexId, VertexProperty)] = _
  var vertexMedication: RDD[(VertexId, VertexProperty)] = _
  var vertexDiagnostic: RDD[(VertexId, VertexProperty)] = _

  var edgeLabResult: RDD[Edge[EdgeProperty]] = _
  var edgeMedication: RDD[Edge[EdgeProperty]] = _
  var edgeDiagnostic: RDD[Edge[EdgeProperty]] = _

  def load(patients: RDD[PatientProperty], labResults: RDD[LabResult],
    medications: RDD[Medication], diagnostics: RDD[Diagnostic]): Graph[VertexProperty, EdgeProperty] = {

    // create patient vertices
    vertexPatient = patients
      .map(patient => (patient.patientID.toLong, patient.asInstanceOf[VertexProperty]))
    println(s"Patient vertices: ${vertexPatient.count}")

    // create unique lab vertices
    val labs = labResults.map(lab => lab.labName).distinct
    vertexLabResult = labs
      .map(testName => (("LAB: " + testName).hashCode.toLong, LabResultProperty(testName).asInstanceOf[VertexProperty]))
    println(s"Lab vertices: ${vertexLabResult.count}")

    // create unique medication vertices
    val meds = medications.map(med => med.medicine).distinct
    vertexMedication = meds
      .map(medicine => (("MED: " + medicine).hashCode.toLong, MedicationProperty(medicine).asInstanceOf[VertexProperty]))
    println(s"Medication vertices: ${vertexMedication.count}")

    // create unique diagnostic vertices
    val diags = diagnostics.map(diag => diag.icd9code).distinct
    vertexDiagnostic = diags
      .map(icd9code => (("DIAG: " + icd9code).hashCode.toLong, DiagnosticProperty(icd9code).asInstanceOf[VertexProperty]))
    println(s"Diagnostic vertices: ${vertexDiagnostic.count}")

    // create lab edges
    val edgeLabResultForward: RDD[Edge[EdgeProperty]] = labResults
      .map(lab => ((lab.patientID, lab.labName), (lab.date, lab.value, lab.recordID)))
      .reduceByKey((v1, v2) => {
        // for each key of patienID/labName we only choose the one with the greatest date (._1) or
        // if the dates are equal (because they are neither > or <) we choose the one with greater
        // recordID (._3) which can never be equal
        if (v1._1 > v2._1) v1 else if (v1._1 < v2._1) v2 else if (v1._3 > v2._3) v1 else v2
      })
      .map({
        case ((patientID, labName), (date, value, recordID)) =>
          Edge(
            patientID.toLong,
            ("LAB: " + labName).hashCode.toLong,
            PatientLabEdgeProperty(LabResult(patientID, date, labName, value, recordID)).asInstanceOf[EdgeProperty])
      })
    val edgeLabResultBackward: RDD[Edge[EdgeProperty]] = edgeLabResultForward
      .map({ case Edge(src, dst, prop) => Edge(dst, src, prop) })
    edgeLabResult = edgeLabResultForward
      .union(edgeLabResultBackward)

    // create medication edges
    val edgeMedicationForward: RDD[Edge[EdgeProperty]] = medications
      .map(med => ((med.patientID, med.medicine), (med.date, med.recordID)))
      .reduceByKey((v1, v2) => {
        if (v1._1 > v2._1) v1 else if (v1._1 < v2._1) v2 else if (v1._2 > v2._2) v1 else v2
      })
      .map({
        case ((patientID, medicine), (date, recordID)) =>
          Edge(
            patientID.toLong,
            ("MED: " + medicine).hashCode.toLong,
            PatientMedicationEdgeProperty(Medication(patientID, date, medicine, recordID)).asInstanceOf[EdgeProperty])
      })
    val edgeMedicationBackward: RDD[Edge[EdgeProperty]] = edgeMedicationForward
      .map({ case Edge(src, dst, prop) => Edge(dst, src, prop) })
    edgeMedication = edgeMedicationForward
      .union(edgeMedicationBackward)

    // create diagnostic edges
    val edgeDiagnosticForward: RDD[Edge[EdgeProperty]] = diagnostics
      .map(diag => ((diag.patientID, diag.icd9code), (diag.date, diag.sequence, diag.recordID)))
      .reduceByKey((v1, v2) => {
        if (v1._1 > v2._1) v1 else if (v1._1 < v2._1) v2 else if (v1._3 > v2._3) v1 else v2
      })
      .map({
        case ((patientID, icd9code), (date, sequence, recordID)) =>
          Edge(
            patientID.toLong,
            ("DIAG: " + icd9code).hashCode.toLong,
            PatientDiagnosticEdgeProperty(Diagnostic(patientID, date, icd9code, sequence, recordID)).asInstanceOf[EdgeProperty])
      })
    val edgeDiagnosticBackward: RDD[Edge[EdgeProperty]] = edgeDiagnosticForward
      .map({ case Edge(src, dst, prop) => Edge(dst, src, prop) })
    edgeDiagnostic = edgeDiagnosticForward
      .union(edgeDiagnosticBackward)

    // Combining for vertices and edges
    val vertexAll: RDD[(VertexId, VertexProperty)] = vertexPatient
      .union(vertexLabResult)
      .union(vertexMedication)
      .union(vertexDiagnostic)
    println(s"VertexAll count: ${vertexAll.count}")

    val edgeAll: RDD[Edge[EdgeProperty]] = edgeLabResult
      .union(edgeMedication)
      .union(edgeDiagnostic)

    // Making Graph
    val graph: Graph[VertexProperty, EdgeProperty] = Graph[VertexProperty, EdgeProperty](vertexAll, edgeAll)

    println(s"Num Vertices: ${graph.vertices.count()}") // 2,845
    println(s"Num Edges: ${graph.edges.count()}") // 119,676

    graph
  }
}
