package edu.gatech.cse6250.model

case class LabResult(patientID: String, date: Int, labName: String, value: String, recordID: Int)

case class Diagnostic(patientID: String, date: Int, icd9code: String, sequence: Int, recordID: Int)

case class Medication(patientID: String, date: Int, medicine: String, recordID: Int)

abstract class VertexProperty

case class PatientProperty(patientID: String, sex: String, dob: String, dod: String) extends VertexProperty

case class LabResultProperty(testName: String) extends VertexProperty

case class DiagnosticProperty(icd9code: String) extends VertexProperty

case class MedicationProperty(medicine: String) extends VertexProperty

abstract class EdgeProperty

case class SampleEdgeProperty(name: String = "Sample") extends EdgeProperty

case class PatientLabEdgeProperty(labResult: LabResult) extends EdgeProperty

case class PatientDiagnosticEdgeProperty(diagnostic: Diagnostic) extends EdgeProperty

case class PatientMedicationEdgeProperty(medication: Medication) extends EdgeProperty

