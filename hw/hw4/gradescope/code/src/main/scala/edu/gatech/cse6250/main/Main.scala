package edu.gatech.cse6250.main

import edu.gatech.cse6250.clustering.PowerIterationClustering
import edu.gatech.cse6250.graphconstruct.GraphLoader
import edu.gatech.cse6250.helper.SparkHelper
import edu.gatech.cse6250.jaccard.Jaccard
import edu.gatech.cse6250.model.{ PatientProperty, _ }
import edu.gatech.cse6250.randomwalk.RandomWalk
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

object Main {
  def author: String = {
    "tb34" // replace tb34 with your Georgia Tech username
  }

  def main(args: Array[String]) {
    import org.apache.log4j.{ Level, Logger }

    Logger.getLogger("org").setLevel(Level.WARN)
    Logger.getLogger("akka").setLevel(Level.WARN)
    Logger.getLogger("org.apache.spark.SparkContext").setLevel(Level.WARN)

    val spark = SparkHelper.spark
    val sc = spark.sparkContext
    //  val sqlContext = spark.sqlContext

    // initialize loading of data
    val (patient, medication, labResult, diagnostic) = loadRddRawData(spark)
    val patientGraph = GraphLoader.load(patient, labResult, medication, diagnostic)
    val numPatients = patientGraph.vertices.filter(_._2.isInstanceOf[PatientProperty]).count()
    println(s"numPatients in the Graph: $numPatients")

    println(Jaccard.jaccardSimilarityOneVsAll(patientGraph, 9))
    println(RandomWalk.randomWalkOneVsAll(patientGraph, 9))

    val similarities = Jaccard.jaccardSimilarityAllPatients(patientGraph)
    val PICLabels = PowerIterationClustering.runPIC(similarities)

    spark.stop()
  }

  def loadRddRawData(spark: SparkSession): (RDD[PatientProperty], RDD[Medication], RDD[LabResult], RDD[Diagnostic]) = {
    import spark.implicits._
    val sqlContext = spark.sqlContext

    var patient_data = spark.read.format("csv")
      .option("header", "true")
      .load("data/PATIENT.csv")
      .select("patient_id", "sex", "dob", "dod")
    // replace null values with empty string for dod
    patient_data = patient_data.na.fill("", Array("dod"))
    // drop any rows that contain null values
    patient_data = patient_data.na.drop()
    // drop duplicates
    patient_data = patient_data.dropDuplicates()
    // map to PatientProperty (998)
    val patient = patient_data.map(r => PatientProperty(r(0).toString, r(1).toString,
      r(2).toString, r(3).toString))
    println(s"Total patient rows: ${patient_data.count}") // 978

    var medication_data = spark.read.format("csv")
      .option("header", "true")
      .option("inferSchema", "false")
      .load("data/MEDICATION.csv")
      .select("patient_id", "date", "med_name", "record_id")
    // drop any rows with missing values
    medication_data = medication_data.na.drop()
    // drop duplicates
    medication_data = medication_data.dropDuplicates()
    // map to Medication (7829)
    val medication = medication_data.map(r => Medication(r(0).toString, r(1).toString.toInt,
      r(2).toString, r(3).toString.toInt))
    println(s"Total medication rows: ${medication_data.count}") // 154184

    var lab_data = spark.read.format("csv")
      .option("header", "true")
      .load("data/LAB.csv")
      .select(col = "patient_id", cols = "date", "lab_name", "value", "record_id")
    // drop any rows with missing values
    lab_data = lab_data.na.drop()
    // drop duplicates
    lab_data = lab_data.dropDuplicates()
    // map to LabResult (521821)
    val labResult = lab_data.map(r => LabResult(r(0).toString, r(1).toString.toInt, r(2).toString,
      r(3).toString, r(4).toString.toInt))
    println(s"Total lab rows: ${labResult.count}") // 547683

    var diagnostic_data = spark.read.format("csv")
      .option("header", "true")
      .load("data/DIAGNOSTIC.csv")
      .select("patient_id", "date", "code", "sequence", "record_id")
    // drop any rows with missing values
    diagnostic_data = diagnostic_data.na.drop()
    // drop duplicates
    diagnostic_data = diagnostic_data.dropDuplicates()
    // map to Diagnostic (9478)
    val diagnostic = diagnostic_data.map(r => Diagnostic(r(0).toString, r(1).toString.toInt, r(2).toString,
      r(3).toString.toInt, r(4).toString.toInt))
    println(s"Total diagnostic rows: ${diagnostic.count}") // 9478

    // convert to RDD and return
    (patient.rdd, medication.rdd, labResult.rdd, diagnostic.rdd)

  }

}
