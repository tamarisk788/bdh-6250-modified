package edu.gatech.cse6250.randomwalk

import edu.gatech.cse6250.model.{ PatientProperty, EdgeProperty, VertexProperty }
import org.apache.spark.graphx._

object RandomWalk {

  def randomWalkOneVsAll(graph: Graph[VertexProperty, EdgeProperty], patientID: Long, numIter: Int = 100, alpha: Double = 0.15): List[Long] = {
    val bcStartIndex = graph.vertices.sparkContext.broadcast(patientID)
    var rankGraph: Graph[Double, Double] = graph
      // Associate the degree with each vertex
      .outerJoinVertices(graph.outDegrees) { (vid, vdata, deg) => deg.getOrElse(0) }
      // Set the weight on the edges based on the degree
      .mapTriplets(e => 1.0 / e.srcAttr, TripletFields.Src)
      // Set the vertex attributes to the initial pagerank values
      .mapVertices { (id, attr) =>
        if (id == bcStartIndex.value) 1.0 else 0.0
      }
    var iteration = 0
    var prevRankGraph: Graph[Double, Double] = null
    while (iteration < numIter) {
      print(s"Starting iteration: $iteration")
      rankGraph.cache()

      // Compute the outgoing rank contributions of each vertex, perform local preaggregation, and
      // do the final aggregation at the receiving vertices. Requires a shuffle for aggregation.
      val rankUpdates = rankGraph.aggregateMessages[Double](
        ctx => ctx.sendToDst(ctx.srcAttr * ctx.attr), _ + _, TripletFields.Src)

      // Apply the final rank updates to get the new ranks, using join to preserve ranks of vertices
      // that didn't receive a message. Requires a shuffle for broadcasting updated ranks to the
      // edge partitions.
      prevRankGraph = rankGraph
      rankGraph = rankGraph.joinVertices(rankUpdates) {
        (id, oldRank, msgSum) =>
          {
            if (id == bcStartIndex.value) alpha + (1.0 - alpha) * msgSum else (1.0 - alpha) * msgSum
          }
      }.cache()

      rankGraph.edges.foreachPartition(x => {}) // also materializes rankGraph.vertices
      //logInfo(s"PageRank finished iteration $iteration.")
      prevRankGraph.vertices.unpersist(false)
      prevRankGraph.edges.unpersist(false)

      iteration += 1
    }

    val richRankGraph = rankGraph.outerJoinVertices(graph.vertices) {
      (id, rank, property) => (rank, property.orNull)
    }
    richRankGraph.vertices
      .filter(_._1 != bcStartIndex.value)
      .filter(_._2._2 != null)
      .filter(_._2._2.isInstanceOf[PatientProperty])
      .takeOrdered(10)(scala.Ordering.by(-_._2._1))
      .map(_._1)
      .collect { case id: Long => id }
      .toList
  }
}
