#! /usr/bin/env python

import unittest
import json

if __name__ == '__main__':
    in_file = '/autograder/source/grading/MASTER_RECORD.log'
    out_file = '/autograder/results/results.json'

    in_txt = open(in_file, "r")
    lines = in_txt.readlines()
    record = lines[0]
    all_rows = record.split(',')

    try:
        q1_score = float(all_rows[1])
        q1_comments = all_rows[2]
    except:
        q1_score = 0.0
        q1_comments = 'No Results Found for Q1'
    try:
        q2_score = float(all_rows[4])
        q2_comments = all_rows[5]
    except:
        q2_score = 0.0
        q2_comments = 'No Results Found for Q2'
    try:
        q3_score = float(all_rows[7])
        q3_comments = all_rows[8]
    except:
        q3_score = 0.0
        q3_comments = 'No Results Found for Q3'
    try:
        q4_1_score = float(all_rows[10])
        q4_1_comments = all_rows[11]
    except:
        q4_1_score = 0.0
        q4_1_comments = 'No Results Found for Q4.1'
    try:
        q4_2_score = float(all_rows[13])
        q4_2_comments = all_rows[14]
    except:
        q4_2_score = 0.0
        q4_2_comments = 'No Results Found for Q4.2'

    total_score = q1_score + q2_score + q3_score + q4_1_score + q4_2_score
    output_dict = {"score": total_score, 
                "output": "Homework 4 Autograder Test Results",
                "stdout_visibility": "visible",
                "tests": [
                    {
                        "score": q1_score,
                        "max_score": 25.0,
                        "number": "1",
                        "output": q1_comments,
                        "visibility": "visible"
                    }, 
                    {
                        "score": q2_score,
                        "max_score": 20.0,
                        "number": "2",
                        "output": q2_comments,
                        "visibility": "visible"
                    },
                    {
                        "score": q3_score,
                        "max_score": 30.0,
                        "number": "3",
                        "output": q3_comments,
                        "visibility": "visible"
                    },
                    {
                        "score": q4_1_score,
                        "max_score": 10.0,
                        "number": "4.1",
                        "output": q4_1_comments,
                        "visibility": "visible"
                    },
                    {
                        "score": q4_2_score,
                        "max_score": 10.0,
                        "number": "4.2",
                        "output": q4_2_comments,
                        "visibility": "visible"
                    }
                ]}
    with open(out_file, 'w') as fp:
        json.dump(output_dict, fp)