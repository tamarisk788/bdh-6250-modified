package edu.gatech.cse6250.jaccard

import edu.gatech.cse6250.model._
import edu.gatech.cse6250.model.{ EdgeProperty, VertexProperty }
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD

import scala.collection.mutable.ListBuffer

object Jaccard {

  def jaccardSimilarityOneVsAll(graph: Graph[VertexProperty, EdgeProperty], patientID: Long): List[Long] = {
    val bcSrcVertexId = graph.edges.sparkContext.broadcast(patientID)
    val srcCharacteristic = graph.edges.filter { case Edge(srcId, dstId, edgeProperty) => srcId == bcSrcVertexId.value }.map { _.dstId }.collect().toSet
    val bcSrcCharacteristic = graph.edges.sparkContext.broadcast(srcCharacteristic)

    graph
      .collectNeighbors(EdgeDirection.Out)
      .filter {
        case (idx, neighbors) =>
          neighbors.length > 0 && !neighbors.exists { case (_, p) => p.isInstanceOf[PatientProperty] }
      }
      .mapValues { neighbors: Array[(VertexId, VertexProperty)] =>
        val characteristic = neighbors.map(_._1).toSet
        val intersect = bcSrcCharacteristic.value.intersect(characteristic)
        val union = bcSrcCharacteristic.value.union(characteristic)
        if (union.isEmpty) 0.0 else intersect.size.toDouble / union.size.toDouble
      }
      .filter(_._2 > 0.0)
      .takeOrdered(10)(scala.Ordering.by(-_._2))
      .map(_._1)
      .collect { case id: Long => id }
      .toList
  }

  def jaccardSimilarityAllPatients_old(graph: Graph[VertexProperty, EdgeProperty]): RDD[(Long, Long, Double)] = {
    println("Jaccad All starting")
    println(graph.numVertices)
    println(graph.numEdges)
    val patientIdAndCharacteristics = graph
      .collectNeighbors(EdgeDirection.Out)
      .filter {
        case (idx, neighbors) =>
          !neighbors.exists { case (_, p) => p.isInstanceOf[PatientProperty] }
      }
      .mapValues(neighbors => neighbors.map(_._1).toSet)

    println("Jaccard Second Part starting")
    patientIdAndCharacteristics
      .cartesian(patientIdAndCharacteristics)
      .filter { case (a, b) => a._1 > b._1 }
      .map {
        case (pa, pb) =>
          (pa._1, pb._1, jaccard(pa._2, pb._2))
      }
  }

  def jaccardSimilarityAllPatients(graph: Graph[VertexProperty, EdgeProperty]): RDD[(Long, Long, Double)] = {
    /**
     * Given a patient, med, diag, lab graph, calculate pairwise similarity between all
     * patients. Return a RDD of (patient-1-id, patient-2-id, similarity) where
     * patient-1-id < patient-2-id to avoid duplications
     */

    /** Remove this placeholder and implement your code */
    val allPatientIds = graph.vertices.filter(_._2.isInstanceOf[PatientProperty])
      .map(_._2.asInstanceOf[PatientProperty]).map(_.patientID.toLong).collect()
    var results = new ListBuffer[(Long, Long, Double)]()
    var stor = collection.mutable.Map[VertexId, Set[Long]]()

    graph.cache()
    for (i <- allPatientIds) {
      stor += i -> graph.edges.filter(x => x.dstId == i).map(_.srcId).collect().toSet
    }

    var idr = collection.mutable.Queue[VertexId]()
    var ids = collection.mutable.Queue[VertexId]()
    idr ++= allPatientIds
    ids ++= allPatientIds

    for (i <- idr) {
      ids.dequeue()
      var givenEdges = stor(i)
      for (j <- ids) {
        var jacc = jaccard(givenEdges, stor(j))
        results += ((i, j, jacc))
      }
    }

    val sc = graph.edges.sparkContext
    // sc.parallelize(Seq((1L, 2L, 0.5d), (1L, 3L, 0.4d)))
    sc.parallelize(results)
  }

  def jaccard[A](a: Set[A], b: Set[A]): Double = {
    val union = a.union(b).size
    val intersection = a.intersect(b)

    if (union > 0) intersection.size.toDouble / union.toDouble else 0.0d
  }
}
