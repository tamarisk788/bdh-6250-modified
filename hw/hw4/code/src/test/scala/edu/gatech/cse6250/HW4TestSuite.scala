package edu.gatech.cse6250

import edu.gatech.cse6250.clustering.PowerIterationClustering
import edu.gatech.cse6250.graphconstruct.GraphLoader
import edu.gatech.cse6250.randomwalk.RandomWalk
import edu.gatech.cse6250.jaccard.Jaccard
import edu.gatech.cse6250.main.Main.{loadRddRawData, author}
import edu.gatech.cse6250.model._
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.scalatest.FunSuite

trait SparkSessionTestWrapper {
  lazy val spark: SparkSession = {
    SparkSession
      .builder()
      .appName("hw4-tester")
      .master("local[*]")
      .config("spark.sql.shuffle.partitions", 15)
      .getOrCreate()
  }
}

class HW4TestSuite extends FunSuite with SparkSessionTestWrapper {

  info("=================================")
  info("CSE 6250 HW4 Test Start")
  info(s"Student: $author")
  info("=================================")
  info("")
  info("")


  info("")
  info("=================================")
  info("Graph Loader testing start............")
  info("=================================")
  info("")

  val (patient, medication, labResult, diagnostic) = loadRddRawData(spark)
  val graph:Graph[VertexProperty, EdgeProperty] =
    GraphLoader.load( patient, labResult, medication, diagnostic )
  val summary:Long = graph.edges.count() + graph.vertices.count()
  info(s"Graph size: $summary")
  info(s"Graph edges: $graph.edges.count()")
  info(s"Graph vertices: $graph.vertices.count()")

  test("Test check patient RDD count") {
    assert(patient.count === 978)
  }

  test("Test check medication RDD count") {
    assert(medication.count === 152894)
  }

  test("Test check labResult RDD count") {
    assert(labResult.count === 547683)
  }

  test("Test check diagnostic RDD count") {
    assert(diagnostic.count === 9478)
  }

  test("Test patient vertices") {
    assert(GraphLoader.vertexPatient.count === 978)
  }

  test("Test lab result vertices") {
    assert(GraphLoader.vertexLabResult.count === 226)
  }

  test("Test medication vertices") {
    assert(GraphLoader.vertexMedication.count === 49)
  }

  test("Test diagnostic vertices") {
    assert(GraphLoader.vertexDiagnostic.count === 1592)
  }

  test("Test edges lab results") {
    assert(GraphLoader.edgeLabResult.count === 97490)
  }

  test("Test edges medication") {
    assert(GraphLoader.edgeMedication.count === 4176)
  }

  test("Test edges diagnostic") {
    assert(GraphLoader.edgeDiagnostic.count === 18010)
  }

  test("Test graph vertices count") {
    assertResult(2845) {
      graph.vertices.count
    }
  }

  test("Test graph edges count") {
    assertResult(119676) {
      graph.edges.count
    }
  }

  info("")
  info("=================================")
  info("Jaccard testing start............")
  info("=================================")
  info("")

  test("Test jaccard similarity for patient 9") {
    val resultList = Jaccard.jaccardSimilarityOneVsAll(graph, 9).sorted
    info(s"$resultList")
    assertResult(9) {resultList.head}
    assertResult(81) {resultList(1)}
    assertResult(214) {resultList(2)}
    info(s"Patient 9: $resultList")
  }

  test("Test jaccard similarity for patient 490") {
    val resultList = Jaccard.jaccardSimilarityOneVsAll(graph, 490).sorted
    info(s"$resultList")
    assertResult(67) {resultList.head}
    assertResult(284) {resultList(1)}
    assertResult(343) {resultList(2)}
    info(s"Patient 490: $resultList")
  }

  test("Test jaccard similarity for patient 950") {
    val resultList = Jaccard.jaccardSimilarityOneVsAll(graph, 950).sorted
    info(s"$resultList")
    assertResult(25) {resultList.head}
    assertResult(80) {resultList(1)}
    assertResult(103) {resultList(2)}
    info(s"Patient 950: $resultList")
  }

  info("")
  info("=================================")
  info("RWR testing start............")
  info("=================================")
  info("")

  val resultList:List[Long] = RandomWalk.randomWalkOneVsAll(graph, 9, 100, 0.15)

  test("Test random walk with restart result 0") {
    assert(resultList.head === 634) // 513
  }

  test("Test random walk with restart result 1") {
    assert(resultList(1) === 433)
  }

  test("Test random walk with restart result 2") {
    assert(resultList(2) == 145)
  }

  test("Test random walk with restart result 3") {
    assert(resultList(3) == 720)
  }

  test("Test random walk with restart result 4") {
    assert(resultList(4) == 689)
  }

  test("Test random walk with restart result 5") {
    assert(resultList(5) == 441)
  }

  test("Test random walk with restart result 6") {
    assert(resultList(6) == 914)
  }

  test("Test random walk with restart result 7") {
    assert(resultList(7) == 801)
  }

  test("Test random walk with restart result 8") {
    assert(resultList(8) == 353)
  }

  test("Test random walk with restart result 9") {
    assert(resultList(9) == 781)
  }

  info("")
  info("=================================")
  info("PIC testing start........")
  info("=================================")
  info("")

  val similarities:RDD[(Long, Long, Double)] = Jaccard.jaccardSimilarityAllPatients(graph)
  println(similarities.count())
  println(similarities.take(20).toList)
  val jaccard_all_results: List[(Long, Long, Double)] = similarities.sortBy(x => (- x._3, x._2)).take(20).toList

  test("Test top 20 Jaccard similiarities by most similar and patient 2") {
    val jaccard_expected = List((120,5,1.0), (844,5,1.0), (845,51,1.0), (926,51,1.0), (215,51,1.0), (300,116,1.0),
      (829,118,1.0), (602,168,1.0), (185,168,1.0), (973,168,1.0), (204,168,1.0), (336,168,1.0), (758,168,1.0),
      (467,168,1.0), (602,185,1.0), (602,204,1.0), (185,204,1.0), (973,204,1.0), (845,215,1.0), (926,215,1.0))
    assert(jaccard_all_results === jaccard_expected)
  }

  //val actual = PowerIterationClustering.runPIC(similarities).collect.toMap
  val PIClabels: RDD[(Long, Int)] = PowerIterationClustering.runPIC(similarities)

  test("Test PIC label count") {
    assert(PIClabels.count === 978)
  }

  test("Test PIC labels first 20") {
    val expected_labels = List((2,2), (3,0), (4,0), (5,2), (6,0), (7,2), (8,2), (9,0), (10,2), (11,0), (12,0),
      (13,0), (14,0), (15,0), (16,2), (17,0), (18,0), (19,0), (20,0), (21,0))

    val actual_labels = PIClabels.sortBy(_._1).take(20).toList
    println(actual_labels)
    assert(expected_labels == actual_labels)
  }


  val clusters:Map[Int, Array[Long]] = PIClabels.collect().groupBy(_._2).mapValues(_.map(_._1))
  val assignments:List[(Int, Array[Long])] = clusters.toList.sortBy { case (k, v) => v.length }

  val assignmentsStr:List[String] = assignments
    .map { case (k, v) =>
      s"Cluster $k -> ${v.sorted.mkString("[", ",", "]")}"
    }

  val sizesStr:String = assignments.map {
    _._2.length
  }.sorted.mkString("(", ",", ")")


  println("Cluster assignments:")
  assignmentsStr.foreach(println)
  println(s"Cluster sizes: $sizesStr")

  val trueCluster0: List[Long] = List(3, 4, 6, 9, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 40, 41, 42, 43, 44, 45, 46, 49, 52, 53, 55, 56, 59, 61, 62, 63, 64, 65, 67, 68, 71, 72, 73, 75, 77, 78, 79, 80, 81, 83, 84, 85, 86, 87, 88, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 103, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 117, 119, 123, 124, 125, 126, 127, 129, 130, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 181, 182, 183, 184, 186, 187, 188, 189, 191, 192, 194, 195, 197, 198, 199, 200, 201, 202, 203, 205, 207, 208, 209, 210, 211, 212, 213, 214, 216, 217, 218, 221, 222, 223, 224, 225, 226, 228, 231, 234, 235, 236, 238, 239, 240, 241, 242, 243, 245, 246, 247, 248, 249, 250, 251, 252, 253, 255, 256, 257, 261, 262, 263, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 278, 279, 281, 282, 283, 284, 285, 286, 287, 289, 290, 291, 292, 293, 294, 295, 296, 298, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 313, 314, 315, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 333, 335, 338, 339, 340, 342, 343, 344, 345, 346, 347, 348, 350, 351, 352, 353, 354, 356, 357, 359, 360, 361, 362, 364, 365, 366, 367, 368, 369, 370, 371, 372, 373, 375, 376, 377, 378, 379, 381, 382, 383, 384, 386, 388, 389, 390, 391, 393, 394, 395, 396, 397, 398, 399, 400, 402, 404, 405, 406, 407, 408, 409, 410, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428, 429, 430, 432, 433, 434, 436, 437, 438, 439, 440, 441, 443, 445, 446, 448, 449, 450, 452, 453, 454, 456, 457, 458, 461, 462, 463, 464, 466, 468, 470, 471, 472, 473, 474, 477, 478, 479, 480, 481, 482, 485, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 497, 498, 499, 500, 501, 502, 503, 505, 507, 508, 509, 510, 511, 513, 514, 515, 516, 517, 518, 519, 521, 522, 523, 525, 527, 528, 529, 530, 532, 533, 534, 536, 538, 539, 540, 542, 543, 544, 545, 546, 548, 549, 550, 551, 553, 554, 557, 558, 559, 561, 562, 563, 564, 565, 568, 569, 570, 571, 572, 575, 577, 579, 580, 581, 583, 584, 585, 586, 587, 588, 589, 590, 591, 592, 593, 594, 595, 596, 599, 600, 601, 603, 604, 605, 606, 608, 609, 610, 612, 613, 614, 616, 617, 618, 619, 620, 621, 622, 623, 624, 625, 626, 627, 628, 629, 630, 631, 633, 634, 635, 636, 638, 639, 641, 642, 644, 649, 650, 651, 652, 653, 654, 655, 656, 657, 660, 661, 663, 664, 665, 666, 667, 668, 669, 670, 671, 672, 673, 674, 676, 678, 679, 680, 681, 682, 683, 684, 685, 688, 689, 690, 691, 695, 697, 698, 699, 700, 701, 702, 703, 704, 705, 706, 707, 708, 709, 710, 711, 712, 714, 715, 716, 717, 718, 719, 720, 721, 722, 723, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736, 738, 739, 740, 741, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 754, 755, 756, 757, 759, 761, 765, 766, 767, 768, 770, 771, 772, 773, 774, 776, 777, 778, 779, 780, 781, 782, 784, 785, 786, 787, 788, 790, 791, 792, 793, 794, 796, 797, 798, 799, 801, 803, 804, 805, 806, 807, 808, 810, 811, 813, 814, 815, 816, 818, 819, 820, 822, 823, 824, 826, 827, 828, 831, 832, 834, 835, 837, 838, 840, 841, 843, 846, 848, 849, 850, 852, 853, 854, 855, 856, 857, 858, 859, 860, 861, 862, 863, 864, 865, 867, 870, 871, 873, 874, 875, 876, 877, 878, 879, 880, 881, 882, 883, 884, 885, 886, 887, 888, 889, 890, 891, 893, 894, 895, 896, 897, 899, 900, 901, 902, 903, 904, 905, 906, 907, 908, 909, 910, 912, 914, 915, 916, 918, 919, 921, 922, 923, 924, 925, 927, 928, 929, 930, 931, 932, 933, 935, 936, 937, 938, 940, 942, 943, 944, 945, 946, 947, 948, 949, 950, 952, 953, 955, 956, 957, 958, 959, 960, 961, 963, 969, 970, 971, 974, 975, 976, 977, 978, 979, 980, 981, 982, 983, 984, 985, 989, 990, 992, 993, 994, 995, 998, 999, 1000)
  val trueCluster1: List[Long] = List(60, 70, 82, 122, 220, 277, 332, 349, 363, 385, 431, 435, 475, 524, 531, 547, 556, 566, 615, 640, 662, 693, 760, 763, 825, 839)
  val trueCluster2: List[Long] = List(2, 5, 7, 8, 10, 16, 27, 39, 50, 51, 54, 57, 58, 66, 69, 74, 89, 102, 104, 116, 118, 120, 128, 131, 153, 167, 168, 180, 185, 190, 193, 204, 215, 219, 227, 229, 230, 232, 233, 244, 258, 260, 264, 280, 288, 297, 299, 300, 312, 316, 331, 334, 336, 337, 341, 355, 358, 374, 380, 387, 392, 401, 403, 411, 442, 444, 447, 451, 455, 459, 460, 465, 467, 469, 476, 483, 484, 506, 512, 520, 526, 535, 537, 541, 552, 560, 567, 573, 582, 597, 598, 602, 607, 611, 632, 637, 643, 645, 646, 647, 648, 659, 675, 677, 686, 687, 692, 694, 696, 713, 724, 737, 742, 758, 762, 764, 775, 783, 789, 795, 800, 802, 809, 812, 817, 821, 829, 830, 833, 836, 842, 844, 845, 847, 851, 866, 868, 869, 872, 892, 898, 911, 917, 920, 926, 934, 939, 941, 951, 962, 964, 965, 966, 967, 968, 972, 973, 986, 987, 988, 991, 996, 997)

  val cluster0:List[Long] = assignments.filter({ case (k, v) => k == 0 }).flatMap({ case (k, v) => v.toList })
  val cluster1:List[Long]  = assignments.filter({ case (k, v) => k == 1 }).flatMap({ case (k, v) => v.toList })
  val cluster2:List[Long]  = assignments.filter({ case (k, v) => k == 2 }).flatMap({ case (k, v) => v.toList })

  test("Compare true cluster 1 to returned cluster 1") {
    assert(trueCluster1 === cluster1.sorted)
  }

  val recallCluster0:Double = trueCluster0.intersect(cluster0).size.toDouble / cluster0.size.toDouble
  val recallCluster1:Double  = trueCluster1.intersect(cluster1).size.toDouble / cluster1.size.toDouble
  val recallCluster2:Double  = trueCluster2.intersect(cluster2).size.toDouble / cluster2.size.toDouble

  val scoreCluster0:Double  = Math.floor(recallCluster0 * 2.0)
  val scoreCluster1:Double  = Math.floor(recallCluster1 * 1.0)
  val scoreCluster2:Double  = Math.floor(recallCluster2 * 1.0)

  test("PIC Score") {
    assert(scoreCluster0 + scoreCluster1 + scoreCluster2 + 1.0 === 5)
  }

}
