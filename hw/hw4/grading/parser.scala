import java.io.{File, PrintWriter, FileOutputStream}

object Gradinghw4 {

  def main(args: Array[String]) {
    val input = io.Source.fromFile("output.log")
    val lines = input.getLines.toList.filter(_.startsWith("FOR_PARSE")).map(_.drop(10)).filter(_.nonEmpty)
      .map(_.split("\t"))
      .sortBy(_(0))

    val path = args(0)

    val pw = new PrintWriter(new FileOutputStream(new File(path), true))
    println(s"Tests Passed")
    pw.append(s"${args(1)},${args(2)}," + lines.map(_.mkString(",")).mkString(",") + "\n")
    pw.close()

  } }