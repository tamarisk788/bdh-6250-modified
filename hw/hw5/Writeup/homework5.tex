\documentclass[12pt]{article}
\usepackage{enumitem}
\usepackage{setspace}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{amsmath, amsthm}
\usepackage{booktabs}
\RequirePackage[colorlinks]{hyperref}
\usepackage[lined,boxed,linesnumbered,commentsnumbered]{algorithm2e}
\usepackage{xcolor}
\usepackage{listings}
\lstset{basicstyle=\ttfamily,
  showstringspaces=false,
  commentstyle=\color{red},
  keywordstyle=\color{blue}
}

% Margins
\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\linespread{1.1}

% Commands
\newenvironment{solution}
  {\begin{proof}[Solution]}
  {\end{proof}}

\title{CSE6250: Big Data Analytics in Healthcare \\ Homework 5}
\author{Jimeng Sun}
\date{Deadline: April 4, 2021, 11:55 PM AoE}

\begin{document}

\maketitle
\begin{itemize}
\item You can use 2 days of late submission throughout the semester \textbf{ONLY for homeworks}.
\item Discussion is encouraged, but each student must write his/her own answers and explicitly mention any collaborators.
\item Each student is expected to respect and follow the \href{http://www.honor.gatech.edu/}{ GT Honor Code}. \textbf{We will apply anti-cheating software to check for plagiarism.} Any one who is flagged by the software will automatically receive 0 for the homework and be reported to the college.
\item Please type the submission with \LaTeX\ or Microsoft Word. We \textbf{will not} accept hand written submissions.
\item Please \textbf{do not change the filenames and function definitions} in the skeleton code provided, as this will cause the test scripts to fail and you will  receive no points in those failed tests. Built-in modules of python and the following libraries -  pandas, numpy, scipy, scikit-learn can be used. 
%\item Update the README.txt file in the folder to include the version of python you have used. 
\item It is your responsibility to make sure that all code and other deliverables are in the correct format and that your submission compiles and runs. We will not manually check your code (not feasible given the class size). Thus \textbf{non-runnable code in our test environment will directly lead to 0 score} without comments. Also, your entire programming parts will \textbf{NOT} be graded and given 0 score if your code prints out anything that is not asked in each question.
\end{itemize}

\section*{Overview}

Neural Networks and Deep Learning are becoming more popular and widely applied to many data science domain including healthcare applications. In this homework, you will implement various types of neural network with clinical data. For this homework, \textbf{Python} programming will be required. See the attached skeleton code as a starting point for the programming questions. Also, you need to \textbf{make a single PDF file (\textit{homework5\_answer.pdf})} of a compiled document for non-programming questions. You can use the attached \LaTeX template.

It is highly recommended to complete \href{http://www.sunlab.org/teaching/cse6250/fall2018/dl/dl-setup.html#framework}{Deep Learning Lab} first if you have not finished it yet. (PyTorch version could be different, but most of parts are same.)  

% Each 
% The data provided in \textit{$events.csv$} are event sequences. Each line of this file consists of a tuple with the format \textit{(patient\textunderscore id, event\textunderscore id, event\textunderscore description, timestamp, value)}. 

% For example, 

% \begin{lstlisting}[frame=single, language=bash]
% 1053,DIAG319049,Acute respiratory failure,2924-10-08,1.0
% 1053,DIAG197320,Acute renal failure syndrome,2924-10-08,1.0
% 1053,DRUG19122121,Insulin,2924-10-08,1.0
% 1053,DRUG19122121,Insulin,2924-10-11,1.0
% 1053,LAB3026361,Erythrocytes in Blood,2924-10-08,3.000
% 1053,LAB3026361,Erythrocytes in Blood,2924-10-08,3.690
% 1053,LAB3026361,Erythrocytes in Blood,2924-10-09,3.240
% 1053,LAB3026361,Erythrocytes in Blood,2924-10-10,3.470
% \end{lstlisting}

% \begin{itemize}
% \item \textbf{patient\textunderscore id}: De-identified patient identiers. For example, the patient in the example above has patient id 1053. 
% \item \textbf{event\textunderscore id}: Clinical event identifiers. For example, DRUG19122121 means that a drug with RxNorm code as 19122121 was prescribed to the patient. DIAG319049 means the patient was diagnosed of disease with SNOMED code of 319049 and LAB3026361 means that the laboratory test with a LOINC code of 3026361 was conducted on the patient.
% \item $\textbf{event\textunderscore description}$: Shows the description of the clinical event. For example, DIAG319049 is the code for Acute respiratory failure and DRUG19122121 is the code for Insulin. 
% \item \textbf{timestamp}: the date at which the event happened. (Here the timestamp is not a real date.)
% \item \textbf{value}: Contains the value associated to an event. See Table~\ref{tbl:value} for the detailed description.
% \end{itemize}

% \begin{table}[th]
% \centering
% \begin{tabular}{@{}llp{4cm}l@{}}
% \toprule
% event type & sample event\textunderscore id & value meaning & example \\ \midrule
% diagnostic code & DIAG319049 & diagnosed with a certain disease, value always be 1.0 & 1.0 \\
% drug consumption  & DRUG19122121  & prescribed a certain medication, value will always be 1.0 & 1.0 \\
% laboratory test & LAB3026361 & test conducted on a patient and its value & 3.690 \\ \bottomrule
% \end{tabular}
% \caption{Event sequence value explanation}
% \label{tbl:value}
% \end{table}

% The data provided in \textit{mortality\textunderscore events.csv} contains the patient ids of only the deceased people. They are in the form of a tuple with the format \textit{(patient\textunderscore id, timestamp, label)}. For example,
% \begin{lstlisting}[frame=single, language=bash]
% 37,3265-12-31,1
% 40,3202-11-11,1
% \end{lstlisting}

% The timestamp indicates the death date of a deceased person and a label of 1 indicates death. Patients that are not mentioned in this file are considered alive.

% The \textit{event\textunderscore feature\textunderscore map.csv} is a map from an event\_id (SNOMED, LOINC and RxNorm) to an integer index. This file contains \textit{(idx, event\textunderscore id)} pairs for all event ids.

\section*{Python and dependencies}
In this homework, we will work with \textbf{PyTorch} 1.3 on Python 3.6 environment. If you do not have a python distribution installed yet, we recommend installing \href{https://docs.continuum.io/anaconda/install}{Anaconda} (or miniconda) with Python 3.6. We provide \textit{homework5/environment.yml} which contains a list of libraries needed to set environment
for this homework. You can use it to create a copy of conda `environment'. Refer to \href{https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html}{the documantation} for more details.
\begin{lstlisting}[frame=single, language=bash]
conda env create -f environment.yml
\end{lstlisting}
If you already have your own Python development environment (it should be Python 3.6), please refer to this file to find necessary libraries, which is used to set the same coding/grading environment.

The given environment file is based on Linux (Ubuntu) system. Mac OS users who want to use GPU should build PyTorch from its source. Please refer to \href{https://github.com/pytorch/pytorch#from-source}{PyTorch installation guide}. Windows users may need to install additional dependencies. Please refer to \href{https://pytorch.org/get-started/locally/}{PyTorch official guide}.

% \noindent Alternatively, we have prepared the docker environment. You are highly encouraged to set the environment ready ASAP following the tutorials of this \href{http://sunlab.org/teaching/cse6250/fall2018/env/#setup-virtual-environment}{link}, which you might use often in later homework. Please also activate \textit{environment.yml} when needed in coding.


% \section*{Running the tests}
% Test cases are provided for every module in this homework and operate on a subset of the data. To run a test, execute the following commands from the base folder i.e. homework1. If any of the test cases fail, an error will be shown. For example to test the statistics computed, the following command should be executed: \\

% \begin{lstlisting}[frame=single, language=bash]
% nosetests tests/test_statistics.py --nologcapture
% \end{lstlisting}

% \noindent A single test can also be run using this syntax: \\    
% %\textbf{nosetests tests/$<filename>$:$<test\_method>$}\\
% \begin{lstlisting}[frame=single, language=bash]
% nosetests tests/<filename>:<test_method> --nologcapture
% \end{lstlisting}
% Remember to use the right \textit{filename} and \textit{test\_method} in above command line. For more information about basic usage of nosetests, please refer to this \href{https://nose.readthedocs.io/en/latest/usage.html?highlight=#cmdoption--nologcapture}{LINK}

\section*{Content Overview}
\begin{lstlisting}[language=bash,frame=single]
homework5
|-- code
|  |-- etl_mortality_data.py
|  |-- mydatasets.py
|  |-- mymodels.py
|  |-- plots.py
|  |-- tests
|  |  |-- test_all.py
|  |-- train_seizure.py
|  |-- train_variable_rnn.py
|  |-- utils.py
|-- data
|  |-- mortality
|  |  |-- test
|  |  |  |-- ADMISSIONS.csv
|  |  |  |-- DIAGNOSES_ICD.csv
|  |  |  |-- MORTALITY.csv
|  |  |-- train
|  |  |  |-- ADMISSIONS.csv
|  |  |  |-- DIAGNOSES_ICD.csv
|  |  |  |-- MORTALITY.csv
|  |  |-- validation
|  |     |-- ADMISSIONS.csv
|  |     |-- DIAGNOSES_ICD.csv
|  |     |-- MORTALITY.csv
|  |-- seizure
|     |-- seizure_test.csv
|     |-- seizure_train.csv
|     |-- seizure_validation.csv
|-- environment.yml
|-- homework5.pdf
|-- homework5_answer.tex
\end{lstlisting}
\section*{Implementation Notes}
Throughout this homework, we will use \href{https://en.wikipedia.org/wiki/Cross_entropy}{\textit{Cross Entropy}} loss function, which has already been applied in the code template. \textbf{Please note that this loss function takes activation logits as its input, not probabilities. Therefore, your models should not have a softmax layer at the end.} Also, you may need to control the training parameters declared near the top of each main script, e.g., the number of epochs and the batch size, by yourself to achieve good model performances.

You will submit a trained model for each type of neural network, which will be saved by the code template, and they will be evaluated on a hidden test set in order to verify that you could train the models. \textbf{Please make sure that your saved/submitted model files match your model class definitions in your source code}. \textbf{You will get 0 score for each part in either case you do not submit your saved model files or we cannot use your model files directly with your code.} You need to submit only your final model (improved one) for each type.

\subsection*{Code Test}
There are two ways(steps) that you can test your implementation:
\begin{enumerate}
\item Python Unit Tests: Some unit tests are provided in  \textit{code/tests/test\_all.py}. You can run all tests, \textbf{in your \texttt{code} directory}, simply by
\begin{lstlisting}[language=bash,frame=single]
python -m pytest
\end{lstlisting}
The purpose of these unit tests is just to verify the return data types and structures
to match them with the grading system.
\item Gradescope Autograder: You can also check the result with our autograder provided
through Gradescope, which we use as the primary submission platform for this home-
work. It will takes a few minutes to get the results and feedback by the autograder.
\textbf{NOTE: There are a few hidden tests measuring the performance of your
models with higher target scores.} Please refer to the submission section for more
details at the end of this homework description.
\end{enumerate}

\section{Epileptic Seizure Classification [50 points]}
For the first part of this homework, we will use Epileptic Seizure Recognition Data Set which is originally from \href{http://archive.ics.uci.edu/ml/datasets/Epileptic+Seizure+Recognition}{UCI Machine Learning Repository}. You can see the three CSV files under the folder \textit{homework5/data/seizure}. Each file contains a header at the first line, and each line represent a single EEG record with its label at the last column (Listing \ref{lst:seizure}). Refer to the link for more details of this dataset.

\begin{lstlisting}[language=bash, frame=single, caption={Example of seizure data}, captionpos=b, label={lst:seizure}]
X1,X2,...,X178,y
-104,-96,...,-73,5
-58,-16,...,123,4
...
\end{lstlisting}

Please start at \textit{train\_seizure.py}, which is a main script for this part. If you have prepared the required Python environment, you can run all procedures simply by
\begin{lstlisting}[language=bash,frame=single]
python train_seizure.py
\end{lstlisting}

\subsection{Data Loading [5 points]}
First of all, we need to load the dataset to train our models. Basically, you will load the raw dataset files, and convert them into a PyTorch \href{https://pytorch.org/docs/stable/data.html#torch.utils.data.TensorDataset}{TensorDataset} which contains data tensor and target (label) tensor. Since each model requires a different shape of input data, you should convert the raw data accordingly. Please look at the code template, and you can complete each type of conversion at each stage as you progress. 
% \begin{enumerate}[label=\textbf{\alph*.}]
%     \item Complete \texttt{load\_seizure\_dataset} in \texttt{mydatasets.py} [5 points]
% \end{enumerate}

\bigskip
\textbf{a.} Complete \texttt{load\_seizure\_dataset} in \texttt{mydatasets.py} [5 points]

\subsection{Multi-layer Perceptron [15 points]}
We start with a simple form of neural network model first, Multi-layer Perceptron (MLP).
\begin{figure}
    \centering
    \includegraphics[width=.5\textwidth]{figs/mlp.png}
    \caption{An example of 3-layer MLP}
    \label{fig:mlp}
\end{figure}

\bigskip

\textbf{a.} Complete a class \texttt{MyMLP} in \texttt{mymodels.py}, and implement a 3-layer MLP similar to the Figure \ref{fig:mlp}. Use a hidden layer composed by 16 hidden units, followed by a sigmoid activation function. [3 points]

\bigskip
\textbf{b.} Calculate the number of "trainable" parameters in the model with providing the calculation details. How many floating-point computations will occur \textbf{APPROXIMATELY} when a new single data point comes in to the model? \textbf{You can make your own assumptions on the number of computations made by each elementary arithmetic, e.g., add/subtraction/multiplication/division/negation/exponent take 1 operation, etc.} [5 points]

\bigskip

\noindent It is important to monitor whether the model is being trained well or not. For this purpose, we usually track how some metric values, loss for example, change over the training iterations.

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figs/loss_curve.png}
        \caption{Loss curves}
        \label{fig:loss_curves}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figs/acc_curve.png}
        \caption{Accuracy curves}
        \label{fig:acc_curves}
    \end{subfigure}
    \caption{Learning curves}
    \label{fig:curves}
\end{figure}

\bigskip
\textbf{c.} Complete a function \texttt{plot\_learning\_curves} in \texttt{plots.py} to plot loss curves and accuracy curves for training and validation sets as shown in Figure \ref{fig:curves}. You should control the number of epochs (in the main script) according to your model training. Attach the plots for your MLP model in your report. [2 points]

\bigskip
\noindent
After model training is done, we also need to evaluate the model performance on an unseen (during the training procedure) test data set with some performance metrics. We will use \href{https://en.wikipedia.org/wiki/Confusion_matrix}{\textit{Confusion Matrix}} among many possible choices.

\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{figs/confusion_matrix.png}
    \caption{An example of confusion matrix}
    \label{fig:confusion_matrix}
\end{figure}

\bigskip
\textbf{d.} Complete a function \texttt{plot\_confusion\_matrix} in \texttt{plots.py} to make and plot a \textbf{\textit{normalized}} confusion matrix for test set similar to Figure \ref{fig:confusion_matrix}, where each element is a fraction of the predicted class given the true class. Attach the plot for your MLP model in your report. [2 points]

\bigskip

You have implemented a very simple MLP model. Try to improve it by changing model parameters, e.g., a number of layers and units, or applying any trick and technique applicable to neural networks.

\bigskip
\textbf{e.} Modify your model class \texttt{MyMLP} in \texttt{mymodels.py} to improve the performance. It still must be a MLP type of architecture. Explain your architecture and techniques used. Briefly discuss about the result with plots. [3 points]



\subsection{Convolutional Neural Network (CNN) [15 points]}
Our next model is CNN. Implement a CNN model constituted by a couple of convolutional layers, pooling layer, and fully-connected layers at the end as shown in Figure \ref{fig:cnn}.

\begin{figure}
    \centering
    \includegraphics[width=0.3\textwidth]{figs/cnn.png}
    \caption{CNN architecture to be implemented. $k$ Conv1d (size d) means there are $k$ numbers of Conv1d filters with a kernel size of $d$. Each convolution and fully-connected layer is followed by ReLU activation function except the last layer.}
    \label{fig:cnn}
\end{figure}

\bigskip
\textbf{a.} Complete \texttt{MyCNN} class in \texttt{mymodels.py}. Use two convolutional layers, one with 6 filters of the kernel size 5 (stride 1) and the other one with 16 filters with the kernel size 5 (stride 1), and they must be followed by Rectified Linear Unit (ReLU) activation. Each convolutional layer (after ReLU activation) is followed by a max pooling layer with the size (as well as stride) of 2. There are two fully-connected (as known as dense or linear) layer, one with 128 hidden units followed by ReLU activation and the other one is the output layer that has five units. [5 points]

\bigskip
\textbf{b.} Calculate the number of "trainable" parameters in the model with providing the calculation details. How many floating-point computation will occur \textbf{APPROXIMATELY} when a new single data point comes in to the model?  \textbf{You can make your own assumptions on the number of computations made by each elementary arithmetic, e.g., add/subtraction/multiplication/division/negation/exponent take 1 operation, etc.} [5 points]

\bigskip

\textbf{c.} Plot and attach the learning curves and the confusion matrix for your CNN model in your report. [2 points]

\bigskip

Once you complete the basic CNN model, try to improve it by changing model parameters and/or applying tricks and techniques.

\bigskip
\textbf{d.} Modify your model class \texttt{MyCNN} in \texttt{mymodels.py} to improve the performance. It still must be a CNN type of architecture. Explain your architecture and techniques used. Briefly discuss about the result with plots. [3 points]

\subsection{Recurrent Neural Network (RNN) [15 points]}
The final model you will implement on this dataset is RNN. For an initial architecture, you will use a Gated Recurrent Unit (GRU) followed by a fully connected layer. Since we have a sequence of inputs with a single label, we will use a many-to-one type of architecture as it is shown in Figure \ref{fig:rnn}.

\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{figs/rnn.png}
    \caption{A many-to-one type of RNN architecture to be implemented.}
    \label{fig:rnn}
\end{figure}

\textbf{a.} Complete \texttt{MyRNN} class in \texttt{mymodels.py}. Use one GRU layer with 16 hidden units. There should be one fully-connected layer connecting the hidden units of GRU to the output units. [5 points]

\textbf{b.} Calculate the number of "trainable" parameters in the model with providing the calculation details. How many floating-point computation will occur \textbf{APPROXIMATELY} when a new single data point comes in to the model?  \textbf{You can make your own assumptions on the number of computations made by each elementary arithmetic, e.g., add/subtraction/multiplication/division/negation/exponent take 1 operation, etc.} [5 points]

\bigskip

\textbf{c.} Plot and attach the learning curves and the confusion matrix for your RNN model in your report. [2 points]

\bigskip

\textbf{d.} Modify your model class \texttt{MyCNN} in \texttt{mymodels.py} to improve the performance. It still must be a RNN type of architecture. Explain your architecture and techniques used. Briefly discuss about the result with plots. [3 points]


\section{Mortality Prediction with RNN [45 points + 5 bonus]}
In the previous problem, the dataset consists of the same length sequences. In many real-world problems, however, data often contains variable-length of sequences, natural language processing for example. Also in healthcare problems, especially for longitudinal health records, each patient has a different length of clinical records history. In this problem, we will apply a recurrent neural network on variable-length of sequences from longitudinal electronic health record (EHR) data. 
Dataset for this problem was extracted from MIMIC-III dataset. You can see the three sub-folders under the folder \textit{homework5/data/mortality}, and each folder contains the following three CSV files.

\begin{itemize}
    \item \textbf{MORTALITY.csv}: A pre-processed label file with the following format with a header. SUBJECT\_ID represents a unique patient ID, and MORTALITY is the target label for the patient. \textbf{For the test set, all labels are -1, and you will submit your predictions on Kaggle competition at the end of this homework.}
\begin{lstlisting}[language=bash,frame=single]
SUBJECT_ID,MORTALITY
123,1
456,0
...
\end{lstlisting}
    \item \textbf{ADMISSIONS.csv}: A filtered MIMIC-III ADMISSION table file. It has patient visits (admissions) information with unique visit IDs (HADM\_ID) and dates (ADMITTIME)
    \item \textbf{DIAGNOSES\_ICD.csv}: A filtered MIMIC-III DIAGNOSES\_ICD table file. It contains diagnosis information as ICD-9 code given to patients at the visits.
\end{itemize}


Please start at \textit{train\_variable\_rnn.py}, which is a main script for this part. You can run all procedures simply by
\begin{lstlisting}[language=bash,frame=single]
python train_variable_rnn.py
\end{lstlisting}
\textbf{after you complete the required parts below}.  

Again, you will submit a trained model (the best one you achieved at the end), and it will be evaluated on a hidden test set in order to verify that you could train the models.

\subsection{Preprocessing [10 points]}
You may already have experienced that preprocessing the dataset is one of the most important part of data science before you apply any kind of machine learning algorithm. Here, we will implement a pipeline that process the raw dataset to transform it to a structure that we can use with RNN model. You can use typical Python packages such as Pandas and Numpy.

\textbf{Simplifying the problem, we will use the main codes, which are the first 3 or 4 alphanumeric values prior to the decimal point, of ICD-9 codes as features in this homework.} For example, for the ICD-9 code E826.9 (Pedal cycle accident injuring unspecified person), we will use E826 only, and thus it will be merged with other sub-codes under E826 such as E826.4 (Pedal cycle accident injuring occupant of streetcar).

The pipeline procedure is very similar to what you have done so far in the past homework, and it can be summarized as follows.
\begin{enumerate}
    \item Loading the raw data files.
    \item Group the diagnosis codes given to the same patient on the same date.
    \item Sorting the grouped diagnoses for the same patient, in chronological order.
    \item Extracting the main code from each ICD-9 diagnosis code, and converting them into unique feature ids, $0$ to $d-1$, where $d$ is the number of unique main codes. 
    \item Converting into the final format we want. Here we will use a List of (patient) List of (code) List as our final format of the dataset.  
\end{enumerate}
The order of some steps can be swapped for implementation convenience. Please look at the \texttt{main} function in \texttt{etl\_mortality\_data.py} first, and do the following tasks.

\bigskip
\textbf{a.} Complete \texttt{convert\_icd9} function in \texttt{etl\_mortality\_data.py}. Specifically, extract the the first 3 or 4 alphanumeric characters prior to the decimal point from a given ICD-9 code. Please refer to the \href{https://www.cms.gov/Medicare/Quality-Initiatives-Patient-Assessment-Instruments/HospitalQualityInits/Downloads/HospitalAppendix_F.pdf}{ICD-9-CM format} for the details, and note that there is no actual decimal point in the representation by MIMIC-III. [2 points]

\noindent 

\bigskip
\textbf{b.} Complete \texttt{build\_codemap} function in \texttt{etl\_mortality\_data.py}. Basically, it is very similar to what you did in Homework 1 and is just constructing a unique feature ID map from the ICD-9 (main) codes. [1 point]

\bigskip
\textbf{c.} Complete \texttt{create\_dataset} function in \texttt{etl\_mortality\_data.py} referring to the summarized steps above and the TODO comments in the code.  [7 points]

For example, if there are three patients with the visit information below (in an abstracted format),
\begin{lstlisting}[language=bash, frame=single]
SUBJECT_ID,ADMITTIME,ICD9_CODE
1, 2018-01-01, 123
1, 2018-01-01, 234
2, 2013-04-02, 456
2, 2013-04-02, 123
3, 2018-02-03, 234
2, 2013-03-02, 345
2, 2013-05-03, 678
2, 2013-05-03, 234
3, 2018-09-07, 987
\end{lstlisting}
the final visit sequence dataset should be
\begin{lstlisting}[language=bash, frame=single]
[[[0, 1]], [[3], [0, 2], [4, 1]], [[1], [5]]]
\end{lstlisting}
with the order of [Patient 1, Patient 2, Patient 3] and the code map \{123: 0, 234: 1, 456: 2, 345: 3, 678: 4, 987: 5\}.

Once you complete this pre-processing part, you should run it to prepare data files required in the next parts.
\begin{lstlisting}[language=bash,frame=single]
python etl_mortality_data.py
\end{lstlisting}

\subsection{Creating Custom Dataset [15 points]}
Next, we will convert the pre-processed data in the previous step into a custom type of PyTorch Dataset. When you create a custom (inherited) PyTorch Dataset, you should
override  at least \texttt{\_\_len\_\_} and \texttt{\_\_getitem\_\_} methods. \texttt{\_\_len\_\_} method just returns the size of dataset, and \texttt{\_\_getitem\_\_} actually returns a sample from the dataset according to the given index. Both methods have already been implemented for you. Instead, you have to complete the constructor of \texttt{VisitSequenceWithLabelDataset} class to make it a valid 'Dataset'. We will store the labels as a List of integer labels, and the sequence data as a List of matrix whose $i$-th row represents $i$-th visit while $j$-th column corresponds to the integer feature ID $j$. For example, the visit sequences we obtained from the previous example will be transformed to matrices as follows.

$$
\text{P1} = \begin{bmatrix}
                1 & 1 & 0 & 0 & 0 & 0
            \end{bmatrix},
~
\text{P2} = \begin{bmatrix}
                0 & 0 & 0 & 1 & 0 & 0 \\[0.3em]
                1 & 0 & 1 & 0 & 0 & 0 \\[0.3em]
                0 & 1 & 0 & 0 & 1 & 0
            \end{bmatrix},
~
\text{P3} = \begin{bmatrix}
                0 & 1 & 0 & 0 & 0 & 0 \\[0.3em]
                0 & 0 & 0 & 0 & 0 & 1
            \end{bmatrix}.
$$

\bigskip
\textbf{a.} Complete \texttt{calculate\_num\_features} function and \texttt{VisitSequenceWithLabelDataset} class in \texttt{mydatasets.py}. [5 points]

\bigskip
PyTorch DataLoader will generate mini-batches for you once you give it a valid Dataset, and most times you do not need to worry about how it generates each mini-batch if your data have fixed size. In this problem, however, we use variable size (length) of data, e.g., visits of each patient are represented by a matrix with a different size as in the example above. Therefore, we have to specify how each mini-batch should be generated by defining \texttt{collate\_fn}, which is an argument of DataLoader constructor. Here, we will create a custom collate function named \texttt{visit\_collate\_fn}, which creates a mini-batch represented by a 3D Tensor that consists of matrices with the same number of rows (visits) by padding zero-rows at the end of matrices shorter than the largest matrix in the mini-batch. Also, the order of matrices in the Tensor must be sorted by the length of visits in descending order. In addition, Tensors contains the lengths and the labels are also have to be sorted accordingly.

Please refer to the following example of a tensor constructed by using matrices from the previous example.

\begin{align*}
T[0, :, :] &= \begin{bmatrix}
                0 & 0 & 0 & 1 & 0 & 0 \\[0.3em]
                1 & 0 & 1 & 0 & 0 & 0 \\[0.3em]
                0 & 1 & 0 & 0 & 1 & 0
            \end{bmatrix}\\
T[1, :, :] &= \begin{bmatrix}
                0 & 1 & 0 & 0 & 0 & 0 \\[0.3em]
                0 & 0 & 0 & 0 & 0 & 1 \\[0.3em]
                0 & 0 & 0 & 0 & 0 & 0
            \end{bmatrix}\\
T[2, :, :] &= \begin{bmatrix}
                1 & 1 & 0 & 0 & 0 & 0 \\[0.3em]
                0 & 0 & 0 & 0 & 0 & 0 \\[0.3em]
                0 & 0 & 0 & 0 & 0 & 0
            \end{bmatrix}\\
\end{align*}
where $T$ is a Tensor contains a mini-batch size of $\text{batch\_size} \times \text{max\_length} \times \text{num\_features}$, and batch\_size=3, max\_length=3, and num\_features=6 for this example.
\bigskip
\noindent

\textbf{b.} Complete \texttt{visit\_collate\_fn} function in \texttt{mydatasets.py}. [10 points]

\subsection{Building Model [15 points]}
Now, we can define and train our model. Figure \ref{fig:var_rnn} shows the architecture you have to implement first, then you can try your own model.

\begin{figure}[!h]
    \centering
    \includegraphics[width=0.5\textwidth]{figs/var_rnn.png}
    \caption{A RNN architecture to be implemented with variable-length of sequences.}
    \label{fig:var_rnn}
\end{figure}


\bigskip
\textbf{a.} Complete \texttt{MyVariableRNN} class in \texttt{mymodels.py}. First layer is a fully-connected layer with 32 hidden units, which acts like an embedding layer to project sparse high-dimensional inputs to dense low-dimensional space. The projected inputs are passed through a GRU layer that consists of 16 units, which learns temporal relationship. It is followed by output (fully-connected) layer constituted by two output units. In fact, we can use a single output unit with a sigmoid activation function since it is a binary classification problem. However, we use a multi-class classification setting with the two output units to make it consistent with the previous problem for reusing the utility functions we have used. Use \texttt{tanh} activation funtion after the first FC layer, and remind that you should not use \texttt{softmax} or \texttt{sigmoid} activation after the second FC (output) layer since the loss function will take care of it. Train the constructed model and evaluate it. Include all learning curves and the confusion matrix in the report. [10 points]

\bigskip
\noindent

\textbf{b.} Modify your model class \texttt{MyVariableRNN} in \texttt{mymodels.py} to improve the performance. It still must be a RNN type that supports variable-length of sequences. Explain your architecture and techniques used. Briefly discuss about the result with plots. [5 points]

\subsection{Kaggle Competition [5 points + 5 bonus]}
\textbf{\color{red} NOTE: Deadline for the Kaggle submission can be different from the homework
deadline, check the exact competition deadline at the Kaggle page. NO LATE SUBMISSION will be accepted after the competition is closed.}

\noindent\textbf{\color{red} NOTE: Make sure your team name (or display name) is exactly your GT account username, e.g., san37.}

You should be familiar with Kaggle, which is a platform for predictive modeling and analytics competitions, that you used in the past homework. You will compete with your class mates using the result by your own model that you created in Q2.3.b.

Throughout this homework, we did not use \texttt{sigmoid} activation or \texttt{softmax} layer at the end of the model output since CrossEntropy loss applies it by itself. For Kaggle submission, however, we need to have a soft-label, probability of mortality, for each patient. For this purpose, you have to complete a function \texttt{predict\_mortality} function in the script to evaluate the test set and collect the probabilities.

\bigskip
\textbf{a.} Complete \texttt{predict\_mortality} function in \texttt{train\_variable\_rnn.py}, submit the generated predictions (output/mortality/my\_predictions.csv) to the Kaggle competition, and achieve AUC $>$ 0.6. [5 points]

\textbf{NOTE: it seems Kaggle accepts 20 submissions at maximum per day for each user.}

\bigskip
%http://www.kaggle.com/c/gt-cse6250-spring-2021-hw5
Submit your \textbf{soft-labeled} CSV prediction file (MORTALITY is in the range of [0,1]) you generate to this \href{http://www.kaggle.com/c/gt-cse6250-spring-2021-hw5}{Kaggle competition}\footnote{http://www.kaggle.com/c/gt-cse6250-spring-2021-hw5} (\textbf{you can join the competition via this link only}) created specifically for this assignment to compete with your fellow classmates. A \textbf{gatech.edu} email address is required to participate; follow the sign up direction using your GT email address, or if you already have an account, change the email on your existing account via your profile settings so that you can participate. \textbf{Make sure your team name or display name (not necessarily your username) matches your GT account username, e.g., san37,} so that your Kaggle submission can be linked to the grading system. Otherwise, you will get \textbf{ZERO point} for this part.

Evaluation criteria is AUC, and the predicted label should be a soft label, which represents the possibility of mortality for each patient you predict. The label range is between 0 and 1. 50\% of the data is used for the public leaderboard where you can receive feedback on your model. The final private leaderboard will use the remaining 50\% of the data and will determine your final class ranking.

Score at least 0.6 AUC to receive 5 points of credit. Additional bonus points can be received for your performance according to the following:
\begin{itemize}
\item Top 10\%: 5 bonus points
\item Top 20\%: 3 bonus points
\item Top 30\%: 1 bonus point
\end{itemize}
\textbf{Percentages are based on the entire class size, not just those who submit to Kaggle.}

\section{Submission [5 points]}
\textbf{NOTE: Please do NOT upload your homework to Canvas.}\par
You should submit your work through Gradescope. You can find a link to Gradescope
on Canvas. On Gradescope, you will see that there are two separate \textbf{HW5-coding} and
\textbf{HW5-written} submission pages.
\subsection{HW5-written}
Please submit a single PDF file containing all written parts of HW5, and mark the page(s) for each question. You may use the provided tex template. Use the following format for your PDF file name:
\begin{lstlisting}[language=bash,frame=single]
<your GTid>-<your GT account>-hw5-written.pdf
\end{lstlisting}
Example submission: 901234567-gburdell3-hw5.pdf
\subsection{HW5-coding}
Please submit a single .zip (ZIP compressed) file that contains all your programming parts; the folder structure of your submission should be as below. You can use the tree command to display and verify the folder structure is as seen below.
\begin{lstlisting}[language=bash,frame=single]
<your gtid>-<your gt account>-hw5
|-- code
   |-- etl_mortality_data.py
   |-- mydatasets.py
   |-- mymodels.py
   |-- plots.py
   |-- train_seizure.py
   |-- train_variable_rnn.py
   |-- utils.py
|-- output
   |-- mortality
      |-- MyVariableRNN.pth
   |-- seizure
      |-- MyCNN.pth
      |-- MyMLP.pth
      |-- MyRNN.pth
\end{lstlisting}Create a zip archive of the folder above and submit the zip file. Linux/Mac users may use the following command in the Terminal (-X option for Mac
users only):
\begin{lstlisting}[language=bash,frame=single]
zip -r -X <your GTid>-<your GT account>-hw5.zip \
<your GTid>-<your GT account>- hw5
\end{lstlisting}
Example submission: 901234567-gburdell3-hw5.zip
\newline
You will get some immediate results and feedback by the autograder after a few minutes.
\textbf{NOTE: There are a few hidden tests measuring the performance of your models with higher target scores than those you can see in the autograder results. These will be shown later after the grading period.}
\end{document}