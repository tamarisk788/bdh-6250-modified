import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

##### DO NOT MODIFY OR REMOVE THIS VALUE #####
checksum = '169a9820bbc999009327026c9d76bcf1'
##### DO NOT MODIFY OR REMOVE THIS VALUE #####


class MyMLP(nn.Module):
	def __init__(self):
		super(MyMLP, self).__init__()
		self.hidden = nn.Linear(178, 16)  # 1 point
		self.out = nn.Linear(16, 5)  # 1 point

	def forward(self, x):
		x = torch.sigmoid(self.hidden(x))  # 1 point
		x = self.out(x)
		return x


class MyCNN(nn.Module):
	def __init__(self):
		super(MyCNN, self).__init__()
		self.conv1 = nn.Conv1d(in_channels=1, out_channels=6, kernel_size=5)
		self.pool = nn.MaxPool1d(kernel_size=2)
		self.conv2 = nn.Conv1d(6, 16, 5)
		self.fc1 = nn.Linear(in_features=16 * 41, out_features=128)
		self.fc2 = nn.Linear(128, 5)

	def forward(self, x):
		x = self.pool(F.relu(self.conv1(x)))  # 1 point + 1 point
		x = self.pool(F.relu(self.conv2(x)))  # 1 point + 1 point
		x = x.view(-1, 16 * 41)
		x = F.relu(self.fc1(x))  # 1 point
		x = self.fc2(x)  # 1 point

		return x


class MyRNN(nn.Module):
	def __init__(self):
		super(MyRNN, self).__init__()
		self.rnn_num_layers = 1
		self.rnn_directions = 1
		self.rnn_hidden_size = 16
		self.rnn = nn.GRU(input_size=1, hidden_size=self.rnn_hidden_size, num_layers=self.rnn_num_layers, batch_first=True)
		self.fc = nn.Linear(in_features=self.rnn_hidden_size, out_features=5)

	def forward(self, x):
		# Set initial states
		device = torch.device("cuda" if next(self.parameters()).is_cuda else "cpu")
		h0 = torch.zeros(self.rnn_num_layers * self.rnn_directions, x.size(0), self.rnn_hidden_size).to(device)
		# c0 = torch.zeros(self.rnn_num_layers * self.rnn_directions, x.size(0), self.rnn_hidden_size).to(device)

		x, _ = self.rnn(x, h0)
		x = self.fc(x[:, -1, :])

		return x


class MyVariableRNN(nn.Module):
	def __init__(self, dim_input):
		super(MyVariableRNN, self).__init__()

		self.embedding = nn.Linear(dim_input, 32, bias=False)
		self.rnn = nn.GRU(input_size=32, hidden_size=16, batch_first=True)
		self.fc = nn.Linear(in_features=16, out_features=2)

	def forward(self, input_tuple):
		seqs, lengths = input_tuple
		batch_size, max_len = seqs.size()[:2]

		emb = torch.tanh(self.embedding(seqs))

		packed = pack_padded_sequence(emb, lengths, batch_first=True)
		rnn_out, last_states = self.rnn(packed)
		unpacked, unpacked_lengths = pad_packed_sequence(rnn_out, batch_first=True)

		idx = (lengths - 1).view(-1, 1).expand(unpacked.size(0), unpacked.size(2)).unsqueeze(1)
		# get last hidden output of each sequence
		gathered = unpacked.gather(1, idx).squeeze(dim=1)

		out = self.fc(gathered)

		return out

# https://github.com/exe1023/LSTM_LN
from lstm import LSTM


# class MyVariableRNN(nn.Module):
# 	def __init__(self, dim_input):
# 		super(MyVariableRNN, self).__init__()
#
# 		self.embedding = nn.Linear(dim_input, 32, bias=False)
# 		self.rnn = nn.GRU(input_size=32, hidden_size=16, batch_first=True)
# 		self.fc = nn.Linear(in_features=16, out_features=2)
#
# 		self.ln_lstm = LSTM(input_size=32, hidden_size=16, num_layers=1, bidirectional=0, batch_first=True)
#
# 	def forward(self, input_tuple):
# 		seqs, lengths = input_tuple
# 		batch_size, max_len = seqs.size()[:2]
#
# 		emb = torch.tanh(self.embedding(seqs))
#
# 		h0 = torch.zeros((1, batch_size, 16))
# 		c0 = torch.zeros((1, batch_size, 16))
# 		rnn_out, _ = self.ln_lstm(emb, (h0, c0))
#
# 		idx = (lengths - 1).view(-1, 1).expand(rnn_out.size(0), rnn_out.size(2)).unsqueeze(1)
# 		gathered = rnn_out.gather(1, idx).squeeze(dim=1)
#
# 		out = F.dropout(gathered, p=0.5)
# 		out = self.fc(out)
#
# 		return out
