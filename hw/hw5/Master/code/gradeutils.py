import os
import pickle
import numpy as np
import pandas as pd
from scipy import sparse
import torch
from torch.utils.data import TensorDataset, Dataset


def load_seizure_dataset(path, model_type):
	"""
	:param path: a path to the seizure data CSV file
	:return dataset: a TensorDataset consists of a data Tensor and a target Tensor
	"""
	# TODO: Read a csv file from path.
	# TODO: Please refer to the header of the file to locate X and y.
	# TODO: y in the raw data is ranging from 1 to 5. Change it to be from 0 to 4.
	# TODO: Remove the header of CSV file of course.
	# TODO: Do Not change the order of rows.
	# TODO: You can use Pandas if you want to.

	df = pd.read_csv(path)
	y = df['y'].values
	y = y - 1
	X = df.loc[:, 'X1':'X178'].values

	if model_type == 'MLP':
		dataset = TensorDataset(torch.from_numpy(X.astype('float32')), torch.from_numpy(y.astype('long')))
	elif model_type == 'CNN':
		dataset = TensorDataset(torch.from_numpy(X.astype('float32')).unsqueeze(1), torch.from_numpy(y.astype('long')))
	elif model_type == 'RNN':
		dataset = TensorDataset(torch.from_numpy(X.astype('float32')).unsqueeze(2), torch.from_numpy(y.astype('long')))
	else:
		raise AssertionError("Wrong Model Type!")

	return dataset


class VisitSequenceWithLabelDataset(Dataset):
	def __init__(self, seqs, labels, num_features):
		"""
		Args:
			seqs (list): list of patients (list) of visits (list) of codes (int) that contains visit sequences
			labels (list): list of labels (int)
			num_features (int): number of total features available
		"""

		if len(seqs) != len(labels):
			raise ValueError("Seqs and Labels have different lengths")

		self.seqs = []

		for seq, label in zip(seqs, labels):

			row = []
			col = []
			val = []
			for i, visit in enumerate(seq):
				for code in visit:
					if code < num_features:
						row.append(i)
						col.append(code)
						val.append(1.0)

			self.seqs.append(sparse.coo_matrix((np.array(val, dtype=np.float32), (np.array(row), np.array(col))),
										shape=(len(seq), num_features)))
		self.labels = labels

	def __len__(self):
		return len(self.labels)

	def __getitem__(self, index):
		return self.seqs[index], self.labels[index]


def visit_collate_fn(batch):
	"""
	DataLoaderIter call - self.collate_fn([self.dataset[i] for i in indices])
	Thus, 'batch' is a list [(seq1, label1), (seq2, label2), ... , (seqN, labelN)]
	where N is minibatch size, seq is a SparseFloatTensor, and label is a LongTensor

	:returns
		seqs
		labels
		lengths
	"""

	batch_seq, batch_label = zip(*batch)

	num_features = batch_seq[0].shape[1]
	seq_lengths = list(map(lambda patient_tensor: patient_tensor.shape[0], batch_seq))
	max_length = max(seq_lengths)

	sorted_indices, sorted_lengths = zip(*sorted(enumerate(seq_lengths), key=lambda x: x[1], reverse=True))
	sorted_padded_seqs = []
	sorted_labels = []

	for i in sorted_indices:
		length = batch_seq[i].shape[0]

		if length < max_length:
			padded = np.concatenate(
				(batch_seq[i].toarray(), np.zeros((max_length - length, num_features), dtype=np.float32)), axis=0)
		else:
			padded = batch_seq[i].toarray()

		sorted_padded_seqs.append(padded)
		sorted_labels.append(batch_label[i])

	seq_tensor = np.stack(sorted_padded_seqs, axis=0)
	label_tensor = torch.LongTensor(sorted_labels)

	# TODO: Return the following two things
	# TODO: 1. a tuple of (sequence data, length of each sequence), 2. label of each sequence
	return (torch.from_numpy(seq_tensor), torch.LongTensor(sorted_lengths)), label_tensor


class AverageMeter(object):
	"""Computes and stores the average and current value"""

	def __init__(self):
		self.reset()

	def reset(self):
		self.val = 0
		self.avg = 0
		self.sum = 0
		self.count = 0

	def update(self, val, n=1):
		self.val = val
		self.sum += val * n
		self.count += n
		self.avg = self.sum / self.count


def compute_batch_accuracy(output, target):
	"""Computes the accuracy for a batch"""
	with torch.no_grad():

		batch_size = target.size(0)
		_, pred = output.max(1)
		correct = pred.eq(target).sum()

		return correct * 100.0 / batch_size


def model_eval(model, data_loader):
	device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
	model.to(device)
	model.eval()

	accuracy = AverageMeter()

	with torch.no_grad():
		for i, (input, target) in enumerate(data_loader):

			if isinstance(input, tuple):
				input = tuple([e.to(device) if type(e) == torch.Tensor else e for e in input])
			else:
				input = input.to(device)
			target = target.to(device)

			output = model(input)
			accuracy.update(compute_batch_accuracy(output, target).item(), target.size(0))

	return accuracy.avg


def eval_seizure_model(model, model_type):
	PATH_TEST_FILE = "../data/seizure/seizure_test.csv"
	dataset = load_seizure_dataset(PATH_TEST_FILE, model_type)
	loader = torch.utils.data.DataLoader(dataset, batch_size=32, shuffle=False, num_workers=1)

	return model_eval(model, loader)


def convert_icd9_for_test(icd9_object):
	icd9_str = str(icd9_object)
	if icd9_str.startswith('E'):
		if len(icd9_str) > 4:
			converted = icd9_str[:4]
		else:
			converted = icd9_str
	else:
		if len(icd9_str) > 3:
			converted = icd9_str[:3]
		else:
			converted = icd9_str
	return converted


def build_codemap_for_test():
	PATH_TRAIN = "../data/mortality/train/"
	df_icd9 = pd.read_csv(os.path.join(PATH_TRAIN, "DIAGNOSES_ICD.csv"), usecols=["ICD9_CODE"])
	df_unique = df_icd9['ICD9_CODE'].dropna().apply(convert_icd9_for_test).drop_duplicates().reset_index(drop=True)
	return pd.Series(df_unique.index.values, index=df_unique).to_dict()
