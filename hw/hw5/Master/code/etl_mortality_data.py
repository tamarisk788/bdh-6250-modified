import os
import pickle
import pandas as pd

##### DO NOT MODIFY OR REMOVE THIS VALUE #####
checksum = '169a9820bbc999009327026c9d76bcf1'
##### DO NOT MODIFY OR REMOVE THIS VALUE #####

PATH_TRAIN = "../data/mortality/train/"
PATH_VALIDATION = "../data/mortality/validation/"
PATH_TEST = "../data/mortality/test/"
PATH_OUTPUT = "../data/mortality/processed/"


def convert_icd9(icd9_object):
	icd9_str = str(icd9_object)
	if icd9_str.startswith('E'):
		if len(icd9_str) > 4:
			converted = icd9_str[:4]
		else:
			converted = icd9_str
	else:
		if len(icd9_str) > 3:
			converted = icd9_str[:3]
		else:
			converted = icd9_str
	return converted


def build_codemap(df_icd9, transform):
	df_unique = df_icd9['ICD9_CODE'].dropna().apply(transform).drop_duplicates().reset_index(drop=True)
	return pd.Series(df_unique.index.values, index=df_unique).to_dict()


def create_dataset(path, codemap, transform):
	# Load data from csv files
	df_adm = pd.read_csv(os.path.join(path, "ADMISSIONS.csv"), usecols=['SUBJECT_ID', 'HADM_ID', 'ADMITTIME'])
	df_diag = pd.read_csv(os.path.join(path, "DIAGNOSES_ICD.csv"), usecols=['HADM_ID', 'ICD9_CODE'])
	df_mortality = pd.read_csv(os.path.join(path, "MORTALITY.csv"))

	df_diag['3DIGIT_ID'] = df_diag['ICD9_CODE'].apply(transform).map(codemap)
	df_diag = df_diag.dropna()
	df_diag['3DIGIT_ID'] = df_diag['3DIGIT_ID'].astype(int)

	series_visit = df_diag.drop(['ICD9_CODE'], axis=1).groupby('HADM_ID')['3DIGIT_ID'].apply(list)

	df_visit = df_adm.join(series_visit, on='HADM_ID', how='inner')

	df_visit['TIME_SEQ'] = list(zip(df_visit['ADMITTIME'], df_visit['3DIGIT_ID']))

	series_time_seq = df_visit.groupby('SUBJECT_ID')['TIME_SEQ'].apply(list)

	df_id_label_seq = df_mortality.join(series_time_seq, on='SUBJECT_ID', how='inner')

	patient_ids = df_id_label_seq['SUBJECT_ID'].tolist()
	seq_data = []
	for idx, item in df_id_label_seq['TIME_SEQ'].iteritems():
		visits = list(map(lambda x: x[1], sorted(item)))
		seq_data.append(visits)
	labels = df_id_label_seq['MORTALITY'].tolist()

	return patient_ids, labels, seq_data


def main():
	# Build a code map from the train set
	print("Build feature id map")
	df_icd9 = pd.read_csv(os.path.join(PATH_TRAIN, "DIAGNOSES_ICD.csv"), usecols=["ICD9_CODE"])
	codemap = build_codemap(df_icd9, convert_icd9)
	os.makedirs(PATH_OUTPUT, exist_ok=True)
	pickle.dump(codemap, open(os.path.join(PATH_OUTPUT, "mortality.codemap.train"), 'wb'), pickle.HIGHEST_PROTOCOL)

	# Train set
	print("Construct train set")
	train_ids, train_labels, train_seqs = create_dataset(PATH_TRAIN, codemap, convert_icd9)

	pickle.dump(train_ids, open(os.path.join(PATH_OUTPUT, "mortality.ids.train"), 'wb'), pickle.HIGHEST_PROTOCOL)
	pickle.dump(train_labels, open(os.path.join(PATH_OUTPUT, "mortality.labels.train"), 'wb'), pickle.HIGHEST_PROTOCOL)
	pickle.dump(train_seqs, open(os.path.join(PATH_OUTPUT, "mortality.seqs.train"), 'wb'), pickle.HIGHEST_PROTOCOL)

	# Validation set
	print("Construct validation set")
	validation_ids, validation_labels, validation_seqs = create_dataset(PATH_VALIDATION, codemap, convert_icd9)

	pickle.dump(validation_ids, open(os.path.join(PATH_OUTPUT, "mortality.ids.validation"), 'wb'), pickle.HIGHEST_PROTOCOL)
	pickle.dump(validation_labels, open(os.path.join(PATH_OUTPUT, "mortality.labels.validation"), 'wb'), pickle.HIGHEST_PROTOCOL)
	pickle.dump(validation_seqs, open(os.path.join(PATH_OUTPUT, "mortality.seqs.validation"), 'wb'), pickle.HIGHEST_PROTOCOL)

	# Test set
	print("Construct grading set")
	test_ids, test_labels, test_seqs = create_dataset(PATH_TEST, codemap, convert_icd9)

	pickle.dump(test_ids, open(os.path.join(PATH_OUTPUT, "mortality.ids.test"), 'wb'), pickle.HIGHEST_PROTOCOL)
	pickle.dump(test_labels, open(os.path.join(PATH_OUTPUT, "mortality.labels.test"), 'wb'), pickle.HIGHEST_PROTOCOL)
	pickle.dump(test_seqs, open(os.path.join(PATH_OUTPUT, "mortality.seqs.test"), 'wb'), pickle.HIGHEST_PROTOCOL)

	# pickle.dump(test_ids, open(os.path.join(PATH_OUTPUT, "mortality.ids.grading"), 'wb'), pickle.HIGHEST_PROTOCOL)
	# pickle.dump(test_labels, open(os.path.join(PATH_OUTPUT, "mortality.labels.grading"), 'wb'), pickle.HIGHEST_PROTOCOL)
	# pickle.dump(test_seqs, open(os.path.join(PATH_OUTPUT, "mortality.seqs.grading"), 'wb'), pickle.HIGHEST_PROTOCOL)

	print("Complete!")


if __name__ == '__main__':
	main()
