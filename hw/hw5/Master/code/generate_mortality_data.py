import os
import pandas as pd
import numpy as np
from sklearn.model_selection import StratifiedShuffleSplit, StratifiedKFold

PATH_ADMISSIONS = "../data/mortality/raw/ADMISSIONS.csv"
PATH_DIAGNOSES = "../data/mortality/raw/DIAGNOSES_ICD.csv"
PATH_PATIENTS = "../data/mortality/raw/PATIENTS.csv"

PATH_TRAIN = "../data/mortality/train/"
PATH_VALIDATION = "../data/mortality/validation/"
PATH_TEST = "../data/mortality/test/"
PATH_KAGGLE = "../data/mortality/kaggle/"

df_adm = pd.read_csv(PATH_ADMISSIONS)
df_diag = pd.read_csv(PATH_DIAGNOSES)
df_patient = pd.read_csv(PATH_PATIENTS)

df_visit_twice = df_adm[df_adm.groupby('SUBJECT_ID')['HADM_ID'].transform('nunique') >= 2]
df_mortality = df_patient.filter(['SUBJECT_ID'])
df_mortality['MORTALITY'] = np.where(pd.isna(df_patient['DOD_HOSP']), 1, 0)  # actually, it means 1 Alive, 0 Dead
df_cohort = df_mortality[df_mortality.SUBJECT_ID.isin(df_visit_twice['SUBJECT_ID'].unique())]

# sss = StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=6250)
# for train_index, test_index in sss.split(df_cohort['SUBJECT_ID'], df_cohort['MORTALITY']):
# 	df_cohort_train = df_cohort.iloc[train_index]
# 	df_cohort_test = df_cohort.iloc[test_index]

skf = StratifiedKFold(n_splits=10, shuffle=True, random_state=20192)
train_index = []
val_index = []
test_index = []
for i, (train_fold, test_fold) in enumerate(skf.split(df_cohort['SUBJECT_ID'], df_cohort['MORTALITY'])):
	if i <= 1:
		test_index.extend(test_fold)
	elif i == 2:
		val_index.extend(test_fold)
	else:
		train_index.extend(test_fold)

df_cohort_train = df_cohort.iloc[train_index]
df_cohort_val = df_cohort.iloc[val_index]
df_cohort_test = df_cohort.iloc[test_index]

list_id_train = df_cohort_train['SUBJECT_ID'].tolist()
list_id_val = df_cohort_val['SUBJECT_ID'].tolist()
list_id_test = df_cohort_test['SUBJECT_ID'].tolist()

df_adm_train = df_adm[df_adm.SUBJECT_ID.isin(list_id_train)]
df_adm_val = df_adm[df_adm.SUBJECT_ID.isin(list_id_val)]
df_adm_test = df_adm[df_adm.SUBJECT_ID.isin(list_id_test)]

df_diag_train = df_diag[df_diag.SUBJECT_ID.isin(list_id_train)]
df_diag_val = df_diag[df_diag.SUBJECT_ID.isin(list_id_val)]
df_diag_test = df_diag[df_diag.SUBJECT_ID.isin(list_id_test)]

print(len(list_id_train))
print(len(df_adm_train['SUBJECT_ID'].unique()))
print(len(df_diag_train['SUBJECT_ID'].unique()))

print(len(list_id_val))
print(len(df_adm_val['SUBJECT_ID'].unique()))
print(len(df_diag_val['SUBJECT_ID'].unique()))

print(len(list_id_test))
print(len(df_adm_test['SUBJECT_ID'].unique()))
print(len(df_diag_test['SUBJECT_ID'].unique()))

os.makedirs(PATH_TRAIN, exist_ok=True)
os.makedirs(PATH_VALIDATION, exist_ok=True)
os.makedirs(PATH_TEST, exist_ok=True)

df_cohort_train.to_csv(os.path.join(PATH_TRAIN, "MORTALITY.csv"), index=False)
df_cohort_val.to_csv(os.path.join(PATH_VALIDATION, "MORTALITY.csv"), index=False)
df_cohort_test.to_csv(os.path.join(PATH_TEST, "TRUE_MORTALITY.csv"), index=False)
df_cohort_test['MORTALITY'] = -1
df_cohort_test.to_csv(os.path.join(PATH_TEST, "MORTALITY.csv"), index=False)
df_cohort_test['MORTALITY'] = np.random.uniform(0, 1, len(df_cohort_test))
df_cohort_test.to_csv(os.path.join(PATH_KAGGLE, "kaggle_sample_solution.csv"), index=False)

df_adm_train.to_csv(os.path.join(PATH_TRAIN, "ADMISSIONS.csv"), index=False)
df_adm_val.to_csv(os.path.join(PATH_VALIDATION, "ADMISSIONS.csv"), index=False)
df_adm_test.to_csv(os.path.join(PATH_TEST, "ADMISSIONS.csv"), index=False)

df_diag_train.to_csv(os.path.join(PATH_TRAIN, "DIAGNOSES_ICD.csv"), index=False)
df_diag_val.to_csv(os.path.join(PATH_VALIDATION, "DIAGNOSES_ICD.csv"), index=False)
df_diag_test.to_csv(os.path.join(PATH_TEST, "DIAGNOSES_ICD.csv"), index=False)
