import hashlib
import os
import scipy as sp
import numpy as np
import pandas as pd
import torch

from gradeutils import eval_seizure_model, build_codemap_for_test, convert_icd9_for_test, model_eval
from mydatasets import load_seizure_dataset, calculate_num_features, VisitSequenceWithLabelDataset, visit_collate_fn
from etl_mortality_data import convert_icd9, build_codemap, create_dataset
from mymodels import *


def test_checksum():
	try:
		from mydatasets import checksum as checksum1
		from mymodels import checksum as checksum2
		from etl_mortality_data import checksum as checksum3
		hash_str = "cse6250fall2019"
		md5_hash = hashlib.md5(hash_str.encode())
		hexcode = md5_hash.hexdigest()
		if checksum1 == hexcode and checksum2 == hexcode and checksum3 == hexcode:
			return True
		else:
			return False
	except:
		return False


def test_1_1_a():
	# Total score assigned is 5
	PATH_TRAIN_FILE = "../data/seizure/seizure_train.csv"
	score = 1  # Give 1 point for submission
	comment = ""

	# 1. MLP type
	dataset = load_seizure_dataset(PATH_TRAIN_FILE, "MLP")

	if type(dataset) == torch.utils.data.dataset.TensorDataset:
		score += 1

		data_tensor, target_tensor = dataset.tensors
		if data_tensor.size() == (8050, 178) and target_tensor.size() == (8050,):
			score += 1
		else:
			comment += "returned Tensors for MLP do not have expected shapes;"
	else:
		comment += "return is not TensorDataset;"

	# 2. CNN type
	dataset = load_seizure_dataset(PATH_TRAIN_FILE, "CNN")

	if type(dataset) == torch.utils.data.dataset.TensorDataset:
		data_tensor, target_tensor = dataset.tensors

		if data_tensor.size() == (8050, 1, 178) and target_tensor.size() == (8050,):
			score += 1
		else:
			comment += "returned Tensors for CNN do not have expected shapes;"
	else:
		comment += "return is not TensorDataset"

	# 3. RNN type
	dataset = load_seizure_dataset(PATH_TRAIN_FILE, "RNN")

	if type(dataset) == torch.utils.data.dataset.TensorDataset:
		data_tensor, target_tensor = dataset.tensors
		if data_tensor.size() == (8050, 178, 1) and target_tensor.size() == (8050,):
			score += 1
		else:
			comment += "returned Tensors for RNN do not have expected shapes;"
	else:
		comment += "return is not TensorDataset;"

	return score, comment


def test_1_2_a():
	# Total score assigned is 3

	score = 1  # Give 1 point for working code
	comment = ""

	model = torch.load("../output/seizure/MyMLP.pth", map_location=lambda storage, loc: storage)

	try:
		accuracy = eval_seizure_model(model, 'MLP')
		if accuracy >= 20.0:
			score += 2
		elif accuracy >= 10.0:
			score += 1
			comment += "Accuracy is lower than expected"
		else:
			comment += "Accuracy is lower than expected"
	except BaseException as e:
		score = 0
		comment = str(e.__class__.__name__) + ": " + str(e).replace(",", " ")

	return score, comment


def test_1_3_a():
	# Total score assigned is 5

	score = 1  # Give 1 point for working code
	comment = ""

	model = torch.load("../output/seizure/MyCNN.pth", map_location=lambda storage, loc: storage)
	try:
		accuracy = eval_seizure_model(model, 'CNN')
		if accuracy >= 60.0:
			score += 4
		elif accuracy >= 50.0:
			score += 3
			comment += "Accuracy is lower than expected"
		elif accuracy >= 40.0:
			score += 2
			comment += "Accuracy is lower than expected"
		elif accuracy >= 30.0:
			score += 1
			comment += "Accuracy is lower than expected"
		else:
			comment += "Accuracy is lower than expected"
	except BaseException as e:
		score = 0
		comment = str(e.__class__.__name__) + ": " + str(e).replace(",", " ")

	return score, comment


def test_1_4_a():
	# Total score assigned is 5

	score = 1  # Give 1 point for working code
	comment = ""

	model = torch.load("../output/seizure/MyRNN.pth", map_location=lambda storage, loc: storage)
	try:
		accuracy = eval_seizure_model(model, 'RNN')
		if accuracy >= 50.0:
			score += 4
		elif accuracy >= 40.0:
			score += 3
			comment += "Accuracy is lower than expected"
		elif accuracy >= 30.0:
			score += 2
			comment += "Accuracy is lower than expected"
		elif accuracy >= 20.0:
			score += 1
			comment += "Accuracy is lower than expected"
		else:
			comment += "Accuracy is lower than expected"
	except BaseException as e:
		score = 0
		comment = str(e.__class__.__name__) + ": " + str(e).replace(",", " ")

	return score, comment


def test_2_1_a():
	try:
		# Total score assigned is 2
		df_test = pd.DataFrame(['V1046', 'V090', '135', '1890', '19889', 'E9352', 'E927'], columns=['ICD9_CODE'])
		df_expected = pd.Series(['V10', 'V09', '135', '189', '198', 'E935', 'E927'])
		df_actual = df_test['ICD9_CODE'].apply(convert_icd9)
		num_correct = (df_expected == df_actual).sum()

		comment = ""
		if num_correct == len(df_test):
			score = 2
		elif num_correct > 0:
			score = 1
			comment += "Partially correct on grading cases"
		else:
			score = 0
			comment += "All grading cases failed"
	except BaseException as e:
		score = 0
		comment = str(e.__class__.__name__) + ": " + str(e).replace(",", " ")

	return score, comment


def test_2_1_b():
	PATH_TRAIN = "../data/mortality/train/"
	df_icd9 = pd.read_csv(os.path.join(PATH_TRAIN, "DIAGNOSES_ICD.csv"), usecols=["ICD9_CODE"])
	try:
		# Total score assigned is 1
		dict_actual = build_codemap(df_icd9, convert_icd9_for_test)
		ids = set(dict_actual.values())

		min_expected = 0
		max_expected = 910

		min_actual = min(ids)
		max_actual = max(ids)

		comment = ""
		if min_expected == min_actual and max_actual == max_expected:
			score = 1
		else:
			score = 0
			if min_expected != min_actual:
				comment += "Min value of feature ID is not {};".format(min_expected)
			if max_expected != max_actual:
				# if len(comment) != 0:
				# 	comment += " and "
				comment += "Max value of feature ID is not {};".format(max_expected)

	except BaseException as e:
		score = 0
		comment = str(e.__class__.__name__) + ": " + str(e).replace(",", " ")

	return score, comment


def test_2_1_c():
	try:
		# Total score assigned is 7
		score = 7
		comment = ""

		PATH_VAL = "../data/mortality/validation/"
		codemap = build_codemap_for_test()
		val_ids, val_labels, val_seqs = create_dataset(PATH_VAL, codemap, convert_icd9_for_test)

		if len(val_ids) != 754:
			score -= 1
			comment += "len(ids) is not matched;"
		if len(val_labels) != 754:
			score -= 1
			comment += "len(labels) is not matched;"
		if len(val_seqs) != 754:
			score -= 1
			comment += "len(seqs) is not matched;"

		test_sample_id = 357
		test_sample_label = 0
		test_sample_seqs = [
			[155, 87, 157, 156, 116, 195, 47, 43, 87, 143, 49, 97, 222],
			[116, 40, 41, 47, 29, 155, 168, 28, 87, 32, 349, 75, 195, 49, 55, 25, 108, 111, 271],
			[5, 75, 28, 40, 41, 116, 47, 49, 84, 50, 168, 155, 42, 43, 43, 50, 5, 205, 25, 26, 24, 102, 111, 60],
			[81, 43, 116, 5, 168, 29, 84, 155, 43, 84, 111, 155, 49, 55, 25, 24, 205],
			[1, 116, 87, 47, 84, 17, 74, 168, 5, 43, 84, 43, 49, 111, 155, 25, 24, 55, 153, 8, 67, 205]
		]

		sample_idx = val_ids.index(test_sample_id)
		sample_label = val_labels[sample_idx]
		sample_seq = val_seqs[sample_idx]

		if sample_label != test_sample_label:
			score -= 1
			comment += "grading sample label is not matched;"

		if len(sample_seq) != len(test_sample_seqs):
			score -= 1
			comment += "the number of visits in the grading sample is not matched;"

		test_sample_set = list(map(set, test_sample_seqs))
		if list(map(set, sample_seq)) != test_sample_set:
			score -= 2
			comment += "grading sample seq is not matched;"

	except BaseException as e:
		score = 0
		comment = str(e.__class__.__name__) + ": " + str(e).replace(",", " ")

	return score, comment


def test_2_2_a():
	try:
		# Total score assigned is 5
		score = 5
		comment = ""

		sample_seqs = [[[0, 1]], [[3], [0, 2], [4, 1]], [[1], [5]]]
		sample_labels = [1, 0, 1]
		sample_num_features = 6
		sample_dataset = VisitSequenceWithLabelDataset(sample_seqs, sample_labels, sample_num_features)
		expected_arrays = [np.array([[1., 1., 0., 0., 0., 0.]]),
						   np.array([[0., 0., 0., 1., 0., 0.],
									 [1., 0., 1., 0., 0., 0.],
									 [0., 1., 0., 0., 1., 0.]]),
						   np.array([[0., 1., 0., 0., 0., 0.],
									 [0., 0., 0., 0., 0., 1.]])]

		if len(sample_seqs) != len(sample_dataset.seqs):
			score -= 1
			comment += "length of the dataset is not matched;"

		for i in range(len(sample_dataset.seqs)):
			if sp.sparse.issparse(sample_dataset.seqs[i]):
				sample_array = sample_dataset.seqs[i].toarray()
			else:
				sample_array = sample_dataset.seqs[i]

			if not np.array_equal(sample_array, expected_arrays[i]):
				score -= 1
			if sample_dataset.seqs[i].shape[0] != len(sample_seqs[i]):
				score -= 1
				comment += "number of rows for the {}-th patient not matched;".format(i)

		if sample_dataset.seqs[0].shape[1] != sample_num_features:
			score -= 1
			comment += "number of cols is not matched;"

	except BaseException as e:
		score = 0
		comment = str(e.__class__.__name__) + ": " + str(e).replace(",", " ")

	return score, comment


def test_2_2_b():
	try:
		# Total score assigned is 10
		score = 10
		comment = ""
		sample_arrays = [np.array([[1., 1., 0., 0., 0., 0.]]),
						 np.array([[0., 0., 0., 1., 0., 0.],
								   [1., 0., 1., 0., 0., 0.],
								   [0., 1., 0., 0., 1., 0.]]),
						 np.array([[0., 1., 0., 0., 0., 0.],
								   [0., 0., 0., 0., 0., 1.]])]
		sample_labels = [1, 0, 1]

		sample_batch = []
		for array, label in zip(sample_arrays, sample_labels):
			sp_array = sp.sparse.coo_matrix(array)

			# i = torch.LongTensor([sp_array.row, sp_array.col])
			# v = torch.FloatTensor(sp_array.data)
			# s = torch.sparse.FloatTensor(i, v, sp_array.shape)

			sample_batch.append((sp_array, label))

		# sparse_failed = False
		try:
			# COO
			(actual_seqs, actual_lengths), actual_labels = visit_collate_fn(sample_batch)
		except:
			try:
				# CSC
				sample_batch = [(coo_array.tocsc(), label) for coo_array, label in sample_batch]
				(actual_seqs, actual_lengths), actual_labels = visit_collate_fn(sample_batch)
			except:
				try:
					# CSR
					sample_batch = [(csc_array.tocsr(), label) for csc_array, label in sample_batch]
					(actual_seqs, actual_lengths), actual_labels = visit_collate_fn(sample_batch)
				except:
					# Dense
					sample_batch = [(np.asarray(csr_array.todense()), label) for csr_array, label in sample_batch]
					(actual_seqs, actual_lengths), actual_labels = visit_collate_fn(sample_batch)

		# sparse_failed = True

		# if sparse_failed:
		# 	dense_sample_batch = []
		# 	for sp_array, label in sample_batch:
		# 		dense_sample_batch.append((np.asarray(sp_array.todense()), label))
		# 	(actual_seqs, actual_lengths), actual_labels = visit_collate_fn(dense_sample_batch)

		expected_seqs = torch.FloatTensor(
			[[[0., 0., 0., 1., 0., 0.],
			  [1., 0., 1., 0., 0., 0.],
			  [0., 1., 0., 0., 1., 0.]],
			 [[0., 1., 0., 0., 0., 0.],
			  [0., 0., 0., 0., 0., 1.],
			  [0., 0., 0., 0., 0., 0.]],
			 [[1., 1., 0., 0., 0., 0.],
			  [0., 0., 0., 0., 0., 0.],
			  [0., 0., 0., 0., 0., 0.]]])
		expected_lengths = torch.LongTensor([3, 2, 1])
		expected_labels = torch.LongTensor([0, 1, 1])

		if not torch.equal(expected_seqs, actual_seqs.float()):
			score -= 6
			comment += "seqs tensor is not matched;"
		if not torch.equal(expected_lengths, actual_lengths):
			score -= 2
			comment += "lengths tensor is not matched;"
		if not torch.equal(expected_labels, actual_labels):
			score -= 2
			comment += "labels tensor is not matched;"

	except BaseException as e:
		score = 0
		comment = str(e.__class__.__name__) + ": " + str(e).replace(",", " ")

	return score, comment


def test_2_3_a():
	try:
		# Total score assigned is 10
		score = 1
		comment = ""

		PATH_TRAIN = "../data/mortality/train/"
		df_icd9 = pd.read_csv(os.path.join(PATH_TRAIN, "DIAGNOSES_ICD.csv"), usecols=["ICD9_CODE"])

		codemap = build_codemap(df_icd9, convert_icd9)
		train_ids, train_labels, train_seqs = create_dataset(PATH_TRAIN, codemap, convert_icd9)
		num_features = calculate_num_features(train_seqs)

		PATH_VALIDATION = "../data/mortality/validation/"
		validation_ids, validation_labels, validation_seqs = create_dataset(PATH_VALIDATION, codemap, convert_icd9)

		dataset = VisitSequenceWithLabelDataset(validation_seqs, validation_labels, num_features)
		loader = torch.utils.data.DataLoader(dataset=dataset, batch_size=32, shuffle=False, collate_fn=visit_collate_fn, num_workers=1)

		model = torch.load("../output/mortality/MyVariableRNN.pth", map_location=lambda storage, loc: storage)

		accuracy = model_eval(model, loader) # eval_mortality_model(model)

		if accuracy >= 70.0:
			score += 9
		elif accuracy >= 65.0:
			score += 8
			comment += "Accuracy is lower than expected"
		elif accuracy >= 60.0:
			score += 7
			comment += "Accuracy is lower than expected"
		elif accuracy >= 55.0:
			score += 6
			comment += "Accuracy is lower than expected"
		elif accuracy >= 50.0:
			score += 5
			comment += "Accuracy is lower than expected"
		elif accuracy >= 45.0:
			score += 4
			comment += "Accuracy is lower than expected"
		elif accuracy >= 40.0:
			score += 3
			comment += "Accuracy is lower than expected"
		elif accuracy >= 35.0:
			score += 2
			comment += "Accuracy is lower than expected"
		elif accuracy >= 30.0:
			score += 1
			comment += "Accuracy is lower than expected"
		else:
			comment += "Accuracy is lower than expected"

	except BaseException as e:
		score = 0
		comment = str(e.__class__.__name__) + ": " + str(e).replace(",", " ")

	return score, comment


def main():
	if test_checksum():
		score_1_1_a, comment_1_1_a = test_1_1_a()
		# print(score_1_1_a, comment_1_1_a)
		score_1_2_a, comment_1_2_a = test_1_2_a()
		# print(score_1_2_a, comment_1_2_a)
		score_1_3_a, comment_1_3_a = test_1_3_a()
		# print(score_1_3_a, comment_1_3_a)
		score_1_4_a, comment_1_4_a = test_1_4_a()
		# print(score_1_4_a, comment_1_4_a)
		score_2_1_a, comment_2_1_a = test_2_1_a()
		# print(score_2_1_a, comment_2_1_a)
		score_2_1_b, comment_2_1_b = test_2_1_b()
		# print(score_2_1_b, comment_2_1_b)
		score_2_1_c, comment_2_1_c = test_2_1_c()
		# print(score_2_1_c, comment_2_1_c)
		score_2_2_a, comment_2_2_a = test_2_2_a()
		# print(score_2_2_a, comment_2_2_a)
		score_2_2_b, comment_2_2_b = test_2_2_b()
		# print(score_2_2_b, comment_2_2_b)
		score_2_3_a, comment_2_3_a = test_2_3_a()
		# print(score_2_3_a, comment_2_3_a)

		results = str(score_1_1_a) + ',' + comment_1_1_a + ',' + str(score_1_2_a) + ',' + comment_1_2_a + ','\
				  + str(score_1_3_a) + ',' + comment_1_3_a + ',' + str(score_1_4_a) + ',' + comment_1_4_a + ','\
				  + str(score_2_1_a) + ',' + comment_2_1_a + ',' + str(score_2_1_b) + ',' + comment_2_1_b + ','\
				  + str(score_2_1_c) + ',' + comment_2_1_c + ',' + str(score_2_2_a) + ',' + comment_2_2_a + ','\
				  + str(score_2_2_b) + ',' + comment_2_2_b + ',' + str(score_2_3_a) + ',' + comment_2_3_a
	else:
		results = '0,CHECKSUM FAILED,0,CHECKSUM FAILED,0,CHECKSUM FAILED,0,CHECKSUM FAILED,0,CHECKSUM FAILED,0,CHECKSUM FAILED,' \
				  + '0,CHECKSUM FAILED,0,CHECKSUM FAILED,0,CHECKSUM FAILED,0,CHECKSUM FAILED'

	print(results)


if __name__ == '__main__':
	main()
