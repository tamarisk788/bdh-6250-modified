import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split, StratifiedKFold

path = "../data/seizure/seizure.csv"

df = pd.read_csv(path)
y = df['y'].values
# y = y - 1
X = df.loc[:, 'X1':'X178'].values

np.random.seed(20192)
X_noisy = X + np.random.randint(-10, 10, X.shape)

skf = StratifiedKFold(n_splits=10, shuffle=True, random_state=20192)

train_index = []
for i, (train_fold, test_fold) in enumerate(skf.split(X_noisy, y)):
	if i == 0:
		private_index = test_fold
	elif i == 1:
		test_index = test_fold
	elif i == 2:
		val_index = test_fold
	else:
		train_index.extend(test_fold)

X_private = X_noisy[private_index]
y_private = y[private_index]

X_test = X_noisy[test_index]
y_test = y[test_index]
X_val = X_noisy[val_index]
y_val = y[val_index]
X_train = X_noisy[train_index]
y_train = y[train_index]

unique, count = np.unique(y, return_counts=True)
print(unique, count, sum(count))

unique, count = np.unique(y_train, return_counts=True)
print(unique, count, sum(count))

unique, count = np.unique(y_val, return_counts=True)
print(unique, count, sum(count))

unique, count = np.unique(y_test, return_counts=True)
print(unique, count, sum(count))

unique, count = np.unique(y_private, return_counts=True)
print(unique, count, sum(count))

# print(df.columns.values.tolist()[1:])
df_train = pd.DataFrame(data=np.hstack((X_train, np.reshape(y_train, (-1, 1)))), columns=df.columns.values.tolist()[1:])
df_val = pd.DataFrame(data=np.hstack((X_val, np.reshape(y_val, (-1, 1)))), columns=df.columns.values.tolist()[1:])
df_test = pd.DataFrame(data=np.hstack((X_test, np.reshape(y_test, (-1, 1)))), columns=df.columns.values.tolist()[1:])
df_private = pd.DataFrame(data=np.hstack((X_private, np.reshape(y_private, (-1, 1)))), columns=df.columns.values.tolist()[1:])

df_train.to_csv("../data/seizure/seizure_train.csv", index=False)
df_val.to_csv("../data/seizure/seizure_validation.csv", index=False)
df_test.to_csv("../data/seizure/seizure_test.csv", index=False)
df_private.to_csv("../data/seizure/seizure_private.csv", index=False)
