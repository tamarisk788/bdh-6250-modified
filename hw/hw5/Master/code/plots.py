import matplotlib.pyplot as plt
# TODO: You can use other packages if you want, e.g., Numpy, Scikit-learn, etc.


def plot_learning_curves(train_losses, valid_losses, train_accuracies, valid_accuracies):

	import numpy as np

	plt.figure(figsize=(12, 9))
	plt.plot(np.arange(len(train_losses)), np.array(train_losses), label='Training Loss')
	plt.plot(np.arange(len(valid_losses)), np.array(valid_losses), label='Validation Loss')
	plt.title("Loss Curve", fontsize=20)
	plt.xlabel('epoch', fontsize=16)
	plt.ylabel('Loss', fontsize=16)
	plt.xticks(fontsize=14)
	plt.yticks(fontsize=14)
	plt.legend(loc="best", fontsize=16)
	plt.tight_layout()
	plt.show()

	plt.figure(figsize=(12, 9))
	plt.plot(np.arange(len(train_accuracies)), np.array(train_accuracies), label='Training Accuracy')
	plt.plot(np.arange(len(valid_accuracies)), np.array(valid_accuracies), label='Validation Accuracy')
	plt.title("Accuracy Curve", fontsize=20)
	plt.xlabel('epoch', fontsize=16)
	plt.ylabel('Accuracy', fontsize=16)
	plt.xticks(fontsize=14)
	plt.yticks(fontsize=14)
	plt.legend(loc="best", fontsize=16)
	plt.tight_layout()
	plt.show()


def plot_confusion_matrix(results, class_names):
	from sklearn.metrics import confusion_matrix
	import itertools
	import numpy as np
	y_true, y_pred = tuple(zip(*results))
	# Compute confusion matrix  # 1 point
	cm = confusion_matrix(y_true, y_pred)
	np.set_printoptions(precision=2)

	# Plot normalized confusion matrix  # 1 point
	cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

	plt.figure(figsize=(12, 9))
	plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
	plt.title('Normalized Confusion Matrix', fontsize=20)
	plt.colorbar()
	plt.clim(0.0, 1.0)
	tick_marks = np.arange(len(class_names))
	plt.xticks(tick_marks, class_names, fontsize=14, rotation=45)
	plt.yticks(tick_marks, class_names, fontsize=14)

	fmt = '.2f'
	thresh = cm.max() / 2.
	for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
		plt.text(j, i, format(cm[i, j], fmt), horizontalalignment="center", fontsize=16, color="white" if cm[i, j] > thresh else "black")

	plt.ylabel('True', fontsize=16)
	plt.xlabel('Predicted', fontsize=16)

	plt.tight_layout()
	plt.show()
