#!/bin/bash
# run_all.sh
#
# Grades all submissions located in the current folder
#  - Should contain student archives or already extracted folders

# CONFIGURATION (change this to match your setup)
MASTER_DIR=/Users/stan/Workspace/TA/bdh/hw/hw5/Master
echo $MASTER_DIR

# Get current directory and push to top of directory stack
DIR=$(pwd)

echo "PROCESSING DIRECTORY: $DIR"
echo "Delete ALL folders in directory and re-untar student submissions?"
echo " --> rm -Rf -- $DIR/*/"
select yn in "Yes" "No"; do
    case $yn in
        Yes )
            echo "Clearing ALL Folders"
            rm -Rf -- */
            echo "Un-taring all archives in directory"
            # tar*.gz handles Canvas resubmit naming, e.g. tar-2.gz
            # ls *.tar*.gz | xargs -i tar -zxf {}
            # ls *.tar*.gz | xargs tar zxf # for OSX
            for file in *.tar*.gz; do tar -zxf $file; done
            break;;
        No )
            echo "Leaving ALL Folders"
            break;;
    esac
done

# Create new log file
RECORD_FILE=$DIR/grade.csv
#echo "BEGIN MASTER LOG..." > $RECORD_FILE
header="GTID,USERNAME,S_1_1_a,C_1_1_a,S_1_2_a,C_1_2_a,S_1_3_a,C_1_3_a,S_1_4_a,C_1_4_a,S_2_1_a,C_2_1_a,S_2_1_b,C_2_1_b,S_2_1_c,C_2_1_c,S_2_2_a,C_2_2_a,S_2_2_b,C_2_2_b,S_2_3_a,C_2_3_b"
echo $header > $RECORD_FILE

# Loop through and grade every student directory
for STUDENT_DIR in */; do
    #$GRADING_DIR/run_one.sh $STUDENT_DIR
    echo "Grading: ${STUDENT_DIR}"
    STUDENT_GTID=$(echo "$STUDENT_DIR" | cut -d '-' -f 1)
    STUDENT_USERNAME=$(echo "$STUDENT_DIR" | cut -d '-' -f 2)
    pushd $STUDENT_DIR
        cp ${MASTER_DIR}/code/grade_homework.py code/
        cp ${MASTER_DIR}/code/gradeutils.py code/
        #rsync -ahq --delete $MASTER_DIR/test/ test/
        rsync -ahq --delete $MASTER_DIR/data/ data/
        #$GRADING_DIR/code/sbt/sbt -Dsbt.log.noformat=true test | tee output.log
        #scala $GRADING_DIR/grading/parser.scala $RECORD_FILE $STUDENT_GTID $STUDENT_USERNAME
        touch 'code/__init__.py'
        pushd code
            OUTPUT=$(python -W ignore grade_homework.py)
            if [ $? -eq 0 ]
            then
              echo "$STUDENT_GTID,$STUDENT_USERNAME,$OUTPUT" >> $RECORD_FILE
            else
              # Redirect stdout from echo command to stderr.
              echo "$STUDENT_GTID,$STUDENT_USERNAME,Failed with a runtime error" >> $RECORD_FILE
            fi
        popd
        rm -rf data/
    popd
    # Output student results to the master record
    #cat $STUDENT_DIR/output.log >> $RECORD_FILE
done

exit 0
