MASTER_DIR=/Users/stan/Workspace/TA/bdh/hw/hw5/Master

cp ${MASTER_DIR}/code/grade_homework.py code/
cp ${MASTER_DIR}/code/gradeutils.py code/
rsync -ahq --delete $MASTER_DIR/data/ data/
touch 'code/__init__.py'
pushd code
    python -W ignore grade_homework.py
popd
rm -rf data/

exit 0
