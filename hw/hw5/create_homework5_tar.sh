#!/usr/bin/env bash

# clean up
rm -rf homework5
rm hw5.tar.gz

# copy data
mkdir -p homework5/code/tests
mkdir -p homework5/data/seizure
mkdir -p homework5/data/mortality

cp environment.yml homework5/
cp -r Student/code/*.py homework5/code/
cp -r Student/code/tests/test_all.py homework5/code/tests/

cp Master/data/seizure/seizure_train.csv homework5/data/seizure/
cp Master/data/seizure/seizure_validation.csv homework5/data/seizure/
cp Master/data/seizure/seizure_test.csv homework5/data/seizure/

cp -r Master/data/mortality/train homework5/data/mortality/
cp -r Master/data/mortality/validation homework5/data/mortality/
cp -r Master/data/mortality/test homework5/data/mortality/
rm homework5/data/mortality/test/TRUE_MORTALITY.csv

cp Writeup/homework5.pdf homework5/
cp Writeup/homework5_answer.tex homework5/

# clean up unnecessary files
./cleanup.sh

# create tar
tar cvzf hw5.tar.gz homework5/

# remove the folder
rm -rf homework5