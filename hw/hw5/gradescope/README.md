# Gradescope Autograder: HW-5

Gradescope autograder runs in a docker container.
The working directory will be /autograder when the autograder runs.

When we upload the autograder zip (steps mentioned below), it gets unzipped in /autograder/source directory inside the docker container. The students code gets unzipped in /autograder/submission directory. We  move the student code to any other directory as we want in run_autograder file (description below).

## Steps to set up a autograder

1. Create an assignment for programming part on gradescope: https://www.gradescope.com/courses/70643

2. Create files as defined in this repo: 

    2.1 requirements.txt : Add all the required python packages.

    2.2 setup.sh: This will run as the init script of gradescope docker container. Any machine level installations and pip install of requirements.txt will go in this.

    2.3 run_tests.py: This is the main unit test file. We can define all our tests in tests folder and this file will trigger all those tests.

    2.4 tests/ : Put all the test files in this directory. All unit test files should start with "test_" prefix. Weights and visibilitty for each test can be setup using @weight and @visibility decorators. Example: https://github.com/gradescope/autograder_samples/blob/master/python/src/tests/test_complex.py

    2.5 run_autograder: This runs the tests. Any copying of the code or unzipping/untar of submision files can be done in this.  

3.  Zip all the above files as mentioned here: https://gradescope-autograders.readthedocs.io/en/latest/specs/#autograder-specifications 
    
4. Upload the zipped file on the  assignment configure autograder section and test it.

## Writing Tests

Each of the unit test is given a weight using @weight decorator. Gradescope ideally wants us to keep unit tests separately, so we can either run a studens method in each uniit test method or we can run execute the code as part of setup method and evaluate the results by reading generated output. This may vary with the requirement of the homework.

## Helpful Links

1. https://gradescope-autograders.readthedocs.io/en/latest/getting_started/

2. https://gradescope-autograders.readthedocs.io/en/latest/tech/

3. https://gradescope-autograders.readthedocs.io/en/latest/manual_docker/

4. https://gradescope-autograders.readthedocs.io/en/latest/manual_grading/

5. Example test for checing general output: https://github.com/gradescope/autograder_samples/blob/master/diff_general/test_generator.py

6. Sample python autograder: https://github.com/gradescope/autograder_samples/tree/master/python

 