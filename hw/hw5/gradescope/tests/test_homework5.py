import warnings
warnings.filterwarnings("ignore")

import unittest
from gradescope_utils.autograder_utils.decorators import weight, visibility
import os
import scipy as sp
import numpy as np
import pandas as pd
import torch
import sys
sys.path.append('/autograder/source/')
sys.path.append('/autograder/source/code/')
# print(sys.path)
from gradeutils import eval_seizure_model, build_codemap_for_test, convert_icd9_for_test, eval_mortality_model, model_eval
from mydatasets import load_seizure_dataset, calculate_num_features, VisitSequenceWithLabelDataset, visit_collate_fn
from etl_mortality_data import convert_icd9, build_codemap, create_dataset
from mymodels import *

class TestComplex(unittest.TestCase):
    def setUp(self):
        pass

    @weight(1)
    def test_11a_submission(self):
        """ Submission Points for 1.1 (a) """
        PATH_TRAIN_FILE = "/autograder/source/data/seizure/seizure_train.csv"
        score = 1  # Give 1 point for submission
        self.assertEqual(1, 1)
    
    @weight(1)
    def test_11a_mlp_type(self):
        """ 1.1 (a) MLP Tensor Type """
        PATH_TRAIN_FILE = "/autograder/source/data/seizure/seizure_train.csv"
        dataset = load_seizure_dataset(PATH_TRAIN_FILE, "MLP")
        if type(dataset) == torch.utils.data.dataset.TensorDataset:
            self.assertEqual(1,1)
        else:
            self.assertEqual(1,2)
    
    @weight(1)
    def test_11a_mlp_shape(self):
        """ 1.1 (a) MLP Tensor Shape """
        PATH_TRAIN_FILE = "/autograder/source/data/seizure/seizure_train.csv"
        dataset = load_seizure_dataset(PATH_TRAIN_FILE, "MLP")

        data_tensor, target_tensor = dataset.tensors
        if data_tensor.size() == (8050, 178) and target_tensor.size() == (8050,):
            self.assertEqual(1,1)
        else:
            self.assertEqual(1, 2, "returned Tensors for MLP do not have expected shapes;")
    
    @weight(1)
    def test_11a_cnn_type(self):
        """ 1.1 (a) CNN Tensor Type """
        PATH_TRAIN_FILE = "/autograder/source/data/seizure/seizure_train.csv"
        dataset = load_seizure_dataset(PATH_TRAIN_FILE, "CNN")

        if type(dataset) == torch.utils.data.dataset.TensorDataset:
            self.assertEqual(1,1)
        else:
            self.assertEqual(1,2)
    
    @weight(1)
    def test_11a_cnn_shape(self):
        """ 1.1 (a) CNN Tensor Shape """
        PATH_TRAIN_FILE = "/autograder/source/data/seizure/seizure_train.csv"
        dataset = load_seizure_dataset(PATH_TRAIN_FILE, "CNN")

        data_tensor, target_tensor = dataset.tensors
        if data_tensor.size() == (8050, 1, 178) and target_tensor.size() == (8050,):
            self.assertEqual(1,1)
        else:
            self.assertEqual(1, 2, "returned Tensors for CNN do not have expected shapes;")
    
    @weight(1)
    def test_11a_rnn_type(self):
        """ 1.1 (a) RNN Tensor Type """
        PATH_TRAIN_FILE = "/autograder/source/data/seizure/seizure_train.csv"
        dataset = load_seizure_dataset(PATH_TRAIN_FILE, "CNN")

        if type(dataset) == torch.utils.data.dataset.TensorDataset:
            self.assertEqual(1,1)
        else:
            self.assertEqual(1,2)
    
    @weight(1)
    def test_11a_rnn_shape(self):
        """ 1.1 (a) RNN Tensor Shape """
        PATH_TRAIN_FILE = "/autograder/source/data/seizure/seizure_train.csv"
        dataset = load_seizure_dataset(PATH_TRAIN_FILE, "CNN")

        data_tensor, target_tensor = dataset.tensors
        if data_tensor.size() == (8050, 1, 178) and target_tensor.size() == (8050,):
            self.assertEqual(1,1)
        else:
            self.assertEqual(1, 2, "returned Tensors for RNN do not have expected shapes")

    @weight(1)
    def test_12a_code(self):
        """1.2 Loading of the MyMlp.pth """
        model = torch.load("/autograder/source/output/seizure/MyMLP.pth")
        self.assertEqual(1,1)

    @weight(2)
    def test_12a_accuracy_20(self):
        """1.2 (a) MyMlp.pth accuracy more than 20 """
        model = torch.load("/autograder/source/output/seizure/MyMLP.pth")
        accuracy = round(eval_seizure_model(model, 'MLP'))
        self.assertGreaterEqual(accuracy, 20)

    @weight(1)
    def test_12a_accuracy_10(self):
        """1.2 (a) MyMlp.pth accuracy more than 10 """
        model = torch.load("/autograder/source/output/seizure/MyMLP.pth")
        accuracy = round(eval_seizure_model(model, 'MLP'))
        self.assertGreaterEqual(accuracy, 10.0)

    @weight(1)
    def test_13a_code(self):
        """1.3 Loading of the MyCNN.pth """
        model = torch.load("/autograder/source/output/seizure/MyCNN.pth")
        self.assertEqual(1,1)
    
    @weight(1)
    def test_13a_accuracy_60(self):
        """1.3 (a) MyCNN.pth accuracy more than 60 """
        model = torch.load("/autograder/source/output/seizure/MyCNN.pth")
        accuracy = round(eval_seizure_model(model, 'CNN'))
        self.assertGreaterEqual(accuracy, 60.0)

    @weight(1)
    def test_13a_accuracy_50(self):
        """1.3 (a) MyCNN.pth accuracy more than 50 """
        model = torch.load("/autograder/source/output/seizure/MyCNN.pth")
        accuracy = round(eval_seizure_model(model, 'CNN'))
        self.assertGreaterEqual(accuracy, 50.0)
    
    @weight(1)
    def test_13a_accuracy_40(self):
        """1.3 (a) MyCNN.pth accuracy more than 40 """
        model = torch.load("/autograder/source/output/seizure/MyCNN.pth")
        accuracy = round(eval_seizure_model(model, 'CNN'))
        self.assertGreaterEqual(accuracy, 40.0)

    @weight(1)
    def test_13a_accuracy_30(self):
        """1.3 (a) MyCNN.pth accuracy more than 30 """
        model = torch.load("/autograder/source/output/seizure/MyCNN.pth")
        accuracy = round(eval_seizure_model(model, 'CNN'))
        self.assertGreaterEqual(accuracy, 30.0)


    @weight(1)
    def test_14a_code(self):
        """1.4 Loading of the MyRNN.pth """
        model = torch.load("/autograder/source/output/seizure/MyRNN.pth")
        self.assertEqual(1,1)
    
    @weight(1)
    def test_14a_accuracy_50(self):
        """1.4 (a) MyRNN.pth accuracy more than 50 """
        model = torch.load("/autograder/source/output/seizure/MyRNN.pth")
        accuracy = round(eval_seizure_model(model, 'RNN'))
        self.assertGreaterEqual(accuracy, 50.0)

    @weight(1)
    def test_14a_accuracy_40(self):
        """1.4 (a) MyRNN.pth accuracy more than 40 """
        model = torch.load("/autograder/source/output/seizure/MyRNN.pth")
        accuracy = round(eval_seizure_model(model, 'RNN'))
        self.assertGreaterEqual(accuracy, 40.0)
    
    @weight(1)
    def test_14a_accuracy_30(self):
        """1.4 (a) MyRNN.pth accuracy more than 30 """
        model = torch.load("/autograder/source/output/seizure/MyRNN.pth")
        accuracy = round(eval_seizure_model(model, 'RNN'))
        self.assertGreaterEqual(accuracy, 30.0)

    @weight(1)
    def test_13a_accuracy_20(self):
        """1.4 (a) MyRNN.pth accuracy more than 20 """
        model = torch.load("/autograder/source/output/seizure/MyRNN.pth")
        accuracy = round(eval_seizure_model(model, 'RNN'))
        self.assertGreaterEqual(accuracy, 20.0)

    @weight(1)
    def test_21a_len(self):
        """ 2.1 (a) Length Correctness  """ 
        df_test = pd.DataFrame(['V1046', 'V090', '135', '1890', '19889', 'E9352', 'E927'], columns=['ICD9_CODE'])
        df_expected = pd.Series(['V10', 'V09', '135', '189', '198', 'E935', 'E927'])
        df_actual = df_test['ICD9_CODE'].apply(convert_icd9)
        num_correct = (df_expected == df_actual).sum()
        self.assertEqual(num_correct, len(df_test), "ICD9_CODE Column Size does not match")
    
    @weight(1)
    def test_21a_partial(self):
        """ 2.1 (a) Partial Grade Test  """ 
        df_test = pd.DataFrame(['V1046', 'V090', '135', '1890', '19889', 'E9352', 'E927'], columns=['ICD9_CODE'])
        df_expected = pd.Series(['V10', 'V09', '135', '189', '198', 'E935', 'E927'])
        df_actual = df_test['ICD9_CODE'].apply(convert_icd9)
        num_correct = (df_expected == df_actual).sum()
        self.assertGreater(num_correct, 0, "ICD9_CODE Column Size is less than zero.")

    @weight(1)
    def test_2_1_b(self):
        """ Test for Min and Max Value of ICD9_CODE after build_codemap."""
        PATH_TRAIN = "/autograder/source/data/mortality/train/"
        df_icd9 = pd.read_csv(os.path.join(PATH_TRAIN, "DIAGNOSES_ICD.csv"), usecols=["ICD9_CODE"])
        dict_actual = build_codemap(df_icd9, convert_icd9_for_test)
        ids = set(dict_actual.values())

        min_expected = 0
        max_expected = 901

        min_actual = min(ids)
        max_actual = max(ids)
        self.assertEqual(min_expected, min_actual, "Min value of feature ID is not {};".format(min_expected))
        self.assertEqual(max_expected, max_actual, "Max value of feature ID is not {};".format(max_expected))
        
    @weight(1)
    def test_21c_len_ids(self):
        """ Length of ids """
        PATH_VAL = "/autograder/source/data/mortality/validation/"
        codemap = build_codemap_for_test()
        val_ids, val_labels, val_seqs = create_dataset(PATH_VAL, codemap, convert_icd9_for_test)
        self.assertEqual(len(val_ids), 754, "len(ids) is not matched")
    
    @weight(1)
    def test_21c_len_labels(self):
        """ Length of labels """
        PATH_VAL = "/autograder/source/data/mortality/validation/"
        codemap = build_codemap_for_test()
        val_ids, val_labels, val_seqs = create_dataset(PATH_VAL, codemap, convert_icd9_for_test)
        self.assertEqual(len(val_labels), 754, "len(labels) is not matched")

    @weight(1)
    def test_21c_len_seqs(self):
        """ Length of ids """
        PATH_VAL = "/autograder/source/data/mortality/validation/"
        codemap = build_codemap_for_test()
        val_ids, val_labels, val_seqs = create_dataset(PATH_VAL, codemap, convert_icd9_for_test)
        self.assertEqual(len(val_seqs), 754, "len(seqs) is not matched")
    
    @weight(1)
    def test_21c_len_sample_label(self):
        """ grading sample label test """
        PATH_VAL = "/autograder/source/data/mortality/validation/"
        codemap = build_codemap_for_test()
        val_ids, val_labels, val_seqs = create_dataset(PATH_VAL, codemap, convert_icd9_for_test)
        test_sample_id = 2136
        test_sample_label = 0
        test_sample_seqs = [[102, 32, 26, 33], [102, 49, 32, 106, 24, 209, 148, 26, 33],
                            [102, 96, 32, 80, 24, 35, 102, 26, 27],
                            [102, 142, 24, 35, 35, 205, 32, 6, 2, 82, 144, 26, 43, 178, 5],
                            [102, 32, 24, 15, 90, 35, 19, 3, 102, 5, 27, 148, 33, 43, 178],
                            [102, 15, 19, 80, 169, 32, 35, 29, 57, 37, 27, 173, 5, 378],
                            [142, 88, 32, 29, 4, 12, 35, 205, 52, 35, 5, 43, 26, 105, 5, 69, 298, 192, 144, 38, 142]]

        sample_idx = val_ids.index(test_sample_id)
        sample_label = val_labels[sample_idx]
        sample_seq = val_seqs[sample_idx]
        self.assertEqual(sample_label, test_sample_label, "grading sample label is not matched")

    @weight(1)
    def test_21c_len_sample_seq(self):
        """ the number of visits in the grading sample """
        PATH_VAL = "/autograder/source/data/mortality/validation/"
        codemap = build_codemap_for_test()
        val_ids, val_labels, val_seqs = create_dataset(PATH_VAL, codemap, convert_icd9_for_test)
        test_sample_id = 2136
        test_sample_label = 0
        test_sample_seqs = [[102, 32, 26, 33], [102, 49, 32, 106, 24, 209, 148, 26, 33],
                            [102, 96, 32, 80, 24, 35, 102, 26, 27],
                            [102, 142, 24, 35, 35, 205, 32, 6, 2, 82, 144, 26, 43, 178, 5],
                            [102, 32, 24, 15, 90, 35, 19, 3, 102, 5, 27, 148, 33, 43, 178],
                            [102, 15, 19, 80, 169, 32, 35, 29, 57, 37, 27, 173, 5, 378],
                            [142, 88, 32, 29, 4, 12, 35, 205, 52, 35, 5, 43, 26, 105, 5, 69, 298, 192, 144, 38, 142]]

        sample_idx = val_ids.index(test_sample_id)
        sample_label = val_labels[sample_idx]
        sample_seq = val_seqs[sample_idx]
        self.assertEqual(len(sample_seq), len(test_sample_seqs), "the number of visits in the grading sample is not matched")    

    @weight(2)
    def test_21c_len_sample_seq(self):
        """ grading sample seq test """
        PATH_VAL = "/autograder/source/data/mortality/validation/"
        codemap = build_codemap_for_test()
        val_ids, val_labels, val_seqs = create_dataset(PATH_VAL, codemap, convert_icd9_for_test)
        test_sample_id = 2136
        test_sample_label = 0
        test_sample_seqs = [[102, 32, 26, 33], [102, 49, 32, 106, 24, 209, 148, 26, 33],
                            [102, 96, 32, 80, 24, 35, 102, 26, 27],
                            [102, 142, 24, 35, 35, 205, 32, 6, 2, 82, 144, 26, 43, 178, 5],
                            [102, 32, 24, 15, 90, 35, 19, 3, 102, 5, 27, 148, 33, 43, 178],
                            [102, 15, 19, 80, 169, 32, 35, 29, 57, 37, 27, 173, 5, 378],
                            [142, 88, 32, 29, 4, 12, 35, 205, 52, 35, 5, 43, 26, 105, 5, 69, 298, 192, 144, 38, 142]]

        sample_idx = val_ids.index(test_sample_id)
        sample_label = val_labels[sample_idx]
        sample_seq = val_seqs[sample_idx]
        test_sample_set = list(map(set, test_sample_seqs))
        self.assertEqual(list(map(set, sample_seq)), test_sample_set, "grading sample seq is not matched")


    @weight(1)
    def test_22a_dataset_len(self):
        """2.2 (a) length of the dataset """
        sample_seqs = [[[0, 1]], [[3], [0, 2], [4, 1]], [[1], [5]]]
        sample_labels = [1, 0, 1]
        sample_num_features = 6
        sample_dataset = VisitSequenceWithLabelDataset(sample_seqs, sample_labels, sample_num_features)
        expected_arrays = [np.array([[1., 1., 0., 0., 0., 0.]]),
                        np.array([[0., 0., 0., 1., 0., 0.],
                                    [1., 0., 1., 0., 0., 0.],
                                    [0., 1., 0., 0., 1., 0.]]),
                        np.array([[0., 1., 0., 0., 0., 0.],
                                    [0., 0., 0., 0., 0., 1.]])]
        self.assertEqual(len(sample_seqs), len(sample_dataset.seqs), "length of the dataset is not matched")

    @weight(2)
    def test_22a_num_rows_pat(self):
        """2.2 (a) Num Rows Test for patients """
        sample_seqs = [[[0, 1]], [[3], [0, 2], [4, 1]], [[1], [5]]]
        sample_labels = [1, 0, 1]
        sample_num_features = 6
        sample_dataset = VisitSequenceWithLabelDataset(sample_seqs, sample_labels, sample_num_features)
        expected_arrays = [np.array([[1., 1., 0., 0., 0., 0.]]),
                        np.array([[0., 0., 0., 1., 0., 0.],
                                    [1., 0., 1., 0., 0., 0.],
                                    [0., 1., 0., 0., 1., 0.]]),
                        np.array([[0., 1., 0., 0., 0., 0.],
                                    [0., 0., 0., 0., 0., 1.]])]
        for i in range(len(sample_dataset.seqs)):
            self.assertEqual(sample_dataset.seqs[i].shape[0], len(sample_seqs[i]), "number of rows for the {}-th patient not matched;".format(i))

    @weight(2)
    def test_22a_num_rows_pat(self):
        """2.2 (a) Test for Checking Num Features """
        sample_seqs = [[[0, 1]], [[3], [0, 2], [4, 1]], [[1], [5]]]
        sample_labels = [1, 0, 1]
        sample_num_features = 6
        sample_dataset = VisitSequenceWithLabelDataset(sample_seqs, sample_labels, sample_num_features)
        expected_arrays = [np.array([[1., 1., 0., 0., 0., 0.]]),
                        np.array([[0., 0., 0., 1., 0., 0.],
                                    [1., 0., 1., 0., 0., 0.],
                                    [0., 1., 0., 0., 1., 0.]]),
                        np.array([[0., 1., 0., 0., 0., 0.],
                                    [0., 0., 0., 0., 0., 1.]])]
        
        self.assertEqual(sample_dataset.seqs[0].shape[1], sample_num_features, "number of cols is not matched")
    
    @weight(6)
    def test22b_seq(self):
        """ Test for Checking Seq Tensor"""
        sample_arrays = [np.array([[1., 1., 0., 0., 0., 0.]]),
                            np.array([[0., 0., 0., 1., 0., 0.],
                                    [1., 0., 1., 0., 0., 0.],
                                    [0., 1., 0., 0., 1., 0.]]),
                            np.array([[0., 1., 0., 0., 0., 0.],
                                    [0., 0., 0., 0., 0., 1.]])]
        sample_labels = [1, 0, 1]

        sample_batch = []
        for array, label in zip(sample_arrays, sample_labels):
            sp_array = sp.sparse.coo_matrix(array)

            # i = torch.LongTensor([sp_array.row, sp_array.col])
            # v = torch.FloatTensor(sp_array.data)
            # s = torch.sparse.FloatTensor(i, v, sp_array.shape)

            sample_batch.append((sp_array, label))

        sparse_failed = False
        try:
            (actual_seqs, actual_lengths), actual_labels = visit_collate_fn(sample_batch)
        except:
            sparse_failed = True

        if sparse_failed:
            dense_sample_batch = []
            for sp_array, label in sample_batch:
                dense_sample_batch.append((np.asarray(sp_array.todense()), label))
            (actual_seqs, actual_lengths), actual_labels = visit_collate_fn(dense_sample_batch)

        expected_seqs = torch.FloatTensor(
            [[[0., 0., 0., 1., 0., 0.],
            [1., 0., 1., 0., 0., 0.],
            [0., 1., 0., 0., 1., 0.]],
            [[0., 1., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 1.],
            [0., 0., 0., 0., 0., 0.]],
            [[1., 1., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0.]]])
        expected_lengths = torch.LongTensor([3, 2, 1])
        expected_labels = torch.LongTensor([0, 1, 1])
        self.assertTrue(torch.equal(expected_seqs, actual_seqs.float()), "seqs tensor is not matched")

    @weight(2)
    def test22b_len(self):
        """ Test for Checking Seq Tensor Lengths"""
        sample_arrays = [np.array([[1., 1., 0., 0., 0., 0.]]),
                            np.array([[0., 0., 0., 1., 0., 0.],
                                    [1., 0., 1., 0., 0., 0.],
                                    [0., 1., 0., 0., 1., 0.]]),
                            np.array([[0., 1., 0., 0., 0., 0.],
                                    [0., 0., 0., 0., 0., 1.]])]
        sample_labels = [1, 0, 1]

        sample_batch = []
        for array, label in zip(sample_arrays, sample_labels):
            sp_array = sp.sparse.coo_matrix(array)

            # i = torch.LongTensor([sp_array.row, sp_array.col])
            # v = torch.FloatTensor(sp_array.data)
            # s = torch.sparse.FloatTensor(i, v, sp_array.shape)

            sample_batch.append((sp_array, label))

        sparse_failed = False
        try:
            (actual_seqs, actual_lengths), actual_labels = visit_collate_fn(sample_batch)
        except:
            sparse_failed = True

        if sparse_failed:
            dense_sample_batch = []
            for sp_array, label in sample_batch:
                dense_sample_batch.append((np.asarray(sp_array.todense()), label))
            (actual_seqs, actual_lengths), actual_labels = visit_collate_fn(dense_sample_batch)

        expected_seqs = torch.FloatTensor(
            [[[0., 0., 0., 1., 0., 0.],
            [1., 0., 1., 0., 0., 0.],
            [0., 1., 0., 0., 1., 0.]],
            [[0., 1., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 1.],
            [0., 0., 0., 0., 0., 0.]],
            [[1., 1., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0.]]])
        expected_lengths = torch.LongTensor([3, 2, 1])
        expected_labels = torch.LongTensor([0, 1, 1])
        self.assertTrue(torch.equal(expected_lengths, actual_lengths), "lengths tensor is not matched")

    @weight(2)
    def test22b_labels(self):
        """ Test for Checking Seq Tensor Labels"""
        sample_arrays = [np.array([[1., 1., 0., 0., 0., 0.]]),
                            np.array([[0., 0., 0., 1., 0., 0.],
                                    [1., 0., 1., 0., 0., 0.],
                                    [0., 1., 0., 0., 1., 0.]]),
                            np.array([[0., 1., 0., 0., 0., 0.],
                                    [0., 0., 0., 0., 0., 1.]])]
        sample_labels = [1, 0, 1]

        sample_batch = []
        for array, label in zip(sample_arrays, sample_labels):
            sp_array = sp.sparse.coo_matrix(array)

            # i = torch.LongTensor([sp_array.row, sp_array.col])
            # v = torch.FloatTensor(sp_array.data)
            # s = torch.sparse.FloatTensor(i, v, sp_array.shape)

            sample_batch.append((sp_array, label))

        sparse_failed = False
        try:
            (actual_seqs, actual_lengths), actual_labels = visit_collate_fn(sample_batch)
        except:
            sparse_failed = True

        if sparse_failed:
            dense_sample_batch = []
            for sp_array, label in sample_batch:
                dense_sample_batch.append((np.asarray(sp_array.todense()), label))
            (actual_seqs, actual_lengths), actual_labels = visit_collate_fn(dense_sample_batch)

        expected_seqs = torch.FloatTensor(
            [[[0., 0., 0., 1., 0., 0.],
            [1., 0., 1., 0., 0., 0.],
            [0., 1., 0., 0., 1., 0.]],
            [[0., 1., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 1.],
            [0., 0., 0., 0., 0., 0.]],
            [[1., 1., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0.]]])
        expected_lengths = torch.LongTensor([3, 2, 1])
        expected_labels = torch.LongTensor([0, 1, 1])
        self.assertTrue(torch.equal(expected_labels, actual_labels), "labels tensor is not matched")
        
    @weight(1)
    def test_23a_code(self):
        """ 2.3 (a) Submission Check """
        codemap = build_codemap_for_test()
        num_features = min(max(codemap.values()) + 1, len(codemap))
        PATH_VALIDATION = "/autograder/source/data/mortality/validation/"
        validation_ids, validation_labels, validation_seqs = create_dataset(PATH_VALIDATION, codemap, convert_icd9_for_test)

        dataset = VisitSequenceWithLabelDataset(validation_seqs, validation_labels, num_features)
        loader = torch.utils.data.DataLoader(dataset=dataset, batch_size=32, shuffle=False, collate_fn=visit_collate_fn,
                                            num_workers=0)

        model = torch.load("/autograder/source/output/mortality/MyVariableRNN.pth")
        self.assertEqual(1,1, "Error in loading MyVariableRNN.pth model or creating loader")

    @weight(1)
    def test_23a_acc_70(self):
        """2.3 (a) Accuracy more than 70"""
        codemap = build_codemap_for_test()
        num_features = min(max(codemap.values()) + 1, len(codemap))
        PATH_VALIDATION = "/autograder/source/data/mortality/validation/"
        validation_ids, validation_labels, validation_seqs = create_dataset(PATH_VALIDATION, codemap, convert_icd9_for_test)

        dataset = VisitSequenceWithLabelDataset(validation_seqs, validation_labels, num_features)
        loader = torch.utils.data.DataLoader(dataset=dataset, batch_size=32, shuffle=False, collate_fn=visit_collate_fn,
                                            num_workers=0)
        model = torch.load("/autograder/source/output/mortality/MyVariableRNN.pth")
        accuracy = round(model_eval(model, loader)) # eval_mortality_model(model)
        self.assertGreaterEqual(accuracy, 70.0)

    @weight(1)
    def test_23a_acc_65(self):
        """ 2.3 (a) Accuracy more than 65"""
        codemap = build_codemap_for_test()
        num_features = min(max(codemap.values()) + 1, len(codemap))
        PATH_VALIDATION = "/autograder/source/data/mortality/validation/"
        validation_ids, validation_labels, validation_seqs = create_dataset(PATH_VALIDATION, codemap, convert_icd9_for_test)

        dataset = VisitSequenceWithLabelDataset(validation_seqs, validation_labels, num_features)
        loader = torch.utils.data.DataLoader(dataset=dataset, batch_size=32, shuffle=False, collate_fn=visit_collate_fn,
                                            num_workers=0)
        model = torch.load("/autograder/source/output/mortality/MyVariableRNN.pth")
        accuracy = round(model_eval(model, loader)) # eval_mortality_model(model)
        self.assertGreaterEqual(accuracy, 65.0)
    
    @weight(1)
    def test_23a_acc_55(self):
        """2.3 (a) Accuracy more than 55"""
        codemap = build_codemap_for_test()
        num_features = min(max(codemap.values()) + 1, len(codemap))
        PATH_VALIDATION = "/autograder/source/data/mortality/validation/"
        validation_ids, validation_labels, validation_seqs = create_dataset(PATH_VALIDATION, codemap, convert_icd9_for_test)

        dataset = VisitSequenceWithLabelDataset(validation_seqs, validation_labels, num_features)
        loader = torch.utils.data.DataLoader(dataset=dataset, batch_size=32, shuffle=False, collate_fn=visit_collate_fn,
                                            num_workers=0)
        model = torch.load("/autograder/source/output/mortality/MyVariableRNN.pth")
        accuracy = round(model_eval(model, loader)) # eval_mortality_model(model)
        self.assertGreaterEqual(accuracy, 55.0)

    @weight(1)
    def test_23a_acc_50(self):
        """2.3 (a) Accuracy more than 50"""
        codemap = build_codemap_for_test()
        num_features = min(max(codemap.values()) + 1, len(codemap))
        PATH_VALIDATION = "/autograder/source/data/mortality/validation/"
        validation_ids, validation_labels, validation_seqs = create_dataset(PATH_VALIDATION, codemap, convert_icd9_for_test)

        dataset = VisitSequenceWithLabelDataset(validation_seqs, validation_labels, num_features)
        loader = torch.utils.data.DataLoader(dataset=dataset, batch_size=32, shuffle=False, collate_fn=visit_collate_fn,
                                            num_workers=0)
        model = torch.load("/autograder/source/output/mortality/MyVariableRNN.pth")
        accuracy = round(model_eval(model, loader)) # eval_mortality_model(model)
        self.assertGreaterEqual(accuracy, 50.0)

    @weight(1)
    def test_23a_acc_45(self):
        """2.3 (a) Accuracy more than 45"""
        codemap = build_codemap_for_test()
        num_features = min(max(codemap.values()) + 1, len(codemap))
        PATH_VALIDATION = "/autograder/source/data/mortality/validation/"
        validation_ids, validation_labels, validation_seqs = create_dataset(PATH_VALIDATION, codemap, convert_icd9_for_test)

        dataset = VisitSequenceWithLabelDataset(validation_seqs, validation_labels, num_features)
        loader = torch.utils.data.DataLoader(dataset=dataset, batch_size=32, shuffle=False, collate_fn=visit_collate_fn,
                                            num_workers=0)
        model = torch.load("/autograder/source/output/mortality/MyVariableRNN.pth")
        accuracy = round(model_eval(model, loader)) # eval_mortality_model(model)
        self.assertGreaterEqual(accuracy, 45.0)

    @weight(1)
    def test_23a_acc_40(self):
        """2.3 (a) Accuracy more than 40"""
        codemap = build_codemap_for_test()
        num_features = min(max(codemap.values()) + 1, len(codemap))
        PATH_VALIDATION = "/autograder/source/data/mortality/validation/"
        validation_ids, validation_labels, validation_seqs = create_dataset(PATH_VALIDATION, codemap, convert_icd9_for_test)

        dataset = VisitSequenceWithLabelDataset(validation_seqs, validation_labels, num_features)
        loader = torch.utils.data.DataLoader(dataset=dataset, batch_size=32, shuffle=False, collate_fn=visit_collate_fn,
                                            num_workers=0)
        model = torch.load("/autograder/source/output/mortality/MyVariableRNN.pth")
        accuracy = round(model_eval(model, loader)) # eval_mortality_model(model)
        self.assertGreaterEqual(accuracy, 40.0)

    @weight(1)
    def test_23a_acc_35(self):
        """2.3 (a) Accuracy more than 35"""
        codemap = build_codemap_for_test()
        num_features = min(max(codemap.values()) + 1, len(codemap))
        PATH_VALIDATION = "/autograder/source/data/mortality/validation/"
        validation_ids, validation_labels, validation_seqs = create_dataset(PATH_VALIDATION, codemap, convert_icd9_for_test)

        dataset = VisitSequenceWithLabelDataset(validation_seqs, validation_labels, num_features)
        loader = torch.utils.data.DataLoader(dataset=dataset, batch_size=32, shuffle=False, collate_fn=visit_collate_fn,
                                            num_workers=0)
        model = torch.load("/autograder/source/output/mortality/MyVariableRNN.pth")
        accuracy = round(model_eval(model, loader)) # eval_mortality_model(model)
        self.assertGreaterEqual(accuracy, 35.0)

    @weight(1)
    def test_23a_acc_30(self):
        """2.3 (a) Accuracy more than 30"""
        codemap = build_codemap_for_test()
        num_features = min(max(codemap.values()) + 1, len(codemap))
        PATH_VALIDATION = "/autograder/source/data/mortality/validation/"
        validation_ids, validation_labels, validation_seqs = create_dataset(PATH_VALIDATION, codemap, convert_icd9_for_test)

        dataset = VisitSequenceWithLabelDataset(validation_seqs, validation_labels, num_features)
        loader = torch.utils.data.DataLoader(dataset=dataset, batch_size=32, shuffle=False, collate_fn=visit_collate_fn,
                                            num_workers=0)
        model = torch.load("/autograder/source/output/mortality/MyVariableRNN.pth")
        accuracy = round(model_eval(model, loader)) # eval_mortality_model(model)
        self.assertGreaterEqual(accuracy, 30.0)