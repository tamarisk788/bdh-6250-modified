#!/usr/bin/env bash
apt-get update
apt-get install  -y python3 python3-pip python3-dev
# apt-get install -y python python-pip python-dev
pip3 install -r /autograder/source/requirements.txt
