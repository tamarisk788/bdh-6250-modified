#!/bin/bash

# clean up
rm -rf hw2
rm -rf hw2.tar.gz

# copy data
mkdir -p hw2/data
cp data/events.csv hw2/data/
cp data/mortality.csv hw2/data/

# download code from github
# Changed to include code
cp -r code hw2/

cp writeup/homework2.* hw2/

# clean up unnecessary files
./cleanup.sh

# create tar
tar cvzf hw2.tar.gz hw2/
