import pickle

# with open("models_py2/part-00001", "rb") as f:
#         lines = [line.strip() for line in f]
#         print(lines)
#         content = '\n'.join(lines)   #expected str instance, bytes found
#         print(content)
#         classifier = pickle.load(content)
#         # print(classifier)


# with open("models/part-00001", "rb") as f:
#         lines = [line.strip() for line in f]
#         print(lines)
#         lines = [line.decode(encoding="utf-8", errors="ignore") for line in lines]
#         # content = '\n'.join(lines)   ## expected str instance, bytes found
#         # lines = [line.decode(encoding="utf-8", errors="ignore") for line in content]
#         print(lines)


#         # lines = [line.decode(encoding="utf-8", errors="ignore").strip() for line in f]
#         # content = '\n'.join(lines)
#         # print(content)
#         # # classifier = pickle.load(content.encode('utf-8'))  #TypeError: file must have 'read' and 'readline' attributes
#         # classifier = pickle.load(content)   #TypeError: file must have 'read' and 'readline' attributes
#         # # print(classifier)


################# comparison(same) ##############
 
# with open("model.txt", "rb") as f:
# 		##should remove readline() when running pickle.load since this operation will affect the model.txt line by line
# 		# print(f.readline())  #b'\x80\x03clrsgd\n'
# 		# print(f.readline())  #b'LogisticRegressionSGD\n'
# 		# print(f.readline())  #b'q\x00)\x81q\x01}q\x02(X\x03\x00\x00\x00etaq\x03G?\x84z\xe1G\xae\x14{X\x06\x00\x00\x00weightq\x04]q\x05(G\xbf{\xb0\x84R\xcf\x07\xa6G\x00\x00\x00\x00\x00\x00\x00\x00G?\x82\xb3\xf48W\x1dHG\xbfl\x8a\xa7\xa3\x9b\x9e\xfeG\xbfl\x02\xd2\xf0\xeb\xd9\xe4G\xbf\x9f\x9d\xb4Rr(BG\xbf`6\xf3nH\x95\xeeG\xbf\xb1\x16/,\xff\x8dEG\xbf\x81\xdfm|\xbf\x13=G?y\xe6=1b\x0b\x88G\xbf=*ivIX\xd8G\xbf\x89*\x05EF,\xc8G\xbfr\xcd\xcd\xc6\xab\xae2G\xbf\x89\xa6D\x11\x95\xbb\xc6G?}\xdc\x1a\x08\xed9\xd7G\xbf\x84\xe7t\xe5;\xb0dG\xbf}4V\xc2\xc2+\x90G\xbfrF\xd6\xe1$\x0c\xffG\xbfx\xbe\xb3n\xe4\xa0\xdcG?\x82/\x9b#+/IG\xbf\x91\x04\xce\x9f\xfc\x8f\xd3G\xbfk|\xad\xa4\x0b\x08{G\xbfs\xdc\xafMQ\xfc\xe7G\xbfq\xe5{\xb3\xb3A\xedG\xbf\x93\xe6\x91\x9d18IG?b\x16N?f\xaa G?wIe\x04\x1f&\x9fG?}1\x93\xac+g\xb8G\xbfj\x0e\n'
#         # classifier = pickle.loads(f) #ModuleNotFoundError: No module named 'lrsgd'  TypeError: a bytes-like object is required, not '_io.BufferedReader'
#         print(f)   #_io.BufferedReader name='model.txt'>
#         # print(b'q\x00)\x81q\x01}q\x02(X\x03\x00\x00\x00etaq\x03G?\x84z\xe1G\xae\x14{X\x06\x00\x00\x00weightq\x04]q\x05(G\xbf{\xb0\x84R\xcf\x07\xa6G\x00\x00\x00\x00\x00\x00\x00\x00G?\x82\xb3\xf48W\x1dHG\xbfl\x8a\xa7\xa3\x9b\x9e\xfeG\xbfl\x02\xd2\xf0\xeb\xd9\xe4G\xbf\x9f\x9d\xb4Rr(BG\xbf`6\xf3nH\x95\xeeG\xbf\xb1\x16/,\xff\x8dEG\xbf\x81\xdfm|\xbf\x13=G?y\xe6=1b\x0b\x88G\xbf=*ivIX\xd8G\xbf\x89*\x05EF,\xc8G\xbfr\xcd\xcd\xc6\xab\xae2G\xbf\x89\xa6D\x11\x95\xbb\xc6G?}\xdc\x1a\x08\xed9\xd7G\xbf\x84\xe7t\xe5;\xb0dG\xbf}4V\xc2\xc2+\x90G\xbfrF\xd6\xe1$\x0c\xffG\xbfx\xbe\xb3n\xe4\xa0\xdcG?\x82/\x9b#+/IG\xbf\x91\x04\xce\x9f\xfc\x8f\xd3G\xbfk|\xad\xa4\x0b\x08{G\xbfs\xdc\xafMQ\xfc\xe7G\xbfq\xe5{\xb3\xb3A\xedG\xbf\x93\xe6\x91\x9d18IG?b\x16N?f\xaa G?wIe\x04\x1f&\x9fG?}1\x93\xac+g\xb8G\xbfj\x0e\n'.decode('utf-16'))
#         # print(f.readlines()[-1])
#         print(type(f))     ########<class '_io.BufferedReader'>
#         # print(f.readlines())  ####  \x00\x00\x00\x00\x00ub.'] end of last line
#         classifier = pickle.load(f)  #right!!  class 'lrsgd.LogisticRegressionSGD'
#         # print(classifier.weight)
#         print(classifier)   #<lrsgd.LogisticRegressionSGD object at 0x7f30c40dae10>
#         print(type(classifier))    #right!! class 'lrsgd.LogisticRegressionSGD'
#         # print(f)    #<_io.BufferedReader name='model.txt'>
#         ##print(f.readline())    # b''  why output null after loading? normal before loading
#         ##print(f.readline())    # b''


# with open('../models/part-00001', 'rb') as f:
# 	print(f)        #<_io.BufferedReader name='../models/part-00000'>
# 	print(type(f))  #<class '_io.BufferedReader'>
# 	print(f.readline())  #b'\x80\x03clrsgd\t\n'
# 	print(f.readline())  #b'LogisticRegressionSGD\t\n'
# 	print(f.readline())  #b'q\x00)\x81q\x01}q\x02(X\x03\x00\x00\x00etaq\x03G?\x84z\xe1G\xae\x14{X\x06\x00\x00\x00weightq\x04]q\x05(G\xbfr\x8c\xde`\x9c9\xe7G\x00\x00\x00\x00\x00\x00\x00\x00G\x00\x00\x00\x00\x00\x00\x00\x00G\x00\x00\x00\x00\x00\x00\x00\x00G\x00\x00\x00\x00\x00\x00\x00\x00G\xbf\xa3\xad%\x19q\x05\x82G\xbfq\xb2\xa2\xa2\xf5\xfa\xdbG\xbf\xa6\xed\x13\xb6%d\x8bG\x00\x00\x00\x00\x00\x00\x00\x00G\x00\x00\x00\x00\x00\x00\x00\x00G\xbfq\xf7~\xb2\xd2~\x0fG\xbfn{\xbcC-)\xcaG\x00\x00\x00\x00\x00\x00\x00\x00G\xbfr\x94\xf89\xe6A\xd6G\x00\x00\x00\x00\x00\x00\x00\x00G\xbf\x7fq\t\n'
# 	print(f.readline(-1))
################# comparison(same) ################


# with open("../models/part-00001", "rb") as f:
# 	print(f.readline())        #b'\x80\x03clrsgd\t\n'
# 	print(f.readline())        #b'LogisticRegressionSGD\t\n'
# 	print(f.readline())        #b'q\x00)\x81q\x01}q\x02(X\x03\x00\x00\x00etaq\x03G?\x84z\xe1G\xae\x14{X\x06\x00\x00\x00weightq\x04]q\x05(G\xbfr\x8c\xde`\x9c9\xe7G\x00\x00\x00\x00\x00\x00\x00\x00G\x00\x00\x00\x00\x00\x00\x00\x00G\x00\x00\x00\x00\x00\x00\x00\x00G\x00\x00\x00\x00\x00\x00\x00\x00G\xbf\xa3\xad%\x19q\x05\x82G\xbfq\xb2\xa2\xa2\xf5\xfa\xdbG\xbf\xa6\xed\x13\xb6%d\x8bG\x00\x00\x00\x00\x00\x00\x00\x00G\x00\x00\x00\x00\x00\x00\x00\x00G\xbfq\xf7~\xb2\xd2~\x0fG\xbfn{\xbcC-)\xcaG\x00\x00\x00\x00\x00\x00\x00\x00G\xbfr\x94\xf89\xe6A\xd6G\x00\x00\x00\x00\x00\x00\x00\x00G\xbf\x7fq\t\n'
# 	


# with open("../models/part-00001", "r") as f:
#         # lines = [line.decode(encoding="utf-8", errors="ignore") for line in f]
#         # print(lines)
#         # lines = [line.strip() for line in lines]
#         # #print(lines.readline())    #'list' object has no attribute 'readline'
#         # print(lines)
#         # content = '\n'.join(lines)
#         # #print(content.readline())   #'str' object has no attribute 'readline'
#         # print(content)
#         lines = [line.strip() for line in f]
#         # print(lines)
#         # lines = [line.decode(encoding="utf-8", errors="ignore") for line in lines]
#         # # print(lines)
#         # content = "\n".join(lines).encode('utf-8')
#         # # print(content)
#         # classifier = pickle.load(content)

#         # contents = f.read()
#         # contents = contents.decode().strip('\t\n')

###-------$$$$

# with open('../models/part-00001', 'rb') as f:
# 	# contents = f.read()
# 	# contents = contents.split(b'\t\n')
# 	# contents = contents[:-1]
# 	# # contents = [content.decode(encoding="utf-8", errors="ignore") for content in contents]
# 	# # print(contents)

# 	# # contents = '\n'.join(contents)   #change a lot
# 	# # contents = [content.encode('utf-8') for content in contents]  #wrong
# 	# # print(contents)
# 	# classifier = pickle.load(contents)

#     # content_list = [x.decode(encoding="utf-8", errors="ignore").strip() for x in f.readlines()]
#     # # print(content_list)
#     # content_list = '\n'.join(content_list)
#     # print(content_list)
#     # content_list = [content.encode('utf-8') for content in content_list]
#     # classifier = pickle.load(content_list)

#     content_list = [x.decode(encoding="utf-8", errors="ignore").replace('\t\n', '\n').encode('utf-8') for x in f.readlines()]
#     # # print(content_list)
#     with open('outfile','wb') as fp:
#         pickle.dump(content_list, fp)
#     print(type(fp))    #<class '_io.BufferedWriter'>
#     print(fp.weight)   #'_io.BufferedWriter' object has no attribute 'weight'
#     # # with open('outfile','rb') as fp:
#     # #     classifier = pickle.load(fp)
#     # # # print(type(classifier))
#     # # # print(classifier.weight)

#     # classifier = pickle.loads(content_list)
#     # print(type(classifier))
#     # print(classifier.weight)   #a bytes-like object is required, not 'list'

#########latest(works !!!)
with open('../models/part-00002', 'rb') as f:
    # print(f.readline().strip())
    # print(type(f.readline()))
    content_list = [x.replace(b'\t\n', b'\n') for x in f.readlines()]
    # content_list = [x.strip()+b'\n' for x in f.readlines()]
    # content_list[-1] = content_list[-1].strip()
    # print(content_list)
    # print(content_list[-1])
    # # print(content_list[-1].strip().decode('utf-8', errors= 'replace'))# = content_list[-1].strip()
    # # print(b'\n'.decode('utf-8'))
    # # print('\t\n' == b'\t\n'.decode('utf-8'))   #True
    # # content_list[-1] = content_list[-1].strip()
    # # print(content_list[0])
    # # print(content_list[1])

with open('pickled.txt', 'wb') as ufile:
    for line in content_list:
        ufile.write(line)
    print(type(ufile))    #<class '_io.BufferedWriter'>
    print(ufile)          #<_io.BufferedWriter name='tmp.txt'>
with open('pickled.txt', 'rb') as ifile:  
	# print(ifile.readline())
	# print(ifile.readline())
	# print(ifile.readline())
	# print(ifile.readlines())
	print(ifile)          #<_io.BufferedWriter name='tmp.txt'>
	print(type(ifile))    #<class '_io.BufferedReader'>
	classifier = pickle.load(ifile)  #_pickle.UnpicklingError: invalid load key, '\x00'.  
	print(type(classifier), classifier.weight, len(classifier.weight))
# ###tp = pickle.dumps(content_list)
# ###aa = pickle.loads(tp)
# ###print(type(aa))    #<class 'list'>  not our expected <class '_io.BufferedReader'>

# # with open('tmp.txt', 'wb') as tp:   # gan !
# # 	pickle.dump(content_list, tp)
# # print(type(tp))           #<class '_io.BufferedWriter'>

# with open('tmp.txt', 'rb') as tp:
# 	classifier = pickle.load(tp)
# print(type(classifier))   #<class 'list'>
# print(classifier.weight)

########latest

########alternative (works as well !!!!)########may due to 'wt' mode leading to information loss in transformation

# with open('../models/part-00001', 'rb') as f:
# 	# content_list = [x.strip()+b'\n' for x in f.readlines()]
# 	## print(f.readlines()[-1])
# 	## print(b'\xbf'.decode(encoding = 'utf-8', errors = 'ignore'))
# 	#####print(type(f.readlines()))   #<class 'list'>

# 	with open('pickled.pkl', 'wt', encoding = 'utf-8') as myfile:  #'wt' mode, because '\n'.join requires str instead of bytes
# 		# myfile.write('\n'.join(str(line.strip()) for line in f.readlines()))
# 		myfile.write('\n'.join(line.strip().decode('utf-8', 'ignore') for line in f.readlines()))
# 	with open('pickled.pkl', 'rb') as ifile:
# 		# print(ifile.readline())
# 		# print(ifile.readline())
# 		# print(ifile.readline())
# 		# print(ifile.readline())
# 		classifier = pickle.load(ifile)  #_pickle.UnpicklingError: invalid load key, '\x03' for 'utf-8' mode, '\x00' for 'latin1' mode
# 		print(classifier.weight)
# 		print(type(classifier))
########alternative######## may due to 'wt' mode leading to information loss in transformation



# a = ['a','b','c']
# file = '\n'.join(a)
# myfile = '\n'.join(f for f in a)
# print(type(a))       ##<class 'list'>
# print(type('a'))     #<class 'str'>
# print(type(file))    #<class 'str'>, (which means a list of 'str's results in type 'str' by join)
# print(type(myfile))  #<class 'str'>
# print(file)
# print(myfile)


# with open('model.txt', encoding = 'utf-8', errors = 'ignore') as f:
#     # classifier = pickle.load(f)
#     # print(type(classifier))
#     # print(f.readline().strip())
#     # print(f.readline())
#     classifier = pickle.loads(f)



# import io
# sample_bytes = bytes(['this is a sample bytearray', 'haha'],'utf-8')
# print(sample_bytes)   #### encoding without a string argument





#####key#####  remove '\t' in _io.BufferedReader line by line in python3 ??
# with open('../models/part-00000', 'rb') as f:
# 	classifier = pickle.load(f)   #ModuleNotFoundError: No module named 'lrsgd\t'


# test = ['a','n','b']
# test = '\n'.join(test)
# print(test)
# print(type(test))

# a = b'foo \t\nfoool\t\n'
# print(a.split(b'\t\n'))

