import sys
import os
import pickle

from optparse import OptionParser

from sklearn.metrics import roc_curve, auc
from sklearn.datasets import load_svmlight_file

import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt

from lrsgd import LogisticRegressionSGD
from utils import parse_svm_light_data


aaa = ([(427, 0.5), (452, 0.5), (526, 0.5), (746, 0.125), (777, 0.333333), (829, 0.25), (865, 0.166667), (1157, 0.5), (1212, 0.25), (1345, 0.333333), (1806, 0.333333), (1817, 0.25), (1854, 1.0), (2524, 0.058824), (2530, 0.041667), (2549, 0.111111), (2597, 0.055556), (2633, 0.4), (2646, 0.083333), (2677, 0.066667), (2748, 0.285714), (2804, 0.5), (2842, 0.4), (2886, 0.133333), (2891, 0.012195), (2904, 0.055556), (2915, 0.058824), (2991, 0.015625), (2992, 0.010989), (3004, 0.083333), (3032, 0.2), (3212, 0.222222), (3230, 0.028169), (3243, 0.2), (3253, 0.166667), (3256, 0.25), (3265, 0.012346), (3272, 0.012766), (3273, 0.002801), (3278, 0.021622), (3292, 0.013699), (3294, 0.020202), (3300, 0.013699), (3311, 0.018223), (3336, 0.019444), (3337, 0.002786), (3346, 0.011342), (3352, 0.041667), (3366, 0.017078), (3374, 0.041667), (3379, 0.013304), (3392, 0.01626), (3406, 0.017361), (3411, 0.018605), (3412, 0.002778), (3421, 0.012048), (3431, 0.018059), (3434, 0.005882), (3440, 0.020785), (3455, 0.017582), (3468, 0.018018), (3492, 0.142857), (3501, 0.021231), (3508, 0.002625), (3514, 0.013699), (3528, 0.013699), (3552, 0.020202), (3565, 0.043478), (3573, 0.013699), (3588, 0.022472), (3592, 0.018141)], 1)
bbb = ([(162, 0.25), (242, 0.2), (706, 1.0), (818, 0.5), (948, 0.333333), (950, 0.333333), (1273, 0.333333), (1779, 0.25), (1952, 0.066667), (2013, 0.5), (2025, 0.071429), (2192, 0.5), (2258, 0.5), (2512, 0.25), (2516, 0.2), (2524, 0.058824), (2530, 0.041667), (2569, 0.055556), (2651, 0.111111), (2677, 0.066667), (2735, 0.047619), (2838, 0.058824), (2915, 0.058824), (2955, 0.066667), (2975, 0.08), (2991, 0.015625), (3004, 0.083333), (3023, 0.002237), (3032, 0.1), (3044, 0.166667), (3106, 0.010526), (3149, 0.25), (3159, 0.285714), (3185, 0.086957), (3203, 0.133333), (3212, 0.111111), (3218, 0.058824), (3265, 0.012346), (3272, 0.021277), (3273, 0.008403), (3278, 0.027027), (3286, 0.014815), (3292, 0.022831), (3294, 0.037037), (3300, 0.022831), (3311, 0.022779), (3316, 0.032258), (3321, 0.004651), (3329, 0.027778), (3332, 0.016529), (3336, 0.027778), (3337, 0.008357), (3342, 0.041885), (3346, 0.018904), (3352, 0.041667), (3366, 0.018975), (3379, 0.022173), (3392, 0.0271), (3406, 0.038194), (3411, 0.023256), (3412, 0.008333), (3421, 0.012048), (3431, 0.022573), (3434, 0.005882), (3440, 0.023095), (3443, 0.021739), (3450, 0.014815), (3455, 0.021978), (3461, 0.015385), (3468, 0.022523), (3488, 0.021739), (3501, 0.021231), (3508, 0.007874), (3514, 0.022831), (3528, 0.022831), (3552, 0.037037), (3573, 0.022831), (3592, 0.022676), (3611, 0.008333), (3612, 0.043956)], 1)
input = [aaa, bbb]


def load_model(path):
    # # with open(path, 'rb') as f:   #python 2
    # with open(path, 'r') as f:    #python 3
    #     lines = [line.strip() for line in f]
    #     content = '\n'.join(lines)
    #     # content = [line.decode('utf-8').strip() for line in f.readlines()]

    #     # classifier = pickle.loads(content.encode('ascii'))   #python 3
    #     classifier = pickle.load(content.encode(encoding))   #python 3
    #     # classifier = pickle.load(bytes(content, 'utf-8'))
    #     return classifier

    ###second reference 
    # with open(path, 'rb') as f:
    #     classifier =  pickle.load(f)
    #     return classifier     #ModuleNotFoundError: No module named 'lrsgd\t'

    with open(path, 'rb') as f:
        content_list = [x.replace(b'\t\n', b'\n') for x in f.readlines()]
    with open('pickled.txt', 'wb') as ufile:
        for line in content_list:
            ufile.write(line)
    with open('pickled.txt', 'rb') as ifile:
        classifier = pickle.load(ifile)
        print(len(classifier.weight))
    return classifier

def predict_prob(classifiers, X):
    """
    Given a list of trained classifiers,
    predict the probability of positive label.
    """
    y_probs = []
    for classifier in classifiers:
        y_prob = classifier.predict_prob(X)
        y_probs.append(y_prob)
    print(y_probs, sum(y_probs), len(y_probs))
    return sum(y_probs)*1.0/len(y_probs)

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-m", "--model-path", action="store", dest="path",
                      default="models", help="path where trained classifiers are saved")
    parser.add_option("-r", "--result", action="store", dest="result",
                      default="roc", help="name of the figure")
    
    options, args = parser.parse_args(sys.argv)

    files = [options.path + "/" +
             filename for filename in os.listdir(options.path) if filename.startswith('part')]
    print(files)
    # classifiers = []
    # for file in files:
    #     classifier = load_model(file)
    #     classifiers.append(classifier)
    # classifiers = map(load_model, files)
    classifiers = list(map(load_model, files))
    print(classifiers)
    y_test_prob = []
    y_test = []
    for X, y in input:
    	y_prob = predict_prob(classifiers, X)
    	print(y_prob)
    	y_test_prob.append(y_prob)



