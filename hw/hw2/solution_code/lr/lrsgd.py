#!/usr/bin/env python
"""
Implement your own version of logistic regression with stochastic
gradient descent.

Author: Meera Kamath
Email : mkamath6@gatech.edu
"""

import collections
import math

bias = False  # use bias to represent if intercept b in logistic regression is included or not
# bias = True #use this option to control if intercept exists or not
class LogisticRegressionSGD:

    def __init__(self, eta, mu, n_feature):
        """
        Initialization of model parameters
        """
        self.eta = eta
        self.weight = [0.0] * (n_feature + int(bias))
        self.mu = mu

    def fit(self, X, y):
        """
        Update model using a pair of training sample
        """
        n_feature = len(self.weight)
        p = self.predict_prob(X)

        # Update weights with stochastic gradient descent(use L2 norm regularization to conquer overfitting)
        for j in range(n_feature - int(bias)): # count intercept b when bias= True, and no intercept when bias = False
            self.weight[j] += -2*self.mu*self.eta*self.weight[j]
        for j in range(len(X)):
            # self.weight[X[j][0]] += -2*self.mu*self.eta*self.weight[X[j][0]] # Common error, only update regularized non-zero terms
            self.weight[X[j][0]] += self.eta*((y - p)*X[j][1])
        if bias:
            self.weight[-1] += self.eta*((y - p)*1)

    def predict(self, X):
        return 1 if self.predict_prob(X) > 0.5 else 0

    def predict_prob(self, X):
        if bias:
            return 1.0 / (1.0 + math.exp(-math.fsum((self.weight[f]*v for f, v in X)) - self.weight[-1]*1))
        else:
            return 1.0 / (1.0 + math.exp(-math.fsum((self.weight[f]*v for f, v in X))))
