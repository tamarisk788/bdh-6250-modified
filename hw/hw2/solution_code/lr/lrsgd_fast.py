#!/usr/bin/env python
"""
Implement your own version of logistic regression with stochastic
gradient descent.

Author: Peter Schneider
Email : peteryschneider@gatech.edu
"""

import collections
import math


class LogisticRegressionSGD:

    def __init__(self, eta, mu, n_feature):
        """
        Initialization of model parameters
        """
        self.eta = eta
        self.weight = [0.0] * n_feature
        self.mu = mu
        # initialize last update time-step to zero, first time-step is one
        self.last_update = [0.0] * n_feature
        self.t = 1
    
    def fit(self, X, y):
        """
        Update model using a training sample pair
        
        As described in the homework, with no clipping. Will get math overflow when calculating sigmoid for large
        eta/mu combinations.
        """
        # calculate residual term
        weight_dot_x = sum(self.weight[feature_id - 1] * feature_value for feature_id, feature_value in X)
        residual = y - self.sigmoid(weight_dot_x)
        
        for feature_id, feature_value in X:
            # update weight from normalization term, consider how many time-steps from last update
            self.weight[feature_id - 1] -= (self.t - self.last_update[feature_id - 1]) * \
                self.eta * 2 * self.mu * self.weight[feature_id - 1]
            # update weight from residual term
            self.weight[feature_id - 1] += self.eta * (feature_value * residual)
            # store off last update t
            self.last_update[feature_id - 1] = self.t
        
        self.t += 1

    def fit_clipped(self, X, y):
        """
        Update model using a training sample pair
        
        Clip weight at zero when applying normalization if this would cause the weight to pass through zero
        (change signs). Also helps avoids math overflow when calculating sigmoid for larger eta/mu combinations.
        """
        # calculate residual term
        weight_dot_x = sum(self.weight[feature_id - 1] * feature_value for feature_id, feature_value in X)
        residual = y - self.sigmoid(weight_dot_x)

        for feature_id, feature_value in X:
            # update weight from normalization term, consider how many time-steps from last update
            # clip weight at zero if the normalization would cause the weight to pass through zero (change signs)
            if abs((self.t - self.last_update[feature_id - 1]) *
                    self.eta * 2 * self.mu * self.weight[feature_id - 1]) > abs(self.weight[feature_id - 1]):

                self.weight[feature_id - 1] = 0

            else:
                self.weight[feature_id - 1] -= (self.t - self.last_update[feature_id - 1]) * \
                    self.eta * 2 * self.mu * self.weight[feature_id - 1]

            # update weight from residual term
            self.weight[feature_id - 1] += self.eta * (feature_value * residual)
            # store off last update t
            self.last_update[feature_id - 1] = self.t

        self.t += 1
         
    def predict(self, X):
        return 1 if self.predict_prob(X) > 0.5 else 0

    def predict_prob(self, X):
        return 1.0 / (1.0 + math.exp(-math.fsum((self.weight[f - 1]*v for f, v in X))))

    @staticmethod
    def sigmoid(t):
        return 1 / float(1 + math.exp(-t))

