# Deletes .DS_Store files
find ./ -type f -name ".DS_Store" -delete

# Delete ._ dot underscore files
find ./ -type f -name "._*" -delete

# Delete .FBCIndex files
find ./ -type f -name ".FBC*" -delete


# Optional commands (off by default)
# Clears the Trash for every user that has items in the Trash
find ./ -type d -name ".Trashes" -delete

find ./ -type d -name ".fseventsd" -delete
