from subprocess import call, check_output, CalledProcessError, STDOUT
import os
import sys

# argv[1] training features, argv[2] mapper file . E.g. python grading-hadoop.py pig/training lr/mapper.py
call(['sh','test-hadoop.sh',sys.argv[1], sys.argv[2]])
diff1 = check_output("diff <(cut -f2 expected_mapper) <(cut -f2 mapper) | wc -l", shell=True, stderr=STDOUT, executable='/bin/bash')
res = False;
if diff1 == '0\n':
    res = True
    msg =""
else:
    msg = "Mapper code does not work!"
print res, msg



