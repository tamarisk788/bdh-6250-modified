#!/bin/bash
rm mapper
hdfs dfs -rm -r hw2/training
hdfs dfs -rm -r hw2/mapper
hdfs dfs -put $1 hw2/
#sed -i '/import random/a random.seed(50)' $2

hadoop jar \
/usr/lib/hadoop-mapreduce/hadoop-streaming.jar \
-D mapreduce.job.reduces=0 \
-files ../solution/lr \
-mapper "python "$2" -n 3 -r 0.5 " \
-input hw2/training \
-output hw2/mapper
hdfs dfs -getmerge hw2/mapper/ mapper
sort -o mapper mapper