#!/usr/bin/env bash

# run.sh OR run.sh /abs/path/to/check
# DEBUG ONLY: Stop entire pipeline if anything is crashed
# set -e OR set -o pipefail

# Configuration Variables (change this to match your setup)
GRADING_ROOT=/hw2_grading/code/pig
LOG_DIR=$GRADING_ROOT/../../logs
SOLUTION_DIR=/bdh/hw/hw2/solution_code

# Get current directory and push to top of directory stack
DIR=$(pwd)
pushd $DIR

echo "PROCESSING DIRECTORY: $DIR"
echo "Do you want to delete all folders in this directory?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) echo "Clearing ALL Folders"; rm -Rf -- */; break;;
        No ) echo "Leaving ALL Folders"; break;;
    esac
done

# Create new log file
RECORD_FILE=$DIR/PIG_RECORD.log
> $RECORD_FILE

# Un-tar.gz all archives in directory (tar* for Canvas resubmit naming, e.g. tar-2.gz)
ls *.tar*.gz | xargs -i tar -zxvf {}

for STUDENT_DIR in */; do
    # Break apart folder name (format is $GTID-$USERNAME-$HWNUM )
    STUDENT_GTID=$(echo "$STUDENT_DIR" | cut -d '-' -f 1)
    STUDENT_USERNAME=$(echo "$STUDENT_DIR" | cut -d '-' -f 2)

    echo "GRADING: $STUDENT_USERNAME ($STUDENT_GTID)"
    pushd "$STUDENT_DIR" 
    if [ -d "code/pig" ]; then
        pushd "code/pig"
        if [ -e etl.pig ]; then

            # Copy student files to the grading directory
            cp etl.pig $GRADING_ROOT/
            if [ -e utils.py ]; then
                cp utils.py $GRADING_ROOT/
            else
                # Be nice and copy official utils if student did not provide them
                cp $SOLUTION_DIR/pig/utils.py $GRADING_ROOT/
            fi

            # Move to the grading directory
            pushd "/hw2_grading/code/pig/"
            
            # Remove any existing folders (from previous output)
            rm -Rf -- */

	    # Run entire student code
            pig -x local etl.pig &> "$LOG_DIR/$STUDENT_GTID-$STUDENT_USERNAME-pig.log"
            
            # Run grading on student output and store in $OUTPUT
	    OUTPUT=$(python /bdh/hw/hw2/script/pig/test_pig.py /bdh/hw/hw2/script/pig/expected_sorted_EDT/)

            # Append student grading output to $RECORD_FILE
            echo "$STUDENT_GTID,$STUDENT_USERNAME,$OUTPUT" >> $RECORD_FILE

            # Clean-up files from grading directory
            rm -Rf -- */
            rm etl.pig
            rm utils.py

            popd # EXIT grading directory
        else
            echo "$STUDENT_GTID,$STUDENT_USERNAME,0.0,etl.pig not found" >> $RECORD_FILE
        fi
        popd # EXIT student Pig folder
    else
        echo "$STUDENT_GTID,$STUDENT_USERNAME,0.0,folder pig not found" >> $RECORD_FILE
    fi
    popd # EXIT student folder
done

exit 0
