from subprocess import call, check_output, CalledProcessError, STDOUT
import sys
import os

score = 0

expected_path = sys.argv[1] #'../../tests/expected/'

# actual_path = 'solution_code/pig_copy/'

# def pig_run(pigfilepath):
#     if pigfilepath:
#         call(["sudo", "pig", "-x", "local", str(pigfilepath)])

def indexdate():
    score = 0
    comments = ''

    if os.path.isfile('aliveevents/part-r-00000'):
        # check aliveevents
        call("sort aliveevents/part-r-00000 > aliveevents/actualaliveevents.csv", shell=True)
        diff1 = check_output("diff aliveevents/actualaliveevents.csv " + expected_path + "expectedaliveevents.csv | wc -l", shell=True, stderr=STDOUT)

        if int(diff1) == 0:
            score = 2.0
            comments += 'aliveevents MATCHED '
        else:
            comments += 'aliveevents MISMATCHED '
    else:
        comments += 'ERROR: aliveevents folder not found '

    if os.path.isfile('deadevents/part-r-00000'):
        #check deadevents
        call("sort deadevents/part-r-00000 > deadevents/actualdeadevents.csv", shell=True)
        diff2 = check_output("diff deadevents/actualdeadevents.csv " + expected_path + "expecteddeadevents.csv | wc -l", shell=True, stderr=STDOUT)

        if int(diff2) == 0:
            score += 2.0
            comments += 'deadevents MATCHED '
        else:
            comments += 'deadevents MISMATCHED '
    else:
        comments += 'ERROR: deadevents folder not found '

    if score == 4.0:
        comments += 'test_indexdate: PASS '
    elif score == 2.0:
        comments += 'test_indexdate: PARTIAL PASS '
    else:
        comments += 'test_indexdate: FAILED '

    return score, comments

def filtered():
    score = 0
    comments = ''

    if os.path.isfile('filtered/part-r-00000'):
        # check filtered
        call("sort filtered/part-r-00000 > filtered/actualfiltered.csv", shell=True)
        diff = check_output("diff filtered/actualfiltered.csv " + expected_path + "expectedfiltered.csv | wc -l", shell=True, stderr=STDOUT)

        if int(diff) == 0:
            score = 4.0
    else:
        comments += 'ERROR: filtered folder not found '

    if score == 4.0:
        comments += 'test_filtered: PASS '
    else:
        comments += 'test_filtered: FAILED '

    return score, comments

def features_aggregate():
    score = 0
    comments = ''

    if os.path.isfile('features_aggregate/part-r-00000'):
        # check features_aggregate
        call("sort features_aggregate/part-r-00000 > features_aggregate/actualfeatures_aggregate.csv", shell=True)
        diff = check_output("diff features_aggregate/actualfeatures_aggregate.csv " + expected_path + "expectedfeatures_aggregate.csv | wc -l", shell=True, stderr=STDOUT)

        if int(diff) == 0:
            score = 4.0
    else:
        comments += 'ERROR: features_aggregate folder not found '

    if score == 4.0:
        comments += 'test_features_aggregate: PASS '
    else:
        comments += 'test_features_aggregate: FAILED '

    return score, comments

def features_map():
    score = 0
    comments = ''

    if os.path.isfile('features/part-m-00000'):
        # check features
        #call("sort features/part-m-00000 > features/actualfeatures", shell=True)
        diff1 = check_output("diff features/part-m-00000 " + expected_path + "expectedfeatures | wc -l", shell=True, stderr=STDOUT)

        if int(diff1) == 0:
            score = 2.0
            comments += 'features MATCHED '
        else:
            comments += 'features MISMATCHED '
    else:
        comments += 'ERROR: features folder not found '

    if os.path.isfile('features_map/part-r-00000'):
        #check features_map
        call("sort features_map/part-r-00000 > features_map/actualfeatures_map.csv", shell=True)
        diff2 = check_output("diff features_map/actualfeatures_map.csv " + expected_path + "expectedfeatures_map.csv | wc -l", shell=True, stderr=STDOUT)

        if int(diff2) == 0:
            score += 2.0
            comments += 'features_map MATCHED '
        else:
            comments += 'features_map MISMATCHED '
    else:
        comments += 'ERROR: features_map folder not found '


    if score == 4.0:
        comments += 'test_features_map: PASS '
    else:
        comments += 'test_features_map: FAILED '

    return score, comments

def features_normalized():
    score = 0
    comments = ''

    if os.path.isfile('features_normalized/part-r-00000'):
        # check features_normalized
        call("sort features_normalized/part-r-00000 > features_normalized/actualfeatures_normalized.csv;", shell=True)
        diff = check_output("diff features_normalized/actualfeatures_normalized.csv " + expected_path + "expectedfeatures_normalized.csv | wc -l", shell=True, stderr=STDOUT)

        if int(diff) == 0:
            score = 4.0
    else:
        comments += 'ERROR: features_normalized folder not found '

    if score == 4.0:
        comments += 'test_features_normalized: PASS '
    else:
        comments += 'test_features_normalized: FAILED '

    return score, comments

def samples():
    comments = ''

    if os.path.isfile('samples/part-r-00000'):
        # check samples
        #call("sort samples/part-r-00000 > samples/actualsamples.csv;", shell=True)
        diff = check_output("diff samples/part-r-00000 " + expected_path + "expectedsamples | wc -l", shell=True, stderr=STDOUT)

        if int(diff) == 0:
            comments = 'test_samples: PASS '
        else:
            comments = 'test_samples: FAILED '
    else:
        comments += 'ERROR: samples folder not found '

    return comments

def test_run():
    # os.chdir(actual_path)
    # call("rm -R -- */", shell=True)
    # pig_run(sys.argv[1])
    score1, comments1 = indexdate()
    score2, comments2 = filtered()
    score3, comments3 = features_aggregate()
    score4, comments4 = features_map()
    score5, comments5 = features_normalized()
    #comments6 = samples()
    return ((score1 + score2 + score3 + score4 + score5), (comments1 + comments2 + comments3 + comments4 + comments5))

if __name__ == "__main__":
    total_score, comments = test_run()
    print("{},{}".format(total_score, comments))

