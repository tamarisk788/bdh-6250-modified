import sys
import math
import os
import pickle
import numpy as np

from lrsgd import LogisticRegressionSGD
from utils import parse_svm_light_data
from testensemble import predict_prob

from sklearn.metrics import roc_curve, auc

def test_weight(train_file, weights, tolerance=1e-04):
    classifier = LogisticRegressionSGD(0.01, 0.01, 3618)
    try:
        with open(train_file) as training:
            for X, y in parse_svm_light_data(training):
                classifier.fit(X, y)
            
            all_zeros = not np.asarray(classifier.weight).any()
            if all_zeros:
                res = False
                msg = "Not implemented. Weights are 0"
            else:
                expected_weights = pickle.load(open(weights, "rb"))
                res = np.allclose(classifier.weight, expected_weights, atol=tolerance)
                msg = ""
                if res==False:
                    msg = "Weights do not match"
            # print(np.testing.assert_array_almost_equal(classifier.weight, expected_weights, decimal=3))
            print res, msg
    except EnvironmentError, err: # parent of IOError, OSError *and* WindowsError where available
        print False, err

def test_lrsgd(train_file, test_file):
    classifier = LogisticRegressionSGD(0.01, 0.01, 3618)
    try:
        with open(train_file) as training:
            for X, y in parse_svm_light_data(training):
                classifier.fit(X, y)
        
        with open(test_file) as testing:
            y_test_prob = []
            y_test = []
            for X, y in parse_svm_light_data(testing):
                y_prob = classifier.predict_prob(X)
                y_test.append(y)
                y_test_prob.append(y_prob)
            fpr, tpr, _ = roc_curve(y_test, y_test_prob)
            roc_auc = auc(fpr, tpr)
            res= roc_auc > 0.56
            # print res
            msg = ""
            if res == False:
                msg = "AUC less than expected " + str(roc_auc)
        print res, msg
    except EnvironmentError, err: # parent of IOError, OSError *and* WindowsError where available
        print False, err
    
def my_predict_prob(classifiers, X):
    """
    Given a list of trained classifiers,
    predict the probability of positive label.
    """
    return sum([classifier.predict_prob(X) for classifier in classifiers])/len(classifiers)


def load_model(path):
    with open(path, 'rb') as f:
        lines = [line.strip() for line in f]
        content = '\n'.join(lines)

        classifier = pickle.loads(content)
        return classifier

# 2.4.b
def test_ensemble(train_file, models_filepath):
    files = [models_filepath + "/" + filename for filename in os.listdir(models_filepath) if filename.startswith('part')]
    classifiers = map(load_model, files)
    res = False;
    msg="";
    with open(train_file) as training:
        for X, y in parse_svm_light_data(training):
            y_prob = predict_prob(classifiers, X)
            anwser = my_predict_prob(classifiers, X)
            if y_prob == None:
                y_prob=0;
            if abs(y_prob - anwser) > 0.00001:
                msg= "Wrong ensemble"
                break;
    if msg=="":
        res= True;
    print res, msg

# print "Right ensemble"

# argv[1] - test_folder_path, argv[2] - models location . E.g. python grading.py ../../data/test/ models_sol/
tolerance=1e-04
test_weight(sys.argv[1]+'train_wt.data', sys.argv[1]+'expected_weights', tolerance);
test_lrsgd(sys.argv[1]+'training.data', sys.argv[1]+'testing.data');
test_ensemble(sys.argv[1]+'training.data', sys.argv[2]);
