-- ***************************************************************************
-- TASK
-- ***************************************************************************

REGISTER utils.py USING jython AS utils;

events = LOAD '../../data/events.csv' USING PigStorage(',') AS (patientid:int, eventid:chararray, eventdesc:chararray, timestamp:chararray, value:float);
events = FOREACH events GENERATE patientid, eventid, ToDate(timestamp, 'yyyy-MM-dd') AS etimestamp, value;

mortality = LOAD '../../data/mortality.csv' USING PigStorage(',') as (patientid:int, timestamp:chararray, label:int);
mortality = FOREACH mortality GENERATE patientid, ToDate(timestamp, 'yyyy-MM-dd') AS mtimestamp, label;

-- ***************************************************************************
-- Compute the index dates for dead and alive patients
-- ***************************************************************************

eventswithmort = JOIN events BY patientid LEFT OUTER, mortality BY patientid;

deadevents = FILTER eventswithmort BY mortality::patientid IS NOT NULL;

deadevents = FOREACH deadevents GENERATE events::patientid AS patientid, events::eventid AS eventid, events::value AS value, mortality::label AS label, DaysBetween(mortality::mtimestamp, events::etimestamp)-30 AS time_difference;

aliveevents = FILTER eventswithmort BY mortality::patientid IS NULL;

aliveevents = FOREACH aliveevents GENERATE events::patientid AS patientid, events::eventid AS eventid, events::value AS value, events::etimestamp AS etimestamp;

alivegrpd = GROUP aliveevents BY (patientid);

alivegrpd = FOREACH alivegrpd GENERATE FLATTEN(aliveevents.(patientid, eventid, value, etimestamp)), MIN(aliveevents.etimestamp) AS mtimestamp;
-- CORRECT: alivegrpd = FOREACH alivegrpd GENERATE FLATTEN(aliveevents.(patientid, eventid, value, etimestamp)), MAX(aliveevents.etimestamp) AS mtimestamp;

aliveevents = FOREACH alivegrpd GENERATE patientid AS patientid, eventid AS eventid, value AS value, 0 AS label, DaysBetween(mtimestamp, etimestamp) AS time_difference;

--TEST-1
deadevents = ORDER deadevents BY patientid, eventid;
aliveevents = ORDER aliveevents BY patientid, eventid;
STORE aliveevents INTO 'aliveevents' USING PigStorage(',');
STORE deadevents INTO 'deadevents' USING PigStorage(',');

-- ***************************************************************************
-- Filter events within the observation window and remove events with missing values
-- ***************************************************************************

events = UNION aliveevents, deadevents;

filtered = FILTER events BY time_difference <= 2000 AND time_difference >= 0;

filtered = FILTER filtered BY value IS NOT NULL;

--TEST-2
filteredgrpd = GROUP filtered BY 1;
filtered = FOREACH filteredgrpd GENERATE FLATTEN(filtered);
filtered = ORDER filtered BY patientid, eventid,time_difference;
STORE filtered INTO 'filtered' USING PigStorage(',');

-- ***************************************************************************
-- Aggregate events to create features
-- ***************************************************************************

grpd = GROUP filtered BY (patientid, eventid);

featureswithid = FOREACH grpd GENERATE group.patientid AS patientid, group.eventid AS eventid, COUNT(filtered) AS featurevalue;

--TEST-3
featureswithid = ORDER featureswithid BY patientid, eventid;
STORE featureswithid INTO 'features_aggregate' USING PigStorage(',');

-- ***************************************************************************
-- Generate feature mapping
-- ***************************************************************************

all_features = FOREACH featureswithid GENERATE eventid;

all_features = DISTINCT all_features;

all_features = ORDER all_features BY eventid;

all_features = RANK all_features BY eventid;

all_features = FOREACH all_features GENERATE (rank_all_features - 1) AS idx, eventid AS eventid;

STORE all_features INTO 'features' using PigStorage(' ');

features = JOIN featureswithid BY eventid, all_features BY eventid;

features = FOREACH features GENERATE featureswithid::patientid AS patientid, all_features::idx AS idx, featureswithid::featurevalue AS featurevalue;

--TEST-4
features = ORDER features BY patientid, idx;
STORE features INTO 'features_map' USING PigStorage(',');

-- ***************************************************************************
-- Normalize the values using min-max normalization
-- ***************************************************************************

grpd = GROUP features BY idx;

maxvalues = FOREACH grpd GENERATE group AS idx, AVG(features.featurevalue) AS maxvalue;
-- CORRECT: maxvalues = FOREACH grpd GENERATE group AS idx, MAX(features.featurevalue) AS maxvalue;

normalized = JOIN features BY idx, maxvalues BY idx;

features = FOREACH normalized GENERATE features::patientid AS patientid, features::idx AS idx, (DOUBLE) features::featurevalue / maxvalues::maxvalue AS normalizedfeaturevalue;

--TEST-5
features = ORDER features BY patientid, idx;
STORE features INTO 'features_normalized' USING PigStorage(',');

-- ***************************************************************************
-- Generate features in svmlight format
-- ***************************************************************************

grpd = GROUP features BY patientid;
grpd_order = ORDER grpd BY $0;
features = FOREACH grpd_order
{
    sorted = ORDER features BY idx;
    generate group as patientid, utils.bag_to_svmlight(sorted) as sparsefeature;
}

-- ***************************************************************************
-- Split into train and test set
-- ***************************************************************************

labels = FOREACH filtered GENERATE patientid AS patientid, label AS label;
labels = DISTINCT labels;

--Generate sparsefeature vector relation
samples = JOIN features BY patientid, labels BY patientid;
samples = DISTINCT samples PARALLEL 1;
samples = ORDER samples BY $0;
samples = FOREACH samples GENERATE $3 AS label, $1 AS sparsefeature;

--TEST-6
STORE samples INTO 'samples' USING PigStorage(' ');

-- randomly split data for training and testing
DEFINE rand_gen RANDOM('6505');
samples = FOREACH samples GENERATE rand_gen() as assignmentkey, *;
SPLIT samples INTO testing IF assignmentkey <= 0.20, training OTHERWISE;
training = FOREACH training GENERATE $1..;
testing = FOREACH testing GENERATE $1..;

-- save training and tesing data
STORE testing INTO 'testing' USING PigStorage(' ');
STORE training INTO 'training' USING PigStorage(' ');
