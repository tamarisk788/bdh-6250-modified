### take MASTER_RECORD.log file as input to create grade.csv file for convenience
### usage: python convert_to_csvfile.py

import csv
#read log file
in_txt = open("master_record.log", "r")
input = []
in_txt.readline()  #leave out the comment 'begin master log' 
for row in in_txt:
	input.append(row.split(','))
# print input

#write to csv file
out_csv = csv.writer(open("grade.csv", "w"))
out_csv.writerows(input)


