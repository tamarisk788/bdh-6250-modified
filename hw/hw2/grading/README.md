
### Grading Instructions
### useful folders/files: 'grading/dir', 'grading/hw2_grading'
### feel free to optimize those grading codes/instructions and clarify on bitbucket

# prepare files and students' submissions
1. Download/move the 'dir' and 'hw2_grading' to your local machine. e.g. parallel to 'bdh/hw/hw2' (copy directly from bitbucket) like below:
	root_folder/
	|
	+--bdh/hw/hw2/...
	|
	+--dir
	|
	+--hw2_grading
IMPORTANT: modify the grading path defined in 'dir/run_one.sh' by your needs
(Alternatively: moving to docker it also okay, file path modification in 'dir/run_one.sh' is also required)

2. Download/save students' hw2 submissions to 'dir' folder

# Start docker and hdfs
3. Pull the image and start the container bigbox, create consistent packages from environment.yml.

4. Upload input data in 2.1 hive section to hdfs
-- IMPORTANT NOTES:
-- You need to put events.csv and mortality.csv under hdfs directory 
-- '/input/events/events.csv' and '/input/mortality/mortality.csv'
-- 
-- To do this, run the following commands for events.csv, 
-- 1. sudo su - hdfs
-- 2. hdfs dfs -mkdir -p /input/events
-- 3. hdfs dfs -chown -R root /input
-- 4. exit 
-- 5. hdfs dfs -put /path-to-events.csv /input/events/
-- Follow the same steps 1 - 5 for mortality.csv, except that the path should be 
-- '/input/mortality'

5. Similarly, upload referenced training data created from 2.3 section solution codes to hdfs which will be used for hadoop grading (2.4 section)
-- training data located at folder 'hw2_grading/expected_output/pig/training'

# start grading
6. Navigate to 'dir' folder and compile/run 'run_all.sh' to start grading for all students

7. Grading output named 'MASTER_RECORD.LOG' will be created

8. Run convert_to_csvfile.py by your needs

