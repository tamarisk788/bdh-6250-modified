"""
Description:
  Grade 2.3 SGD by training a model, checking the weights, and ensuring
  sufficiently high ROC is achieved on a test set

Parameters:
  $1: training data file (our defined output from Pig ETL to feed into students' lrsgd)
  $2: testing data file (our defined output from Pig ETL to feed into students' lrsgd)
  $3: expected outputs directory (the directory that contains the expected reference models for comparison)

Usage:
  python grade.py path/to/training/part-r-00000 path/to/testing/part-r-00000 expected_output_dir

Requires:
  None
"""

import sys
import math
import os
import pickle
import numpy as np

from lrsgd import LogisticRegressionSGD
from utils import parse_svm_light_data
from testensemble import predict_prob
from sklearn.metrics import roc_curve, auc

mu = 0.01
eta = 0.01
num_features = 3618

tolerance = 1e-04
min_auc = 0.56

def test_weight(train_file, expected_model_dir):
    score = 0
    comments = ''

    classifier = LogisticRegressionSGD(eta, mu, num_features)
    # Include classifier optimizing 1/2*mu^2 (so 2*mu becomes mu)
    classifier_2x_mu = LogisticRegressionSGD(eta, 2*mu, num_features)
    try:
        with open(train_file) as training:
            for X, y in parse_svm_light_data(training):
                classifier.fit(X, y)
                classifier_2x_mu.fit(X, y)

            all_zeros = not np.asarray(classifier.weight).any()
            if all_zeros:
                score = 0.0
                comments += "SGD: FAIL (weights all 0, not implemented)"
            else:
                if len(classifier.weight) == num_features + 1: # Includes intercept term
                    correct_model = pickle.load(open(expected_model_dir + "/model_intercept.txt", "rb"))
                    half_right_model = pickle.load(open(expected_model_dir + "/model_intercept_wrong.txt", "rb"))
                else: # No intercept term
                    correct_model = pickle.load(open(expected_model_dir + "/model.txt", "rb"))
                    half_right_model = pickle.load(open(expected_model_dir + "/model_wrong.txt", "rb"))
                correct_res = np.allclose(classifier.weight, correct_model.weight, atol=tolerance)
                half_right_res = np.allclose(classifier.weight, half_right_model.weight, atol=tolerance)
                correct_res_2x_mu = np.allclose(classifier_2x_mu.weight, correct_model.weight, atol=tolerance)
                half_right_res_2x_mu = np.allclose(classifier_2x_mu.weight, half_right_model.weight, atol=tolerance)

                if correct_res == True or correct_res_2x_mu == True:
                    score = 15.0
                    comments += "SGD: PASS (weights within tolerance)"
                elif half_right_res == True or half_right_res_2x_mu == True:
                    score = 7.5
                    comments += "SGD: PARTIAL (only regularized non-zero terms updated)"
                else:
                    score = 0.0
                    comments += "SGD: FAIL (weights outside {} absolute tolerance)".format(tolerance)

        return score, comments
    except OSError as err:
        return 0.0, "SGD: ERROR (could not score)"

def test_lrsgd(train_file, test_file):
    score = 0
    comments = ''

    classifier = LogisticRegressionSGD(eta, mu, num_features)
    classifier_2x_mu = LogisticRegressionSGD(eta, 2*mu, num_features)  #new added
    try:
        with open(train_file) as training:
            for X, y in parse_svm_light_data(training):
                classifier.fit(X, y)
                classifier_2x_mu.fit(X, y)   #new added

        with open(test_file) as testing:
            y_test_prob = []
            y_test_prob2 = []  #new added
            y_test = []
            for X, y in parse_svm_light_data(testing):
                y_prob = classifier.predict_prob(X)
                y_prob2 = classifier_2x_mu.predict_prob(X)   #new added
                y_test.append(y)
                y_test_prob.append(y_prob)
                y_test_prob2.append(y_prob2)  #new added
            fpr, tpr, _ = roc_curve(y_test, y_test_prob)
            fpr2, tpr2, _ = roc_curve(y_test, y_test_prob2)  #new added
            roc_auc = auc(fpr, tpr)
            roc_auc2 = auc(fpr2, tpr2)  #new added

            if roc_auc > min_auc or roc_auc2 > min_auc:
                score = 5.0
                comments += "ROC: PASS (test AUC above {})".format(min_auc)
            else:
                score = 0.0
                comments += "ROC: FAIL (test AUC below {})".format(min_auc)
        return score, comments
    except OSError as err:
        return 0.0, "ROC: ERROR (could not score)"

if __name__ == "__main__":
    train_file_path = sys.argv[1]
    test_file_path = sys.argv[2]
    exp_model_dir = sys.argv[3]

    sgd_score, sgd_comments = test_weight(train_file_path, exp_model_dir)
    roc_score, roc_comments = test_lrsgd(train_file_path, test_file_path)

    total_score = sgd_score + roc_score
    comments = sgd_comments + ' - ' + roc_comments

    print("{},{}".format(total_score, comments))
