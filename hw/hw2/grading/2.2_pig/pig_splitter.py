#!/usr/local/conda2/bin/python
'''
This code independently grades each section of HW2 S2.2 Pig ETL

This script takes as input a student etl.pig file, parses out sections
of the script based on Pig STORE commands, and outputs separate scripts
for each section that use the CORRECT/Expected data as inputs instead of 
the data produced from previous step.

This allows the sections to be graded independently and solves problems
with students making a step in the first section causing them to receive
a zero score despite all subsequent sections being correct.
'''

import sys
import argparse

parser = argparse.ArgumentParser(description="Turn student etl.pig script into multiple scripts to break dependency between parts during grading")

parser.add_argument("student_pig_file", help="Input .pig file to split into multiple .pig files")
parser.add_argument("input_data_dir", help="Directory with input data for the Pig script (events.csv, mortality.csv)")
parser.add_argument("expected_data_dir", help="Directory with expected Pig data to load instead of running student code")
parser.add_argument("-o", "--output_dir", help="Directory to output the split .pig files", default=".")

# Data loading lines
header = """REGISTER utils.py USING jython AS utils;
events = LOAD '{data_dir}/events.csv' USING PigStorage(',') AS (patientid:int, eventid:chararray, eventdesc:chararray, timestamp:chararray, value:float);
events = FOREACH events GENERATE patientid, eventid, ToDate(timestamp, 'yyyy-MM-dd') AS etimestamp, value;
mortality = LOAD '{data_dir}/mortality.csv' USING PigStorage(',') as (patientid:int, timestamp:chararray, label:int);
mortality = FOREACH mortality GENERATE patientid, ToDate(timestamp, 'yyyy-MM-dd') AS mtimestamp, label;"""

var_load_lines = {
    "header": header,
    "aliveevents": "aliveevents = LOAD '{expected_dir}/aliveevents/part-r-00000' USING PigStorage(',') AS (patientid:int, eventid:chararray, value:float, label:int, time_difference:int);",
    "deadevents": "deadevents = LOAD '{expected_dir}/deadevents/part-r-00000' USING PigStorage(',') AS (patientid:int, eventid:chararray, value:float, label:int, time_difference:int);",
    "filtered": "filtered = LOAD '{expected_dir}/filtered/part-r-00000' USING PigStorage(',') AS (patientid:int, eventid:chararray, value:float, label:int, time_difference:int);",
    "features_aggregate": "featureswithid = LOAD '{expected_dir}/features_aggregate/part-r-00000' USING PigStorage(',') AS (patientid:int, eventid:chararray, featurevalue:int);",
    "features": "all_features = LOAD '{expected_dir}/features/part-m-00000' USING PigStorage(' ') AS (idx:int, eventid:chararray);", 
    "features_map": "features = LOAD '{expected_dir}/features_map/part-r-00000' USING PigStorage(',') AS (patientid:int, idx:int, featurevalue:double);",
    "features_normalized": "features = LOAD '{expected_dir}/features_normalized/part-r-00000' USING PigStorage(',') AS (patientid:int, idx:int, normalizedfeaturevalue:double);"
}


class PigScriptSplitter:
    def __init__(self, input_data_dir, expected_data_dir):
        ''' Creates PigScript '''
        self.input_data_dir = input_data_dir
        self.expected_data_dir = expected_data_dir
        self.sections = [PigSection()]

    def parse_pig_sections(self, input_pig_file):
        ''' Parse input Pig script into sections by  STORE {var} INTO '{table}'  commands '''
        with open(input_pig_file, 'r') as etl_pig:
            for line in etl_pig:
                last_section = self.sections[-1]
                last_section.add_line(line.strip() + "\n")

                commands = line.lower().split(' ')

                if commands[0] == 'store' and commands[1] == 'samples':
                    break # Stop after SAMPLES line added (do not add to stored_table)
                if commands[0] == 'store' and commands[1] == 'aliveevents':
                    # Do not split sections on ALIVEEVENTS as DEADEVENTS built at the same time
                    continue
                elif commands[0] == 'store' and commands[2] == 'into':
                    table = commands[3].strip("'")

                    new_section = PigSection(last_section)
                    new_section.add_table_to_load(table)

                    # ALIVEEVENTS skipped earlier so add here (placing after DEADEVENTS is fine)
                    if table == 'deadevents':
                        new_section.add_table_to_load('aliveevents')

                    self.sections.append(new_section)

    def output_new_scripts(self, output_dir):
        ''' Output separate .pig files for each section '''
        for index, pig_section in enumerate(self.sections):
            new_pig_file = '{}/etl_section_{}.pig'.format(output_dir, index)
            write_header = (True if index > 0 else False)
            pig_section.write_output(new_pig_file, self.input_data_dir,
                self.expected_data_dir, write_header)


class PigSection:
    ''' Stores a divisible section of a Pig script '''

    def __init__(self, old_section = None):
        ''' Create new PigSection but carrying forward all previously seen vars '''
        self.script_header = header # Define above
        self.table_load_lines = var_load_lines # Defined above
        self.tables_to_load = []
        self.script_lines = []
        if old_section != None:
            for table in old_section.tables_to_load:
                self.add_table_to_load(table)
    
    def add_line(self, line):
        self.script_lines.append(line)

    def add_table_to_load(self, table):
        self.tables_to_load.append(table)

    def get_load_line(self, table, data, exp):
        ''' Get line to load table back to a variable '''
        line = self.table_load_lines[table] + "\n"
        return line.format(data_dir = data, expected_dir = exp)

    def write_output(self, out_file, in_data_dir, exp_data_dir, write_header = True):
        ''' Write the Pig section to a new file, loading all necessary expected data '''
        with open(out_file, 'w') as f_out:
            if write_header:
                header_lines = self.get_load_line("header", in_data_dir, exp_data_dir)
                f_out.write(header_lines)
            for table in self.tables_to_load:
                load_line = self.get_load_line(table, in_data_dir, exp_data_dir)
                f_out.write(load_line + "\n")
            f_out.write("------ ^^ CORRECT (EXPECTED) DATA LOADED ABOVE ^^ ------\n")
            for line in self.script_lines:
                f_out.write(line)



if __name__ == "__main__":
    args = parser.parse_args()

    splitter = PigScriptSplitter(args.input_data_dir, args.expected_data_dir)

    splitter.parse_pig_sections(args.student_pig_file)

    splitter.output_new_scripts(args.output_dir)
