#!/bin/bash
#
# Description:
#   Run Pig ETL code
#
# Parameters:
#   None
#
# Usage:
#   run.sh
#
# Requires:
#   - MUST be run from the /code/pig folder (uses relative paths)
#   - utils.py in the same folder as etl.pig
#   - events.csv and mortality.csv in ../../data/

sudo pig -x local etl.pig
