"""
Description:
  Grade 2.3 SGD by training a model, checking the weights, and ensuring
  sufficiently high ROC is achieved on a test set

Parameters:
  $1: training data file (output from Pig ETL)
  $2: test data file (output from Pig ETL)
  $3: expected weight outputs

Usage:
  python grade.py training/part-r-00000 test/part-r-00000 exp_out/weight.out

Requires:
  None
"""

import sys
import math
import os
import pickle
import numpy as np

from lrsgd_fast import LogisticRegressionSGD
from utils import parse_svm_light_data
from sklearn.metrics import roc_curve, auc
from time import time

mu = 0.01
eta = 0.01
num_features = 5676

# Solution AUC: 0.6734  Runtime: 0.5459
min_auc = 0.66
max_run_seconds = 0.60

def train_model(classifier, train_file):
    run_time = 9e5
    #loop_counter = 0

    with open(train_file) as training:
        start_time = time()
        for X, y in parse_svm_light_data(training):
            classifier.fit(X, y)
            #loop_counter += 1
            #if loop_counter % 1000 == 0 and time() - start_time > max_run_seconds:
            #    break
        end_time = time()

    run_time = end_time - start_time
    return classifier, run_time

def test_model(classifier, test_file):
    roc_auc = 0

    with open(test_file) as testing:
       y_test_prob = []
       y_test = []
       for X, y in parse_svm_light_data(testing):
           y_prob = classifier.predict_prob(X)
           y_test.append(y)
           y_test_prob.append(y_prob)
       fpr, tpr, _ = roc_curve(y_test, y_test_prob)
       roc_auc = auc(fpr, tpr)

    return roc_auc

def test_lrsgd_fast(train_file, test_file):
    score = 0
    comments = ''

    classifier = LogisticRegressionSGD(eta, mu, num_features)
    # Include classifier optimizing 1/2*mu^2 (so 2*mu becomes mu)
    #classifier_2x_mu = LogisticRegressionSGD(eta, 2*mu, num_features)
    try:
        classifier, run_time = train_model(classifier, train_file)

        all_zeros = not np.asarray(classifier.weight).any()
        if all_zeros:
            score += 0.0
            comments += "SGD_FAST: FAIL (weights all 0, not implemented)"
        elif run_time > max_run_seconds:
            score += 0.0
            comments += "SGD_FAST: FAIL (training time was >{:.2f}s: {:.2f})".format(max_run_seconds, run_time)
        else:
            roc_auc = test_model(classifier, test_file)

            if roc_auc < min_auc:
                score += 0.0
                comments += "SGD_FAST: FAIL (model accuracy was below {:.2f} min: {:.2f})".format(min_auc, roc_auc)
            else:
                score += 10.0
                comments += "SGD_FAST: PASS (fast runtime {:.2f} and high accuracy {:.2f})".format(run_time, roc_auc)

        return score, comments
    except OSError as err:
        return 0.0, "SGD_FAST: ERROR (could not score)"

if __name__ == "__main__":
    train_file_path = sys.argv[1]
    test_file_path = sys.argv[2]

    score, comments = test_lrsgd_fast(train_file_path, test_file_path)

    print("{},{}".format(score, comments))
