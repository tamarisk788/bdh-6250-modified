#!/bin/bash
#
# Description:
#  Run Hive query for descriptive statistics
#
# Parameters:
#  $1: path to .hql file

hive -f $1
