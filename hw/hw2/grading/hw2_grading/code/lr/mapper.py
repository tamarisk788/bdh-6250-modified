#!/usr/bin/env python

import sys
import random

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-n", "--model-num", action="store", dest="n_model",
                  help="number of models to train", type="int")
parser.add_option("-r", "--sample-ratio", action="store", dest="ratio",
                  help="ratio to sample for each ensemble", type="float")

options, args = parser.parse_args(sys.argv)

random.seed(6505)

for line in sys.stdin:
    # Strip out leading and trailing white spaces of the feature set
    value = line.strip()
    
    for key in range(0, options.n_model):
        # Take a guess, since we are using uniform distribution, we expect 
        # to have total selectionshit the expect sampling ratio
        m=random.uniform(0,1)
        if m<options.ratio:
            print("%d\t%s" % (key, value))
