#!/bin/bash
#
# Description:
#   Run Hadoop model fitting from /code folder
#
# Parameters:
#   [DELETE] ]$1: path to Pig output folder with training/
#   [DELETE] $2: path to mapper.py file
#
# Usage:
#   run.sh
#
# Requires:
#   - MUST be run from the hw2_grading/code folder
#   - Hadoop user home folder must be created
#   - Pig training output on HDFS at hw2/training/

# ----------------------- #
#  PART 1: MAPPER TESTING #
# ----------------------- #

# Remove previous run data
rm map.out
hdfs dfs -rm -r hw2/mapper

# Run Hadoop job with identity reducer
hadoop jar \
  /usr/lib/hadoop-mapreduce/hadoop-streaming.jar \
  -D mapreduce.job.reduces=0 \
  -files lr \
  -mapper "python lr/mapper.py -n 3 -r 0.5 " \
  -input hw2/training \
  -output hw2/mapper

# Combine all files in hw2/mapper to one file mapper.out
hdfs dfs -getmerge hw2/mapper/ map.out

# Replace map.out with a sorted version
sort -o map.out map.out


# ----------------------- #
#  PART 2: ENSEMBLE MODEL #
# ----------------------- #
