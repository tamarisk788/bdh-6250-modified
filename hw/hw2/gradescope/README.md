# Gradescope Autograder: HW-2

A generic gradescope tutorial is available in Homework 5 grading.

## Homework2 Setup

Homework 2 autograder is built with a docker image.
The docker image used is can be built from Dockerfile and should be pushed to a Docker Registry (dockerhub).
The pushed image is then used in gradescope to configure the autograder.

## Build Commands

docker build -f Dockerfile -t gradescope_cse6250_hw2 .

docker tag <image-id> rishabhbhardwaj/gradescope_cse6250_hw2

docker push  <dockerhub-repo-name>/gradescope_cse6250_hw2
