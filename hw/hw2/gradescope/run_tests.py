import unittest
import json

if __name__ == '__main__':
    in_file = '/autograder/source/grading/dir/MASTER_RECORD.log'
    out_file = '/autograder/results/results.json'

    in_txt = open(in_file, "r")
    lines = in_txt.readlines()
    record = lines[0]
    all_rows = record.split(',')
    gt_id = all_rows[0]
    gt_account = all_rows[1]
    try:
        q21hive_score = float(all_rows[2])
        q21hive_comments = all_rows[3]
    except:
        q21hive_score = 0.0
        q21hive_comments = 'No Results Found for 2.1'
    try:
        q22pig_score = float(all_rows[4])
        q22pig_comments = all_rows[5]
    except:
        q22pig_score = 0.0
        q22pig_comments = 'No Results Found for 2.2'
    try:
        q23ab_score = float(all_rows[6])
        q23ab_comments = all_rows[7]
    except:
        q23ab_score = 0.0
        q23ab_comments = 'No Results Found for 2.3'
    try:
        q24ab_mapper_score = float(all_rows[8])
        q24ab_mapper_comments = all_rows[9]
    except:
        q24ab_mapper_score = 0.0
        q24ab_mapper_comments = 'No Results Found for 2.4 mapper'
    try:
        q24ab_ensemble_score = float(all_rows[10])
        q24ab_ensemble_comments = all_rows[11]
    except:
        q24ab_ensemble_score = 0.0
        q24ab_ensemble_comments = 'No Results Found for 2.4 ensemble'

    total_score = q21hive_score + q22pig_score + q23ab_score + q24ab_mapper_score + q24ab_ensemble_score
    output_dict = {"score": total_score, 
                "output": "Homework 2 Autograder Test Results",
                "stdout_visibility": "visible",
                "tests": [
                    {
                        "score": q21hive_score,
                        "max_score": 15.0,
                        "number": "2.1",
                        "output": q21hive_comments,
                        "visibility": "visible"
                    }, 
                    {
                        "score": q22pig_score,
                        "max_score": 20.0,
                        "number": "2.2",
                        "output": q22pig_comments,
                        "visibility": "visible"
                    },
                    {
                        "score": q23ab_score,
                        "max_score": 20.0,
                        "number": "2.3 ab",
                        "output": q23ab_comments,
                        "visibility": "visible"
                    },
                    {
                        "score": q24ab_mapper_score,
                        "max_score": 5.0,
                        "number": "2.4 ab mapper",
                        "output": q24ab_mapper_comments,
                        "visibility": "visible"
                    },
                    {
                        "score": q24ab_ensemble_score,
                        "max_score": 5.0,
                        "number": "2.4 ab ensemble",
                        "output": q24ab_ensemble_comments,
                        "visibility": "visible"
                    }
                ]}
    with open(out_file, 'w') as fp:
        json.dump(output_dict, fp)
