#!/bin/sh
# disable core dump file
ulimit -c 0
# pre-process: start the services
/scripts/start-scripts.sh
# start all
exec "$@"