"""
Description:
  Grade 2.3 SGD by training a model, checking the weights, and ensuring
  sufficiently high ROC is achieved on a test set

Parameters:
  $1: training data file (output from Pig ETL)
  $2: test data file (output from Pig ETL)
  $3: expected weight outputs

Usage:
  python grade.py training/part-r-00000 test/part-r-00000 exp_out/weight.out

Requires:
  None
"""

import sys
import math
import os
import pickle
import numpy as np

from lrsgd_fast import LogisticRegressionSGD
from utils import parse_svm_light_data
from testensemble import predict_prob
from sklearn.metrics import roc_curve, auc
from time import time

mu = 0.01
eta = 0.01
num_features = 5676

# Solution AUC: 0.6734  Runtime: 0.5459
tolerance = 0.2
max_run_seconds = 0.65

def train_model(classifier, train_file):
    run_time = 9e5

    with open(train_file) as training:
        start_time = time()
        for X, y in parse_svm_light_data(training):
            classifier.fit(X, y)
        end_time = time()

    run_time = end_time - start_time
    return classifier, run_time

def test_lrsgd_fast(train_file, expected_model_dir):
    score = 0
    comments = ''

    classifier = LogisticRegressionSGD(eta, mu, num_features)
    # Include classifier optimizing 1/2*mu^2 (so 2*mu becomes mu)
    #classifier_2x_mu = LogisticRegressionSGD(eta, 2*mu, num_features)
    try:
        classifier, run_time = train_model(classifier, train_file)

        if len(classifier.weight) == num_features + 1: # Includes intercept term
            # /shruggie Just manually grade
            comments += "[+bias] "
            correct_model = classifier
        else: # No intercept term
            correct_model = pickle.load(open(expected_model_dir + "/fast_model.txt", "rb"))

        correct_res = np.allclose(classifier.weight, correct_model.weight, atol=tolerance)

        all_zeros = not np.asarray(classifier.weight).any()
        if all_zeros:
            score += 0.0
            comments += "SGD_FAST: FAIL (weights all 0, not implemented)"
        elif run_time > max_run_seconds:
            score += 0.0
            comments += "SGD_FAST: FAIL (runtime >{:.2f}s: {:.2f})".format(max_run_seconds, run_time)
        elif correct_res:
            score += 10.0
            comments += "SGD_FAST: PASS (weights matched and {:.2f} runtime <{:.2f}s)".format(run_time, max_run_seconds)
        else:
            score += 0.0
            comments += "SGD_FAST: FAIL (weights outside {} tolerance)".format(tolerance)

        return score, comments
    except OSError as err:
        return 0.0, "SGD_FAST: ERROR (could not score)"

if __name__ == "__main__":
    train_file_path = sys.argv[1]
    expected_model_dir = sys.argv[2]

    score, comments = test_lrsgd_fast(train_file_path, expected_model_dir)

    print("{},{}".format(score, comments))
