# Do not use anything outside of the standard distribution of python
# when implementing this class
import math 

class LogisticRegressionSGD:
    """
    Logistic regression with stochastic gradient descent
    """

    def __init__(self, eta, mu, n_feature):
        """
        Initialization of model parameters
        """
        self.eta = eta
        self.weight = [0.0] * n_feature
        self.mu = mu
        self.n_feature = n_feature
        # Cache the regularization constant portion for reuse
        self.regconst = 1-2*self.mu*self.eta
        # Keep track of pending constant portion updates per weight
        self.pending = [0] * n_feature
        # Keep track of the iteration number
        self.k = 0

    def fit(self, X, y):
        """
        Update model using a pair of training sample
        """
        p = self.predict_prob(X)
        
        # Point to next iteration
        self.k = self.k + 1

        # Update the weight with feature value factor and pending regularization updates
        for i in range(len(X)):
            iter = self.k - self.pending[i]
            self.weight[X[i][0]] = self.weight[X[i][0]] * math.pow(self.regconst, iter) \
                                          + self.eta*(y - p)*X[i][1]
            self.pending[i] = self.k
        
    def finalupdate(self, X, y):
        '''
        Make the final update of pending regularization
        '''
        # Update the weights with all pending regularizations
        for i in range(self.n_feature):
            iter = self.k - self.pending[i]
            self.weight[i] = self.weight[i] * math.pow(self.regconst, iter)

    def predict(self, X):
        """
        Predict 0 or 1 given X and the current weights in the model
        """
        return 1 if predict_prob(X) > 0.5 else 0

    def predict_prob(self, X):
        """
        Sigmoid function
        """
        return 1.0 / (1.0 + math.exp(-math.fsum((self.weight[f]*v for f, v in X))))
