"""
Description:
  Grade 2.2 Pig ETL comparing each of the output datasets with the expected outputs
  of the official solution code

Parameters:
  $1: path to the expected Pig output datasets, i.e. path/to/expected_output_pig_dir/pig/

Usage:
  python grade.py path/to/expected_output_pig_dir/pig/

Requires:
  TODO: this currently requires expected_output pig files like part-r-00000_sorted.csv
  (I have saved these required files into expected folders for speeding up)
"""

from subprocess import call, check_output, CalledProcessError, STDOUT
import sys
import os
import argparse

parser = argparse.ArgumentParser(description="Grading script for HW2 Pig ETL, should be run from student Pig data folder")
parser.add_argument("expected_data_dir", help="Folder with gold-standard Pig data in its sub-folders, e.g. <dir>/aliveevents/")
#parser.add_argument("to_grade_data_dir", default=".", help="Folder with student Pig data in its sub-folders (eg aliveevents/) to grade")

def indexdate(expected_path):
    score = 0
    comments = ''

    if os.path.isfile('aliveevents/part-r-00000'):
        # check aliveevents
        call("sort aliveevents/part-r-00000 > aliveevents/actual.csv", shell=True)
        # call('sort ' + expected_path + 'aliveevents/part-r-00000' + '>' + expected_path + 'aliveevents/part-r-00000_sorted.csv', shell = True) ##have saved this output of this step for speeding up
        diff1 = check_output("diff aliveevents/actual.csv " + expected_path + "aliveevents/part-r-00000_sorted.csv | wc -l", shell=True, stderr=STDOUT)

        if int(diff1) == 0:
            score = 2.0
            comments += 'aliveevents MATCHED '
        else:
            comments += 'aliveevents MISMATCHED '
    else:
        comments += 'ERROR: aliveevents folder not found '

    if os.path.isfile('deadevents/part-r-00000'):
        #check deadevents
        call("sort deadevents/part-r-00000 > deadevents/actual.csv", shell=True)
        # call("sort " + expected_path + "deadevents/part-r-00000" + ">" + expected_path + "deadevents/part-r-00000_sorted.csv", shell=True)  ##have saved this output of this step for speeding up
        diff2 = check_output("diff deadevents/actual.csv " + expected_path + "deadevents/part-r-00000_sorted.csv | wc -l", shell=True, stderr=STDOUT)

        if int(diff2) == 0:
            score += 2.0
            comments += 'deadevents MATCHED '
        else:
            comments += 'deadevents MISMATCHED '
    else:
        comments += 'ERROR: deadevents folder not found '

    if score == 4.0:
        comments += 'test_indexdate: PASS '
    elif score == 2.0:
        comments += 'test_indexdate: PARTIAL PASS '
    else:
        comments += 'test_indexdate: FAILED '

    return score, comments

def filtered(expected_path):
    score = 0
    comments = ''

    if os.path.isfile('filtered/part-r-00000'):
        # check filtered
        call("sort filtered/part-r-00000 > filtered/actual.csv", shell=True)
        # call("sort " + expected_path + "filtered/part-r-00000" + ">" + expected_path + "filtered/part-r-00000_sorted.csv", shell=True)  ##have saved this output of this step for speeding up
        diff = check_output("diff filtered/actual.csv " + expected_path + "filtered/part-r-00000_sorted.csv | wc -l", shell=True, stderr=STDOUT)

        if int(diff) == 0:
            score = 4.0
    else:
        comments += 'ERROR: filtered folder not found '

    if score == 4.0:
        comments += 'test_filtered: PASS '
    else:
        comments += 'test_filtered: FAILED '

    return score, comments

def features_aggregate(expected_path):
    score = 0
    comments = ''

    if os.path.isfile('features_aggregate/part-r-00000'):
        # check features_aggregate
        # call("sort features_aggregate/part-r-00000 > features_aggregate/actual.csv", shell=True)
        diff = check_output("diff features_aggregate/part-r-00000 " + expected_path + "features_aggregate/part-r-00000 | wc -l", shell=True, stderr=STDOUT)

        if int(diff) == 0:
            score = 4.0
    else:
        comments += 'ERROR: features_aggregate folder not found '

    if score == 4.0:
        comments += 'test_features_aggregate: PASS '
    else:
        comments += 'test_features_aggregate: FAILED '

    return score, comments

def features_map(expected_path):
    score = 0
    comments = ''

    if os.path.isfile('features/part-m-00000'):
        # check features
        # call("sort features/part-m-00000 > features/actual.csv", shell=True)
        diff1 = check_output("diff features/part-m-00000 " + expected_path + "features/part-m-00000 | wc -l", shell=True, stderr=STDOUT)
        if int(diff1) == 0:
            score = 2.0
            comments += 'features MATCHED '
        else:
            comments += 'features MISMATCHED '
    else:
        comments += 'ERROR: features folder not found '

    if os.path.isfile('features_map/part-r-00000'):
        #check features_map
        # call("sort features_map/part-r-00000 > features_map/actual.csv", shell=True)
        diff2 = check_output("diff features_map/part-r-00000 " + expected_path + "features_map/part-r-00000 | wc -l", shell=True, stderr=STDOUT)
        if int(diff2) == 0:
            score += 2.0
            comments += 'features_map MATCHED '
        else:
            comments += 'features_map MISMATCHED '
    else:
        comments += 'ERROR: features_map folder not found '


    if score == 4.0:
        comments += 'test_features_map: PASS '
    else:
        comments += 'test_features_map: FAILED '

    return score, comments

def features_normalized(expected_path):
    score = 0
    comments = ''

    if os.path.isfile('features_normalized/part-r-00000'):
        # check features_normalized
        # call("sort features_normalized/part-r-00000 > features_normalized/actual.csv;", shell=True)
        diff = check_output("diff features_normalized/part-r-00000 " + expected_path + "features_normalized/part-r-00000 | wc -l", shell=True, stderr=STDOUT)
        if int(diff) == 0:
            score = 4.0
    else:
        comments += 'ERROR: features_normalized folder not found '

    if score == 4.0:
        comments += 'test_features_normalized: PASS '
    else:
        comments += 'test_features_normalized: FAILED '

    return score, comments

def samples(expected_path):
    comments = ''

    if os.path.isfile('samples/part-r-00000'):
        # check samples
        #call("sort samples/part-r-00000 > samples/actual.csv;", shell=True)
        diff = check_output("diff samples/part-r-00000 " + expected_path + "samples/part-r-00000 | wc -l", shell=True, stderr=STDOUT)

        if int(diff) == 0:
            comments = 'test_samples: PASS '
        else:
            comments = 'test_samples: FAILED '
    else:
        comments += 'ERROR: samples folder not found '

    return comments

if __name__ == "__main__":
    args = parser.parse_args()

    score1, comments1 = indexdate(args.expected_data_dir)
    score2, comments2 = filtered(args.expected_data_dir)
    score3, comments3 = features_aggregate(args.expected_data_dir)
    score4, comments4 = features_map(args.expected_data_dir)
    score5, comments5 = features_normalized(args.expected_data_dir)

    total_score = score1 + score2 + score3 + score4 + score5
    comments = comments1 + comments2 + comments3 + comments4 + comments5

    print("{},{}".format(total_score, comments))
