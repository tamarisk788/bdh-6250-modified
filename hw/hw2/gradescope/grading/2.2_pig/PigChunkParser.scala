import scala.collection.mutable.ListBuffer
import java.io.{BufferedWriter, File, FileWriter}

// option: true, print some log in stdout
val kOptionLogInStdout = true
// option: true, reserve 'store' otherwise add '-- ' before store command in template files
val kOptionKeepStoreInTemplate = false

case class GroundTruthElem(checkingVars: List[String], prevVars: List[String], data: List[String])

case class PigLine(data: String, isCommand: Boolean = true, isStore: Boolean = false, varStore: Option[String] = None)

def readLines(path: String): List[String] = {
  import java.io.File

  import scala.io.Source
  Source.fromFile(new File(path)).getLines().toList
}

def writeLines(path: String, data: List[String]) {
  val pw = new BufferedWriter(new FileWriter(new File(path)))
  pw.append(data.mkString("\n"))
  pw.flush()
  pw.close()
}

def simpleParser(line: String): PigLine = {
  val tline = line.trim
  if (tline.isEmpty) {
    // empty line
    PigLine(
      data = line,
      isCommand = false
    )
  } else if (tline.startsWith("--")) {
    PigLine(
      data = line,
      isCommand = false
    )
  } else {
    val elems = tline.split("\\s+").filter(_.nonEmpty)
    if (elems.length >= 4 && elems.head.toLowerCase == "store" && elems(2).toLowerCase() == "into") {
      var cvar = elems(3)
      if (cvar.startsWith("\'")) {
        cvar = cvar.drop(1)
      }
      if (cvar.endsWith("\'")) {
        cvar = cvar.dropRight(1)
      }
      PigLine(
        data = line,
        isStore = true,
        varStore = Some(cvar)
      )
    } else {
      PigLine(
        data = line
      )
    }
  }
}

if (args.length < 2) {
  println("\tUsage:xxx.scala /path/to/template.pig etl.pig [split.log]")
  sys.exit(-1)
}

// prepare gtBuff
def loadGtBuff(): List[GroundTruthElem] = {
  val gtBuff = ListBuffer[GroundTruthElem]()
  val linesTemplate: List[PigLine] = readLines(args(0)).map(simpleParser)

  val cCheckingVars = ListBuffer[String]()
  val cPrevVars = ListBuffer[String]()
  val cData = ListBuffer[String]()
  var inPending = false
  linesTemplate.foreach { e =>
    if (inPending) {
      if (e.isStore) {
        // is store, continous storing
        cCheckingVars.append(e.varStore.get)
      } else if (e.isCommand) {
        // checkingVars: List[String], prevVars: List[String], data: List[String]
        gtBuff.append(GroundTruthElem(cCheckingVars.toList, cPrevVars.toList, cData.toList))
        cPrevVars.clear()
        cPrevVars.appendAll(cCheckingVars)
        cCheckingVars.clear()
        cData.clear()
        inPending = false
      }
    } else {
      if (e.isStore) {
        inPending = true
        cCheckingVars.append(e.varStore.get)
      }
    }
    if ((!e.isStore) || kOptionKeepStoreInTemplate) {
      cData.append(e.data)
    } else {
      cData.append(s"-- ${e.data}")
    }
  }
  if (cCheckingVars.nonEmpty) {
    gtBuff.append(GroundTruthElem(cCheckingVars.toList, cPrevVars.toList, cData.toList))
  }
  gtBuff.toList
}

val gtBuff = loadGtBuff()

//writeLines("debug-gtbuff.txt", gtBuff.map { e =>
//  val sb = new StringBuffer()
//  sb.append(e.data.mkString("\n"))
//  sb.append("\n")
//  sb.append(s"###### [PRE]${e.prevVars.mkString(";")}, [CHK]${e.checkingVars.mkString(";")}\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
//  sb.toString
//}.toList)


def loadHwBuff(): (List[PigLine], Map[String, Int]) = {
  val hwBuff = readLines(args(1)).map(simpleParser)
  val hwIndex = hwBuff.zipWithIndex.filter(_._1.isStore).map(e => e._1.varStore.get -> e._2).toMap
  (hwBuff, hwIndex)
}

val (hwBuff, hwIndex) = loadHwBuff()

val targetBase = args(1).dropRight(4)

val logName = if (args.length > 2) args(2) else "split.log"

val logPW = new BufferedWriter(new FileWriter(new File(logName)))

gtBuff.foldLeft((List[String](), 0)) { (l, c) =>
  val templateBuffs = l._1
  val id = l._2
  // count of lines to drop
  var startIndex = 0
  val prevVarMissing = ListBuffer[String]()
  val checkingVarMissing = ListBuffer[String]()
  c.prevVars.foreach { pVar =>
    hwIndex.get(pVar) match {
      case Some(index) =>
        if (startIndex <= index) startIndex = index + 1
      case None =>
        if (kOptionLogInStdout) {
          println(s"ERROR: prevVars($pVar) not found in ${args(1)} !!! id:${id}")
        }
        prevVarMissing.append(pVar)
    }
  }
  var endIndx = 0
  c.checkingVars.foreach { cVar =>
    hwIndex.get(cVar) match {
      case Some(index) =>
        if (endIndx <= index) endIndx = index + 1
      case None =>
        if (kOptionLogInStdout) {
          println(s"ERROR: checkingVars($cVar) not found in ${args(1)} !!! id:${id}")
        }
        checkingVarMissing.append(cVar)
    }
  }
  val outbuff = ListBuffer[String]()
  outbuff.appendAll(templateBuffs)
  outbuff.append("-- THE PREVIOUS PART ARE FROM TEMPLATE")
  logPW.append(s"ID: $id, status: ${if (prevVarMissing.isEmpty && checkingVarMissing.isEmpty) "OK" else "FAILED"} ")
  if (prevVarMissing.nonEmpty) {
    logPW.append(s"[MISSING:PREV] ${prevVarMissing.mkString(",")}")
  }
  if (checkingVarMissing.nonEmpty) {
    logPW.append(s"[MISSING:CURR] ${checkingVarMissing.mkString(",")}")
  }
  logPW.append("\n")
  outbuff.appendAll(hwBuff.slice(startIndex, endIndx).map(_.data))
  val name = s"$targetBase-${id}.pig"
  writeLines(name, outbuff.toList)
  (templateBuffs ++ c.data, id + 1)
}

logPW.flush()
logPW.close()


if (kOptionLogInStdout) {
  println("done")
}

sys.exit(0)
