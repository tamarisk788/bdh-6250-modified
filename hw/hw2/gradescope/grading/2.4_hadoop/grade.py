"""
Description:
  Grade 2.4 Hadoop Mapper Output

Parameters:
  $1: student mapper output
  $2: expected mapper output file
  $3: ensemble model output directory
  $4: training data file (output from Pig ETL)

Usage:
  python grade.py map.out exp_out/map.out lr/training/part-r-00000

Requires:
  None
"""

import os
import sys
import pickle
import math

from utils import parse_svm_light_data
from testensemble import predict_prob
from subprocess import call, check_output, CalledProcessError, STDOUT

def my_predict_prob(classifiers, X):
    """
    Given a list of trained classifiers,
    predict the probability of positive label.
    """
    preds = [classifier.predict_prob(X) for classifier in classifiers]
    return sum(preds)/len(preds)

def load_model(path):
    # with open(path, 'rb') as f: # Python 2
    # #with open(path, 'r') as f: # Python 3
    #     lines = [line.strip() for line in f]
    #     content = '\n'.join(lines)

    #     classifier = pickle.loads(content) # Python 2
    #     # classifier = pickle.loads(content.encode('ascii')) # Python 3
    #     return classifier
    with open(path, 'rb') as f:
        content_list = [x.replace(b'\t\n', b'\n') for x in f.readlines()]
    with open('pickled.txt', 'wb') as ufile:
        for line in content_list:
            ufile.write(line)
    with open('pickled.txt', 'rb') as ifile:
        classifier = pickle.load(ifile)
    return classifier

def grade_mapper_output(act_mapper_out, input_obs_count, exp_ratio, exp_n_model):
    ''' Grade the 2.4a Hadoop Mapper output question
    act_mapper_out: output file from student mapper run
    input_obs_count: number of obs in Pig training data used as
      input into the mapper.py (used to calculate confidence intervals)
    exp_ratio: expected sampling ratio
    exp_n_model: expected number of models
    '''
    score = 0.0
    comments = ''

    model_counts = {}
    with open(act_mapper_out, 'r') as f:
        for line in f:
            model_id, value = line.split('\t')
            model_counts[model_id] =+ 1

    # Calculate 99.7p confidence intervals
    std_err = math.sqrt(exp_ratio * (1-exp_ratio) / input_obs_count)
    upper_ci = exp_ratio + 3 * std_err
    lower_ci = exp_ratio - 3 * std_err

    success = False

    # Success if E[# models] appeared and ratio is within 99.7p CI
    if len(model_counts) != exp_n_model:
        score = 0.0
        comments += "Hadoop Mapper: FAIL (Incorrect model count)"
    else:
        right_ratios = True
        for m in model_counts:
            act_ratio = model_counts[m] / input_obs_count
            if act_ratio < lower_ci or act_ratio > upper_ci:
                success = False

        if right_ratios:
            score = 5.0
            comments += "Hadoop Mapper: PASS"
        else:
            score = 0.0
            comments += "Hadoop Mapper: FAIL (Abnormal sample ratio)"

    return score, comments

def grade_ensemble(train_file, models_filepath):
    score = 0.0
    comments = ''

    # files = [models_filepath + "/" + filename
    #          for filename in os.listdir(models_filepath)
    #          if filename.startswith('part')
    #          ]

    # nonempty_files = [f for f in files if os.stat(f).st_size > 0]

    # classifiers = [load_model(f) for f in files]
    files = [models_filepath + "/" + filename for filename in os.listdir(models_filepath) if filename.startswith('part')]
    # classifiers = map(load_model, files)   #python2
    classifiers = list(map(load_model, files))

    with open(train_file) as training:
        for X, y in parse_svm_light_data(training):
            y_prob = predict_prob(classifiers, X)
            answer = my_predict_prob(classifiers, X)
            if y_prob == None:
                y_prob = 0;
            if abs(y_prob - answer) < 0.00001:
                score = 5.0
                comments = "Ensemble: PASS"
            else:
                score = 0.0
                comments += "Ensemble: FAIL (aggregation appears wrong)"

    return score, comments

if __name__ == "__main__":
    act_mapper_out = sys.argv[1]
    exp_mapper_out = sys.argv[2]
    ensemble_models = sys.argv[3]
    train_file_path = sys.argv[4]

    score_1, comments_1 = grade_mapper_output(act_mapper_out, 2971, 0.5, 3)
    score_2, comments_2 = grade_ensemble(train_file_path, ensemble_models)

    print("{},{},{},{}".format(score_1, comments_1, score_2, comments_2))
