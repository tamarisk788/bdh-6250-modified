import numpy as np 
from subprocess import check_output, STDOUT
import argparse

def hive_grading(expected_path):
	score = 0
	comments = ""

	diff1 = check_output("diff ./common_diag_alive/* " + expected_path + "common_diag_alive/* | wc -l", shell=True, stderr = STDOUT)
	diff2 = check_output("diff ./common_diag_dead/* " + expected_path + "common_diag_dead/* | wc -l", shell=True, stderr = STDOUT)
	diff3 = check_output("diff ./common_lab_alive/* " + expected_path + "common_lab_alive/* | wc -l", shell=True, stderr = STDOUT)
	diff4 = check_output("diff ./common_lab_dead/* " + expected_path + "common_lab_dead/* | wc -l", shell=True, stderr = STDOUT)
	diff5 = check_output("diff ./common_med_alive/* " + expected_path + "common_med_alive/* | wc -l", shell=True, stderr = STDOUT)
	diff6 = check_output("diff ./common_med_dead/* " + expected_path + "common_med_dead/* | wc -l", shell=True, stderr = STDOUT)

	diff7 = check_output("diff ./encounter_count_alive/* " + expected_path + "encounter_count_alive/* | wc -l", shell=True, stderr = STDOUT)
	diff8 = check_output("diff ./encounter_count_dead/* " + expected_path + "encounter_count_dead/* | wc -l", shell=True, stderr = STDOUT)
	diff9 = check_output("diff ./event_count_alive/* " + expected_path + "event_count_alive/* | wc -l", shell=True, stderr = STDOUT)
	diff10 = check_output("diff ./event_count_dead/* " + expected_path + "event_count_dead/* | wc -l", shell=True, stderr = STDOUT)
	diff11 = check_output("diff ./record_length_alive/* " + expected_path + "record_length_alive/* | wc -l", shell=True, stderr = STDOUT)
	diff12 = check_output("diff ./record_length_dead/* " + expected_path + "record_length_dead/* | wc -l", shell=True, stderr = STDOUT)
	# print(diff1)  # b'0\n'
	if int(diff9) == 0:
		score += 1.5
		comments += "event_count_alive MATCHED "
	else:
		comments += "event_count_alive MISMATCHED "
	if int(diff10) == 0:
		score += 1.5
		comments += "event_count_dead MATCHED "
	else:
		comments += "event_count_dead MISMATCHED "
	if int(diff7) == 0:
		score += 1.5
		comments += "encounter_count_alive MATCHED "
	else:
		comments += "encounter_count_alive MISMATCHED "
	if int(diff8) == 0:
		score += 1.5
		comments += "encounter_count_dead MATCHED "
	else:
		comments += "encounter_count_dead MISMATCHED "
	if int(diff11) == 0:
		score += 1.5
		comments += "record_length_alive MATCHED "
	else:
		comments += "record_length_alive MISMATCHED "
	if int(diff12) == 0:
		score += 1.5
		comments += "record_length_dead MATCHED "
	else:
		comments += "record_length_dead MISMATCHED "

	if int(diff1) == 0:
		score += 1
		comments += "common_diag_alive MATCHED "
	else:
		comments += "common_diag_alive MISMATCHED "
	if int(diff2) == 0:
		score += 1
		comments += "common_diag_dead MATCHED "
	else:
		comments += "common_diag_dead MISMATCHED "
	if int(diff3) == 0:
		score += 1
		comments += "common_lab_alive MATCHED "
	else:
		comments += "common_lab_alive MISMATCHED "
	if int(diff4) == 0:
		score += 1
		comments += "common_lab_dead MATCHED "
	else:
		comments += "common_lab_dead MISMATCHED "
	if int(diff5) == 0:
		score += 1
		comments += "common_med_alive MATCHED "
	else:
		comments += "common_med_alive MISMATCHED "
	if int(diff6) == 0:
		score += 1
		comments += "common_med_dead MATCHED "
	else:
		comments += "common_med_dead MISMATCHED "

	return score, comments

if __name__ == '__main__':
	
	parser = argparse.ArgumentParser(description = "Grading script for HW2 2.1 hive session, should be run from student's hive folder")
	parser.add_argument("expected_output_hive_dir", help= "Folder with gold-standard statistics hive data in its sub-folders, e.g. expected_output/hive/")
	args = parser.parse_args()

	totalscore, comments = hive_grading(args.expected_output_hive_dir)

	print("{},{}".format(totalscore, comments))






