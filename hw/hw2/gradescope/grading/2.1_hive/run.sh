#!/bin/bash
#
# Description:
#   Run Hive query for descriptive statistics
#
# Parameters:
#   $1: path to the event_statistics.hql file
#
# Usage:
#   run.sh
#
# Requires:
#   - events.csv data present on HDFS at /input/events/
#   - mortality.csv data present on HDFS at /input/mortality/


hive -f $1
