#!/bin/bash
#
# Description:
#   Run Python SGD
#
# Parameters:
#   None
#
# Usage:
#   run.sh
#
# Requires:
#   - Pig training output data present at ../pig/training/part-r-00000

cat ../pig/training/part-r-00000 | python train.py -f 3618 -e 0.01 -c 0.01
