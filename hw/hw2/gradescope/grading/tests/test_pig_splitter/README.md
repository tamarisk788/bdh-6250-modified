The etl.pig in this folder has two mistakes in the aliveevents and features_normalized sections.

Running the script directly will give the student 4.0 points as most sections will be broken by the aliveevents mistakes.

Test the splitter script and running the sub-sections, which should give the student 14.0 points by using the correct data between sections.
