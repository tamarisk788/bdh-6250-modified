import numpy as np
from sklearn.datasets import load_svmlight_file
from sklearn.linear_model import LogisticRegression
from sklearn.svm import LinearSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import *

from . import utils

# setup the randoms tate
RANDOM_STATE = 545510477

#input: X_train, Y_train and X_test
#output: Y_pred
def logistic_regression_pred(X_train, Y_train, X_test):
	#train a logistic regression classifier using X_train and Y_train. Use this to predict labels of X_test
	#use default params for the classifier
	logreg_classifier = LogisticRegression(random_state=RANDOM_STATE)
	logreg_classifier.fit(X_train,Y_train)
	Y_test = logreg_classifier.predict(X_test)
	return Y_test
	#pass

#input: X_train, Y_train and X_test
#output: Y_pred
def svm_pred(X_train, Y_train, X_test):
	#train a SVM classifier using X_train and Y_train. Use this to predict labels of X_test
	#use default params for the classifier
	svm_classifier = LinearSVC(random_state=RANDOM_STATE)
	svm_classifier.fit(X_train,Y_train)
	Y_test = svm_classifier.predict(X_test)
	return Y_test
	#pass

#input: X_train, Y_train and X_test
#output: Y_pred
def decisionTree_pred(X_train, Y_train, X_test):
	#train a logistic regression classifier using X_train and Y_train. Use this to predict labels of X_test
	#use max_depth as 5
	dt_classifier = DecisionTreeClassifier(max_depth=5, random_state=RANDOM_STATE)
	dt_classifier.fit(X_train,Y_train)
	Y_test = dt_classifier.predict(X_test)
	return Y_test
	#pass

#input: Y_pred,Y_true
#output: accuracy, auc, precision, recall, f1-score
def classification_metrics(Y_pred, Y_true):
	#NOTE: It is important to provide the output in the same order
	acc = accuracy_score(Y_true,Y_pred)
	fpr, tpr, thresholds = roc_curve(Y_true, Y_pred)
	auc_ = auc(fpr, tpr)
	precision = precision_score(Y_true, Y_pred)
	recall = recall_score(Y_true, Y_pred)
	f1score = f1_score(Y_true, Y_pred)
	#cmat = confusion_matrix(Y_true, Y_pred)
	return acc, auc_, precision, recall, f1score
	#pass

#input: Name of classifier, predicted labels, actual labels
def display_metrics(classifierName,Y_pred,Y_true):
	print("______________________________________________")
	print(("Classifier: "+classifierName))
	acc, auc_, precision, recall, f1score = classification_metrics(Y_pred,Y_true)
	print(("Accuracy: "+str(acc)))
	print(("AUC: "+str(auc_)))
	print(("Precision: "+str(precision)))
	print(("Recall: "+str(recall)))
	print(("F1-score: "+str(f1score)))
	print("______________________________________________")
	print("")

def main():
	X_train, Y_train = utils.get_data_from_svmlight("../deliverables/features_svmlight.train")
	X_test, Y_test = utils.get_data_from_svmlight("../data/features_svmlight.validate")
	'''
	utils.generate_submission("../deliverables/features_final.test",logistic_regression_pred(X_train,Y_train,X_test))
	f=open("submission_file.csv")
	lines=f.readlines()
	Y_pred=[]
	for i in range(len(lines)):
		if i==0:
			continue
		Y_pred.append(float(lines[i].split(',')[1]))

	'''
	display_metrics("Logistic Regression",logistic_regression_pred(X_train,Y_train,X_test),Y_test)
	display_metrics("SVM",svm_pred(X_train,Y_train,X_test),Y_test)
	display_metrics("Decision Tree",decisionTree_pred(X_train,Y_train,X_test),Y_test)
	
	#display_metrics("Logistic Regression",Y_pred,Y_test)

if __name__ == "__main__":
	main()
	
