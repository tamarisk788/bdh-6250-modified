import sys
import untangle
import pdb
def main(filename):
    #print "\nFile: ", filename
    obj = untangle.parse(filename)
    
    # test-case, #tests, failures, errors, test1, time, pass, failure message , test2, time, pass, failure message, ... 
    results = []
    
    if hasattr(obj.testsuite, 'testcase'):
        results.append(obj.testsuite.testcase[0]['classname'])
        results.append(obj.testsuite['tests'])
        results.append(obj.testsuite['failures'])
        results.append(obj.testsuite['errors'])
    
        for test in obj.testsuite.testcase:
            results.append(test['name'])
            results.append(test['time'])
            if hasattr(test, 'failure'):
                results.append("FAIL")
                message = "'Type: " + test.failure['type']
                message+="; Message: " + test.failure['message'] + "'"
                results.append(message.decode('utf-8').replace(',',';').replace('\n',' '))
            elif hasattr(test, 'error'):
                results.append("ERROR")
                message = "'Type: " + test.error['type']
                message+="; Message: " + test.error['message'] + "'"
                results.append(message.decode('utf-8').replace(',',';').replace('\n',' '))
            else:
                results.append("PASS")
                results.append("")
    print(','.join(results))
    #return ','.join(results)

if __name__ == "__main__":
    main(sys.argv[1])