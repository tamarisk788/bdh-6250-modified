from nose.tools import with_setup, eq_, ok_,nottest, timed
from src.etl import read_csv, create_features, save_svmlight, calculate_index_date, filter_events, aggregate_events
import datetime 
import pandas as pd
import datetime
import filecmp 
import os, errno
import time
import re
def silentremove(filename):
    """ Copied from the internet. """
    try:
        os.remove(filename)
    except OSError as e: # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
            raise # re-raise exception if a different error occured

VALIDATION_FEATURES = "custom_tests/features_svmlight.train"
VALIDATION_DELIVERABLE = "custom_tests/features.train"

def setup_module ():
    global deliverables_path 
    deliverables_path = 'custom_tests/'

@nottest
def teardown_features_order():
    silentremove (VALIDATION_FEATURES)
    silentremove (VALIDATION_DELIVERABLE)

@nottest
def date_convert(x):
    for fmt in ('%Y-%m-%d %H:%M:%S', '%Y-%m-%d'):
        try:
            return datetime.datetime.strptime(x, fmt)
        except ValueError:
            pass

@nottest
def setup_index_date ():
    global events_df, mortality_df,feature_map_df
    events_df, mortality_df,feature_map_df = read_csv('custom_tests/data/etl_1/')    

@nottest
def teardown_index_date ():
    silentremove (deliverables_path + 'etl_index_dates.csv')

@nottest
def setup_filtered_events ():
    global events_df, mortality_df,feature_map_df
    events_df, mortality_df,feature_map_df = read_csv('custom_tests/data/etl_1/')    

@nottest
def teardown_filtered_events ():
    teardown_index_date()
    silentremove (deliverables_path + 'etl_filtered_events.csv')

@nottest
def setup_aggregate_events ():
    global events_df, mortality_df,feature_map_df
    events_df, mortality_df,feature_map_df = read_csv('custom_tests/data/etl_3/')    

@nottest
def teardown_aggregate_events ():
    teardown_filtered_events()
    silentremove (deliverables_path + 'etl_aggregated_events.csv')

@with_setup (setup_index_date, teardown_index_date)
def test_index_date ():
    
    expected_indx_dates = {99: datetime.datetime(2013, 10, 13, 0, 0), 1700: datetime.datetime(2013, 2, 11, 0, 0), 24581: datetime.datetime(2013, 11, 7, 0, 0), 3014: datetime.datetime(2015, 11, 17, 0, 0), 1200: datetime.datetime(2010, 5, 21, 0, 0), 19: datetime.datetime(2014, 2, 2, 0, 0), 2200: datetime.datetime(2012, 7, 10, 0, 0)}
    calculate_index_date(events_df, mortality_df, deliverables_path)
    
    indx_date_df = pd.read_csv(deliverables_path + 'etl_index_dates.csv')
    indx_date_df['indx_date'] = indx_date_df['indx_date'].apply(date_convert)

    indx_date = dict(list(zip(indx_date_df.patient_id, indx_date_df.indx_date)))

    if isinstance(indx_date, pd.DataFrame):
        indx_date =  dict(list(zip(indx_date.patient_id, indx_date.indx_date)))
    res= True
    msg = ""
    if len(indx_date) != len(expected_indx_dates):
        res = False
        msg = "Size mismatch"
    else: 
        for key, value in list(expected_indx_dates.items()):
            if key  not in  indx_date:
                res = False;
                msg = "Key %s missing. " %(key)
                break;
            if not abs(indx_date[key].date()  == value.date()):
                msg = "Values  %s , %s different. " %(indx_date[key].date(), value.date())
                res = False;
                break;

    eq_(res, True, "Index dates do not match - " + msg)

@with_setup (setup_filtered_events, teardown_filtered_events)
def test_filtered_events ():
    
    expected_data = []
    with open('custom_tests/expected_etl_filtered_events.csv') as expected_file:
        expected_data = expected_file.readlines()
        expected_data = expected_data[1:]
        expected_data.sort()

    indx_dates_df = calculate_index_date(events_df, mortality_df, deliverables_path)
    #indx_dates_df = pd.read_csv('custom_tests/expected_etl_index_dates.csv')
    #indx_dates_df['indx_date'] = indx_dates_df['indx_date'].apply(date_convert)

    filter_events(events_df, indx_dates_df, deliverables_path)
    actual_data = []
    with open(deliverables_path + 'etl_filtered_events.csv') as actual_file:
        actual_data = actual_file.readlines()
        actual_data = actual_data[1:]
        actual_data.sort()

    res = True
    msg = ""
    for idx,line in enumerate(expected_data):
        first = line.split(',')
        second = actual_data[idx].split(',')
        if not (float(first[0])==float(second[0]) and first[1] == second[1] and (first[2]==second[2]) if second[2]=='\n' else abs(float(first[2])-float(second[2]))<=0.1):
            res = False
            msg = "Mistmatch on line %d. Expected: %s  Actual: %s " %(idx+1, line, actual_data[idx])
            break   

    eq_(res, True, "Filtered events do not match. " + msg)

@with_setup (setup_aggregate_events, teardown_aggregate_events)
def test_aggregate_events ():
    
    expected_data = []
    with open('custom_tests/expected_etl_aggregated_events.csv') as expected_file:
        expected_data = expected_file.readlines()
        expected_data = expected_data[1:]
        expected_data.sort()

    filtered_events_df = pd.read_csv('custom_tests/filtered_events.csv')
    aggregated_events_df = aggregate_events(filtered_events_df, mortality_df, feature_map_df, deliverables_path)
    
    actual_data = []
    with open(deliverables_path + 'etl_aggregated_events.csv') as actual_file:
        actual_data = actual_file.readlines()
        actual_data = actual_data[1:]
        actual_data.sort()

    res = True
    msg = ""
    for idx,line in enumerate(expected_data):
        first = line.split(',')
        second = actual_data[idx].split(',')
        if not (float(first[0])==float(second[0]) and float(first[1]) == float(second[1]) and abs(float(first[2])-float(second[2]))<=0.1):
            res = False
            msg = "Mistmatch on line %d. Expected: %s  Actual: %s " %(idx+1, line, actual_data[idx])
            break    
    eq_(res, True, "Aggregated events do not match. " + msg)

@with_setup (None, teardown_features_order)
def test_features_order():
    patient_features = {2293.0: [(2741.0, 1.0), (2751.0, 1.0), (2760.0, 1.0), (2841.0, 1.0), (2880.0, 1.0), (2914.0, 1.0), (2948.0, 1.0), (3008.0, 1.0), (3049.0, 1.0), (1193.0, 1.0), (1340.0, 1.0), (1658.0, 1.0), (1723.0, 1.0), (2341.0, 1.0), (2414.0, 1.0)], 120.0: [(2741.0, 1.0), (2751.0, 2.0), (2760.0, 3.0), (1721.0, 1.0), (2341.0, 1.0)]}
    mortality = {2293.0: 1.0, 120.0:0.0}
    save_svmlight(patient_features, mortality, VALIDATION_FEATURES, VALIDATION_DELIVERABLE)
    #result = filecmp.cmp('custom_tests/expected_features.train', VALIDATION_DELIVERABLE)
    array = []
    array2 = []
    i = 0
    j = 0
    with open(VALIDATION_DELIVERABLE, 'r') as fin:
        for line in fin:
            for x in line.split():
                if i == 0:
                    x = re.sub("[^0-9]", "", x)
                    i += 1;
                for y in x.split(':'):
                    if(int(float(y)) == 0 or int(float(y)) == 1):
                        y = int(float(y))
                    array.append(y)
        print(array)
    with open('custom_tests/expected_features.train', 'r') as fin:
        for line in fin:
            for a in line.split():
                if j == 0:
                    a = re.sub("[^0-9]", "", a)
                    j += 1
                for b in a.split(':'):
                    if(int(float(b)) == 0 or int(float(b)) == 1):
                        b = int(float(b))
                    array2.append(b)
        print(array2)
    eq_(True, array == array2, "Features are not same")
