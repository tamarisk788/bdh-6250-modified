from nose.tools import with_setup, eq_, ok_, assert_almost_equals
from src.event_statistics import record_length_metrics, encounter_count_metrics, event_count_metrics
import filecmp 
import os, errno
import pandas as pd

def read_csv(filepath):
    #Columns in events.csv - patient_id,event_id,event_description,timestamp,value
    events_df = pd.read_csv(filepath + 'events.csv', parse_dates=['timestamp'])
     
    #Columns in mortality_event.csv - patient_id,timestamp,label
    mortality_df = pd.read_csv(filepath + 'mortality_events.csv', parse_dates=['timestamp'])
 
    return events_df, mortality_df

def silentremove(filename):
    """ Copied from the internet. """
    try:
        os.remove(filename)
    except OSError as e: # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
            raise # re-raise exception if a different error occured

def setup_module ():
    global events, mortality
    events, mortality = read_csv('custom_tests/data/statistics/')

def test_event_count():
    actual = event_count_metrics(events, mortality)
    expected = (1, 3743, 1096.6296296296296, 1, 12376, 650.4153846153846)
    res = 0

    for i in range(6):
        if abs(float(actual[i])-float(expected[i])) <= 0.1:
            res += 0.5


    eq_(res, float(3), res)

    '''
    assert_almost_equals(expected[0], actual[0],places=2, msg="UNEQUAL Expected:%s, Actual:%s" %(expected, actual))
    assert_almost_equals(expected[1], actual[1],places=2, msg="UNEQUAL Expected:%s, Actual:%s" %(expected, actual))
    assert_almost_equals(expected[2], actual[2],places=2, msg="UNEQUAL Expected:%s, Actual:%s" %(expected, actual))
    assert_almost_equals(expected[3], actual[3],places=2, msg="UNEQUAL Expected:%s, Actual:%s" %(expected, actual))
    assert_almost_equals(expected[4], actual[4],places=2, msg="UNEQUAL Expected:%s, Actual:%s" %(expected, actual))
    assert_almost_equals(expected[5], actual[5],places=2, msg="UNEQUAL Expected:%s, Actual:%s" %(expected, actual))
    '''

def test_encounter_count():
    actual = encounter_count_metrics(events, mortality)
    print(actual)
    #expected =  (1, 90, 27.11111111111111, 1, 232, 17.953846153846154)
    expected = (1, 90, 27.11111111111111, 1, 237, 18.115384615384617)
    res = 0
    for i in range(6):
        if abs(float(actual[i])-float(expected[i])) <= 0.1:
            res += 0.5
    

    eq_(res, float(3), res)

    '''
    assert_almost_equals(expected[0], actual[0],places=2, msg="UNEQUAL Expected:%s, Actual:%s" %(expected, actual))
    assert_almost_equals(expected[1], actual[1],places=2, msg="UNEQUAL Expected:%s, Actual:%s" %(expected, actual))
    assert_almost_equals(expected[2], actual[2],places=2, msg="UNEQUAL Expected:%s, Actual:%s" %(expected, actual))
    assert_almost_equals(expected[3], actual[3],places=2, msg="UNEQUAL Expected:%s, Actual:%s" %(expected, actual))
    assert_almost_equals(expected[4], actual[4],places=2, msg="UNEQUAL Expected:%s, Actual:%s" %(expected, actual))
    assert_almost_equals(expected[5], actual[5],places=2, msg="UNEQUAL Expected:%s, Actual:%s" %(expected, actual))
    '''

def test_record_length():
    actual = record_length_metrics(events, mortality)
    #expected = (0, 394, 140.22222222222223, 0, 1871, 143.6846153846154)
    expected = (0, 530, 122.81481481481481, 0, 1989, 180.16923076923078)
    res = 0.0
    for i in range(6):
        if abs(float(actual[i])-float(expected[i])) <= 0.1:
            res += 1 
    

    eq_(res, float(6), 4*res/float(6))

    '''
    assert_almost_equals(expected[0], actual[0],places=2, msg="UNEQUAL Expected:%s, Actual:%s" %(expected, actual))
    assert_almost_equals(expected[1], actual[1],places=2, msg="UNEQUAL Expected:%s, Actual:%s" %(expected, actual))
    assert_almost_equals(expected[2], actual[2],places=2, msg="UNEQUAL Expected:%s, Actual:%s" %(expected, actual))
    assert_almost_equals(expected[3], actual[3],places=2, msg="UNEQUAL Expected:%s, Actual:%s" %(expected, actual))
    assert_almost_equals(expected[4], actual[4],places=2, msg="UNEQUAL Expected:%s, Actual:%s" %(expected, actual))
    assert_almost_equals(expected[5], actual[5],places=2, msg="UNEQUAL Expected:%s, Actual:%s" %(expected, actual))
    '''
