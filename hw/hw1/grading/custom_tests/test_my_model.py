from nose.tools import with_setup, ok_, eq_, assert_almost_equals, nottest
from src.utils import get_data_from_svmlight
from src.models_partc import logistic_regression_pred,svm_pred,decisionTree_pred,classification_metrics
from src.cross import get_acc_auc_kfold
from src.my_model import my_features, my_classifier_predictions
import pandas as pd
from sklearn.metrics import *

def test_num_patients():
	expected = True
	f = open("deliverables/test_features.txt").readlines()
	N = int(len(f))
	actual = (N==633 or N==634 or N==635)
	eq_(expected, actual,msg="Test for number of test patients Expected:%s, Actual:%s" %(expected, actual))

@nottest
def test_code():
	expected = True
	X_train, Y_train, X_test = my_features()
	Y_pred = my_classifier_predictions(X_train,Y_train,X_test)
	actual = (sum(Y_pred)<635 and sum(Y_pred)>-1)
	eq_(expected, actual,msg="Test to check if functions are implemented and labels are within range Expected:%s, Actual:%s" %(expected, actual))

def test_get_auc():
	# my_pred = pd.DataFrame.from_csv("deliverables/my_predictions.csv",index_col=None)
	my_pred = pd.read_csv("deliverables/my_predictions.csv",index_col=None)
	my_pred.columns=['patient_id','pred_label']
	actual_label = pd.read_csv("custom_tests/data/solutions_kaggle.csv",index_col=None)
	actual_label.columns=['patient_id','true_label']
	df = my_pred.merge(actual_label,on='patient_id')
	Y_pred = df['pred_label']
	Y_true = df['true_label']
	fpr, tpr, thresholds = roc_curve(Y_true, Y_pred)
	auc_ = auc(fpr, tpr)
	print(("Your AUC for the entire set of test patients is "+str(auc_)))
	eq_(True,True)

