#!/bin/bash
op_file="grades.csv"
touch $op_file
chmod a+rw $op_file
base_directory=$(pwd)

test_files=('custom_tests/test'*.py)
tests=($(sort <<<"${test_files[*]}"))

header="name, gt-id, gt-account,Submission"
# prepare the grading csv here
for test_case in "${tests[@]}"
do
    header+=", ${test_case##*/}, Total_Test_Cases, Failed, Errors"
    nosetests -v --collect-only  $test_case > a.txt 2>&1 
    sed '$d' a.txt | sed '$d' | sed '$d' | sed '$d' | sed '$d' > b.txt
    while read p; do
         header+=", Test Case, Time, Status, Message"
    done <b.txt
done
rm b.txt
rm a.txt
echo $header >> $op_file

# grade the homework here
homework_directory="homework_1"
for file_path in "$homework_directory"/*gz
do
    homework_directory="homework_1"
    student_entry=${file_path##*/}
    student_name=${student_entry%%_*}
    echo $student_entry
    echo $student_name
    #folder_path="$homework_directory/$student_name" #Submission attachment(s)"
    echo "$file_path is being tested"
    if test -e "$file_path" # *-hw*.tar.gz
    then
        file_name="${file_path##*/}"
        echo "Found homework for $student_name, grading $file_name"
        tar -zxf "$file_path" -C $homework_directory
        file_path=${file_name##*_}
        homework_directory="$homework_directory/${file_path%%.*}"
        IFS="-" read -r -a gt_fields <<< "${file_path%%.*}"
        echo $homework_directory
        cp -r "custom_tests" "$homework_directory"
        #cp -r "data" "${folder_path}/${gt_fields[0]}-${gt_fields[1]}-hw1/"
        cp  "parse_nosetest.py" "$homework_directory"
        cd $homework_directory
        touch 'src/__init__.py'
        rm "tmp.txt"
        #sed -i.bak 's/\.\.\/data/data/g' 'code/my_model.py' 
        #sed -i.bak 's/\.\.\/deliverables/deliverables/g' 'code/my_model.py' 
        for test_case in "${tests[@]}"
        do
            #echo $test_case
            filename=$(basename "$test_case")
            filename="${filename%.*}"
            nosetests -s --nocapture --with-xunit $test_case --xunit-file "$filename".xml > op.txt
            python3 parse_nosetest.py "$filename".xml >> tmp.txt
        done
        
        results=$(paste -d, -s "tmp.txt")

        echo "\"$student_name\"", ${gt_fields[0]}, ${gt_fields[1]}, "Found",$results >> $base_directory/$op_file
        cd $base_directory
    else
        echo "Skipping grading for $student_name, $file_name, missing attachment"
        echo "\"$student_name\"", "", "", "Not found" >> $op_file
    fi
done