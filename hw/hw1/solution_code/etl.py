import pandas as pd 
import numpy as np
from datetime import datetime
from datetime import timedelta
import utils
import pdb 

def read_csv(filepath):
    #Columns in events.csv - patient_id,event_id,event_description,timestamp,value
    events_df = pd.read_csv(filepath + 'events.csv', parse_dates=['timestamp'])
    
    #Columns in mortality_event.csv - patient_id,timestamp,label
    mortality_df = pd.read_csv(filepath + 'mortality_events.csv', parse_dates=['timestamp'])

    #Columns in event_feature_map.csv - idx,event_id
    feature_map_df = pd.read_csv(filepath + 'event_feature_map.csv')

    return events_df, mortality_df, feature_map_df


def calculate_index_date(events_df, mortality_df, deliverables_path):
    #Create list of patients alive ( mortality_events.csv only contains information about patients deceased)
    dead_patients  = mortality_df['patient_id']
    all_patients  = pd.unique(events_df.patient_id.ravel())
    alive_patients = list(set(all_patients) - set(dead_patients))

    
    #Split events into two groups based on whether the patient is alive or deceased
    alive_events_df = events_df[events_df['patient_id'].isin(alive_patients)]
    dead_events_df = events_df[events_df['patient_id'].isin(dead_patients)]


    #Calculate index date for each patient alive
    max_indx_date_alive = alive_events_df[['patient_id','timestamp']].sort_values(['patient_id', 'timestamp'], ascending=[1,0]).groupby('patient_id', as_index=False).first()
    max_indx_date_alive['timestamp'] = max_indx_date_alive['timestamp'].apply(lambda x: x.date())

    indx_date_dead = mortality_df[['patient_id','timestamp']]
    indx_date_dead['timestamp'] = indx_date_dead['timestamp'].apply(lambda x: x.date() + timedelta(days=-30))
    indx_date = pd.concat([max_indx_date_alive, indx_date_dead], axis=0)
    indx_date.columns = ['patient_id', 'indx_date']

    '''
    Save indx_date to a csv file in the deliverables folder named as etl_index_dates.csv. 
    Use the global variable deliverables_path while specifying the filepath. 
    Each row is of the form patient_id, indx_date.
    The csv file should have a header 
    For example if you are using Pandas, you could write: 
        indx_date.to_csv(deliverables_path + 'etl_index_dates.csv', columns=['patient_id', 'indx_date'], index=False)
    '''
    indx_date.to_csv(deliverables_path + 'etl_index_dates.csv', columns=['patient_id', 'indx_date'], index=False)

    return indx_date


def filter_events(events_df, indx_date_df, deliverables_path):
    #Join index date with events 
    events_df = pd.merge(events_df, indx_date_df, how='inner', on=['patient_id', 'patient_id'])

    #Filter events occuring in the observation window(IndexDate-2030 to IndexDate - 30)
    events_df['timestamp'] = events_df['timestamp'].apply(lambda x : x.date())
    date_diff_col = events_df[['indx_date']].sub(events_df['timestamp'], axis=0).astype('timedelta64[D]').astype(int)
    date_diff_col.columns = ['diff']
    events_df = pd.concat([events_df[['patient_id','event_id' , 'value']], date_diff_col], axis=1)
    filtered_events = events_df[(events_df['diff'] >= 0) & (events_df['diff'] <= 2000)]


    #filtered_events_df should be a Pandas data frame of form (patient_id , event_id , value)

    '''
    Save filtered_events to a csv file in the deliverables folder named as etl_filtered_events.csv. 
    Use the global variable deliverables_path while specifying the filepath. 
    Each row is of the form patient_id, event_id, value.
    The csv file should have a header 
    For example if you are using Pandas, you could write: 
        filtered_events.to_csv(deliverables_path + 'etl_filtered_events.csv', columns=['patient_id', 'event_id', 'value'], index=False)
    '''
    filtered_events.to_csv(deliverables_path + 'etl_filtered_events.csv', columns=['patient_id', 'event_id', 'value'], index=False)

    return filtered_events


def aggregate_events(filtered_events_df, mortality_df,feature_map_df, deliverables_path):
    
    #Replace event_id's with index available in event_feature_map.csv
    lab_events_df = filtered_events_df['event_id'].str.startswith('LAB', na=False)
    lab_events_df.name='lab'
    filtered_events_df = pd.concat([filtered_events_df, lab_events_df], axis=1)
    filtered_events_df['lab_new'] = filtered_events_df['lab'].astype(int)
    
    mapped_events_df = pd.merge(filtered_events_df, feature_map_df, how='inner', on=['event_id', 'event_id'])
    mapped_events = pd.merge(mapped_events_df, mortality_df[['patient_id','label']], how='left', on=['patient_id', 'patient_id'])
    mapped_events['label'].fillna(0, inplace=True)

    #Remove events with n/a values
    mapped_events = mapped_events.dropna(axis=0, how='any')
    grpd_lab = mapped_events[mapped_events['lab']== True][['patient_id', 'idx','lab_new', 'label']].groupby(['patient_id', 'idx','label'],  as_index=False)['lab_new'].sum()
    grpd_lab.columns=['patient_id', 'idx', 'label', 'value']
    grpd_others = mapped_events[mapped_events['lab']== False][['patient_id', 'idx','value', 'label']].groupby(['patient_id', 'idx','label'],  as_index=False)['value'].sum()
    grpd = pd.concat([grpd_lab, grpd_others], axis =0 )
    grpd.columns=['patient_id', 'idx', 'label', 'feature_value']
    
    #Normalize the values obtained above using min-max normalization
    max_values = grpd[['idx','feature_value']].groupby('idx',  as_index=False).max()
    grpd = pd.merge(grpd, max_values, how='inner', on=['idx', 'idx'])
    normalized_value = grpd['feature_value_x'] / grpd.feature_value_y
    grpd = pd.concat([grpd[['patient_id','idx','label']], normalized_value], axis=1)
    grpd.columns=['patient_id', 'feature_id', 'label', 'feature_value']


    '''
    Save aggregated_events to a csv file in the deliverables folder named as etl_aggregated_events.csv. 
    Use the global variable deliverables_path while specifying the filepath. 
    Each row is of the form patient_id, event_id, value.
    The csv file should have a header .
    For example if you are using Pandas, you could write: 
        aggregated_events.to_csv(deliverables_path + 'etl_aggregated_events.csv', columns=['patient_id', 'feature_id', 'feature_value'], index=False)
    '''
    grpd.to_csv(deliverables_path + 'etl_aggregated_events.csv', columns=['patient_id', 'feature_id', 'feature_value'], index=False)

    return grpd

def create_features(events_df, mortality_df, feature_map_df):
    
    deliverables_path = '../deliverables/'

    #Calculate index date
    indx_date_df = calculate_index_date(events_df, mortality_df, deliverables_path)
    
    #Filter events in the observation window
    filtered_events_df = filter_events(events_df, indx_date_df,  deliverables_path)
    
    #Aggregate the event values for each patient 
    aggregated_events_df = aggregate_events(filtered_events_df, mortality_df, feature_map_df, deliverables_path)

    '''
    Create two dictionaries - 
    1. patient_features :  Key - patient_id and value is array of tuples(feature_id, feature_value)
    2. mortality : Key - patient_id and value is mortality label
    '''
    patient_features = {}
    mortality = {}
    for idx, row in aggregated_events_df.iterrows():
        if row.patient_id not in patient_features:
            patient_features[row.patient_id] = []
        patient_features[row.patient_id].append((row.feature_id, row.feature_value))
        mortality[row.patient_id] = row.label

    return patient_features, mortality

def save_svmlight(patient_features, mortality, op_file, op_deliverable):
    #Write to file in svmlight format . Note please make sure the features are ordered in ascending order. 
    s_patients = {}
    for key,value in list(patient_features.items()):
        s_patients[key] = sorted(value, key=lambda tup: tup[0])
    target = open(op_file, 'wb')
    deliverable = open(op_deliverable, 'wb')
    
    for key in sorted(s_patients):
        features = utils.bag_to_svmlight(s_patients[key])
        target.write(bytes("%s %s \n" %(mortality[key], features),'UTF-8'));
        deliverable.write(bytes("%d %s %s \n" %(key, mortality[key], features),'UTF-8'));

def main():
    train_path = '../data/train/'
    events_df, mortality_df, feature_map_df = read_csv(train_path)
    patient_features, mortality = create_features(events_df, mortality_df, feature_map_df)
    save_svmlight(patient_features, mortality, '../deliverables/features_svmlight.train', '../deliverables/features.train')

if __name__ == "__main__":
    main()




