from models_partc import logistic_regression_pred, classification_metrics
# from sklearn.cross_validation import KFold, ShuffleSplit
from sklearn.model_selection import KFold, ShuffleSplit
from numpy import mean

import utils

RANDOM_STATE = 545510477

#input: training data and corresponding labels
#output: accuracy, auc
def get_acc_auc_kfold(X,Y,k=5):
	#First get the train indices and test indices for each iteration
	#Then train the classifier accordingly
	#Report the mean accuracy and mean auc of all the folds
	# N = X.shape[0]
	aucs = []
	accs = []
	cms = []
	#print N
	# kf = KFold(N, n_folds=k)
	kf = KFold(n_splits=k)
	for train_idx, test_idx in kf.split(X):
		X_train = X[train_idx]
		Y_train = Y[train_idx]
		X_test = X[test_idx]
		Y_test = Y[test_idx]
		Y_pred = logistic_regression_pred(X_train, Y_train, X_test)
		auc_ = classification_metrics(Y_pred,Y_test)[1]
		acc = classification_metrics(Y_pred,Y_test)[0]
		#cm = classification_metrics(Y_pred, Y_test)[5]
		#cms.append(cm)
		aucs.append(auc_)
		accs.append(acc)
	return mean(accs),mean(aucs)#,sum(cms)
	#pass

#input: training data and corresponding labels
#output: accuracy, auc
def get_acc_auc_randomisedCV(X,Y,iterNo=5,test_percent=0.2):
	#First get the train indices and test indices for each iteration
	#Then train the classifier accordingly
	#Report the mean accuracy and mean auc of all the iterations
	# N = X.shape[0]
	aucs = []
	accs = []
	# ss = ShuffleSplit(N, n_iter=iterNo, test_size=test_percent)
	ss = ShuffleSplit(n_splits=iterNo, test_size=test_percent, random_state = 0)
	for train_idx, test_idx in ss.split(X):
		X_train = X[train_idx]
		Y_train = Y[train_idx]
		X_test = X[test_idx]
		Y_test = Y[test_idx]
		Y_pred = logistic_regression_pred(X_train, Y_train, X_test)
		auc_ = classification_metrics(Y_pred,Y_test)[1]
		acc = classification_metrics(Y_pred,Y_test)[0]
		aucs.append(auc_)
		accs.append(acc)
	return mean(accs),mean(aucs)
	#pass

def main():
	X,Y = utils.get_data_from_svmlight("../deliverables/features_svmlight.train")
	print("Classifier: Logistic Regression")
	acc_k,auc_k = get_acc_auc_kfold(X,Y)
	print(("Average Accuracy in KFold CV: "+str(acc_k)))
	print(("Average AUC in KFold CV: "+str(auc_k)))
	acc_r,auc_r = get_acc_auc_randomisedCV(X,Y)
	print(("Average Accuracy in Randomised CV: "+str(acc_r)))
	print(("Average AUC in Randomised CV: "+str(auc_r)))

if __name__ == "__main__":
	main()

