#!/bin/bash

# clean up
rm -rf homework1
rm -rf homework1.tar.gz

# copy data
mkdir -p homework1/deliverables

cp environment.yml homework1/
cp -r data homework1/
cp -r src homework1/
cp -r tests homework1/
cp writeup/homework1.* homework1/

# clean up unnecessary files
./cleanup.sh

# create tar
tar cvzf homework1.tar.gz homework1/

# remove the folder
rm -rf homework1