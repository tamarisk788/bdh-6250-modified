# HW1 autograder on GradeScope

This is the instructions how to setup HW1 autograder on GradeScope

## Getting Started

For more information to get familiar with GradeScope, please check https://bitbucket.org/realsunlab/bdh/src/spring2019/hw/hw5/gradescope/

## Required files
	- requirements.txt
	- run_autograder
	- run_tests.py
	- setup.sh
	- tests
	- data


```
Compress above files and upload the zipped file to GradeScope, then it's ready to grade students' submissions automatically
```

### Restriction
test_code() testing case is commented out because the input to ```my_features()``` is large, while GradeScope RAM configuration is up to 1GB at most.
In the future, if GradeScope configurations get upgraded we can continue to use this case but we have to require students (add comments) to use complete relative path in ```my_model.my_features()``` in students' code like beblow for the convenience of grading:

Unqualified:
```
'../data/train/events' 
```
Qualified
```
os.path.join(os.path.dirname(__file__), '../data/train/events')
```

###tips for students for submission:
submission structure:
```
- <your gtid>-<your gt account>-hw1
	- src 
	- deliverables
```
then compress it to .zip file and upload on gradescope

### Author:
Ming Liu (mliu302@gatech)



  