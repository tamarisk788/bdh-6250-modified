import warnings
warnings.filterwarnings("ignore")
import unittest
from gradescope_utils.autograder_utils.decorators import weight, visibility
import os
import scipy as sp
import numpy as np
import pandas as pd
from sklearn.metrics import *
import time
import datetime
import filecmp 
import os, errno
import time
import re
import filecmp
import sys
# sys.path.append('/autograder/source/')
sys.path.append('/autograder/source/src/')
from event_statistics import record_length_metrics, encounter_count_metrics, event_count_metrics
from nose.tools import with_setup, ok_, eq_, assert_almost_equals, nottest, timed
from utils import get_data_from_svmlight
from models_partc import logistic_regression_pred,svm_pred,decisionTree_pred,classification_metrics
from cross import get_acc_auc_kfold, get_acc_auc_randomisedCV
from etl import read_csv, create_features, save_svmlight, calculate_index_date, filter_events, aggregate_events
from my_model import my_features, my_classifier_predictions




class TestComplex(unittest.TestCase):
	global X_train, Y_train
	X_train, Y_train = get_data_from_svmlight("tests/data/models/features_svmlight.train")
	
	global X_test, Y_test
	X_test, Y_test = get_data_from_svmlight("tests/data/models/features_svmlight.validate")
	@weight(1.67)
	def test_accuracy_lr_partc(self):
		expected = 0.680952380952
		Y_pred = logistic_regression_pred(X_train,Y_train,X_test)
		actual = classification_metrics(Y_pred,Y_test)[0]
		# assert_almost_equals(expected, actual,places=2, msg="UNEQUAL Expected:%s, Actual:%s" %(expected, actual))
		real = abs(expected - actual) < 0.05
		eq_(True, real,msg="UNEQUAL with diff larger than 0.05, Expected:%s, Actual:%s" %(expected, actual))

	@weight(1.67)
	def test_auc_svm_partc(self):
		expected = 0.605555555556
		Y_pred = svm_pred(X_train,Y_train,X_test)
		actual = classification_metrics(Y_pred,Y_test)[1]
		# assert_almost_equals(expected, actual,places=1, msg="UNEQUAL Expected:%s, Actual:%s" %(expected, actual))
		real = abs(expected - actual) < 0.05
		eq_(True, real,msg="UNEQUAL with diff larger than 0.05, Expected:%s, Actual:%s" %(expected, actual))

	@weight(1.66)
	def test_fscore_dt_partc(self):
		expected = 0.606382978723
		Y_pred = decisionTree_pred(X_train,Y_train,X_test)
		actual = classification_metrics(Y_pred,Y_test)[4]
		# assert_almost_equals(expected, actual,places=2, msg="UNEQUAL Expected:%s, Actual:%s" %(expected, actual))
		real = abs(expected - actual) < 0.05
		eq_(True, real,msg="UNEQUAL with diff larger than 0.05, Expected:%s, Actual:%s" %(expected, actual))

