import time
import pandas as pd
import numpy as np
from datetime import datetime

def read_csv(filepath):
    '''
    Read the events.csv and mortality_events.csv files. Variables returned from this function are passed as input to the metric functions.
    This function needs to be completed.
    '''
    events = pd.read_csv(filepath + 'events.csv', parse_dates=['timestamp'])

    mortality = pd.read_csv(filepath + 'mortality_events.csv', parse_dates=['timestamp'])

    return events, mortality

def event_count_metrics(events, mortality):
    '''
    Event count is defined as the number of events recorded for a given patient.
    This function needs to be completed.
    '''
    # rename columns in mortality to prevent column name conflict after joining with events
    mortality.rename(columns={'timestamp': 'deathtime', 'label': 'isdead'}, inplace=True)

    eventstats_byPatient = events.groupby(['patient_id'], as_index = False).count()
    
    # join eventstats_byPatient with mortality
    stats_byPatient = pd.merge(eventstats_byPatient, mortality, on='patient_id', how='left')

    alivefolks = stats_byPatient[np.isnan(stats_byPatient.isdead)]
    deadfolks = stats_byPatient[stats_byPatient.isdead == 1]

    avg_dead_event_count = deadfolks['event_id'].mean()
    max_dead_event_count = deadfolks['event_id'].max()
    min_dead_event_count = deadfolks['event_id'].min()

    avg_alive_event_count = alivefolks['event_id'].mean()
    max_alive_event_count = alivefolks['event_id'].max()
    min_alive_event_count = alivefolks['event_id'].min()

    return min_dead_event_count, max_dead_event_count, avg_dead_event_count, min_alive_event_count, max_alive_event_count, avg_alive_event_count

def encounter_count_metrics(events, mortality):
    '''
    Encounter count is defined as the count of unique dates on which a given patient visited the ICU. 
    This function needs to be completed.
    '''

    joined = pd.merge(events, mortality, on='patient_id', how='outer')
    print((joined.columns))
    joined['isdead'].fillna(0, inplace=True)

    alive = joined[joined['isdead'] == 0]
    dead = joined[joined['isdead'] == 1]

    ga = alive.groupby(['patient_id', 'timestamp'])['event_id'].count().reset_index(name="count")
    gd = dead.groupby(['patient_id', 'timestamp'])['event_id'].count().reset_index(name="count")

    ga_new = ga.groupby('patient_id')['timestamp'].count().reset_index(name="count")
    gd_new = gd.groupby('patient_id')['timestamp'].count().reset_index(name="count")

    avg_dead_encounter_count = gd_new['count'].mean()
    max_dead_encounter_count = gd_new['count'].max()
    min_dead_encounter_count = gd_new['count'].min()

    avg_alive_encounter_count = ga_new['count'].mean()
    max_alive_encounter_count = ga_new['count'].max()
    min_alive_encounter_count = ga_new['count'].min()
    
    return min_dead_encounter_count, max_dead_encounter_count, avg_dead_encounter_count, min_alive_encounter_count, max_alive_encounter_count, avg_alive_encounter_count

def record_length_metrics(events, mortality):
    '''
    Record length is the duration between the first event and the last event for a given patient. 
    This function needs to be completed.
    '''
    # rename columns in mortality to prevent column name conflict after joining with events
    mortality.rename(columns={'timestamp': 'deathtime', 'label': 'isdead'}, inplace=True)
    # join events with mortality
    patientStatus = pd.merge(events, mortality, on='patient_id', how='left')

    # separate out dead folks, alive folks
    alivefolks = patientStatus[np.isnan(patientStatus.isdead)].groupby('patient_id')
    deadfolks = patientStatus[patientStatus.isdead == 1].groupby('patient_id')

    alivemin = pd.DataFrame(alivefolks.timestamp.min())
    alivemin.rename(columns={'timestamp': 'mintime'}, inplace=True)
    alivemax = pd.DataFrame(alivefolks.timestamp.max())
    alivemax.rename(columns={'timestamp': 'maxtime'}, inplace=True)

    deadmin = pd.DataFrame(deadfolks.timestamp.min())
    deadmin.rename(columns={'timestamp': 'mintime'}, inplace=True)
    deadmax = pd.DataFrame(deadfolks.timestamp.max())
    deadmax.rename(columns={'timestamp': 'maxtime'}, inplace=True)

    alivedays = pd.concat([alivemin, alivemax], axis=1)
    deaddays = pd.concat([deadmin, deadmax], axis=1)

    alivedays['datediff'] = alivedays.apply(lambda x: (x['maxtime'] - x['mintime']).days, axis =1)   
    deaddays['datediff'] = deaddays.apply(lambda x: (x['maxtime'] - x['mintime']).days, axis =1)

    avg_dead_rec_len = deaddays['datediff'].mean()
    max_dead_rec_len = deaddays['datediff'].max()
    min_dead_rec_len = deaddays['datediff'].min()

    avg_alive_rec_len = alivedays['datediff'].mean()
    max_alive_rec_len = alivedays['datediff'].max()
    min_alive_rec_len = alivedays['datediff'].min()

    return min_dead_rec_len, max_dead_rec_len, avg_dead_rec_len, min_alive_rec_len, max_alive_rec_len, avg_alive_rec_len

def main():
    '''
    DONOT MODIFY THIS FUNCTION. 
    Just update the train_path variable to point to your train data directory.
    '''
    #Modify the filepath to point to the CSV files in train_data
    train_path = '../custom_tests/data/statistics/'
    events, mortality = read_csv(train_path)

    #Compute the event count metrics
    start_time = time.time()
    event_count = event_count_metrics(events, mortality)
    end_time = time.time()
    print(("Time to compute event count metrics: " + str(end_time - start_time) + "s"))
    print(event_count)

    #Compute the encounter count metrics
    start_time = time.time()
    encounter_count = encounter_count_metrics(events, mortality)
    end_time = time.time()
    print(("Time to compute encounter count metrics: " + str(end_time - start_time) + "s"))
    print(encounter_count)

    #Compute record length metrics
    start_time = time.time()
    record_length = record_length_metrics(events, mortality)
    end_time = time.time()
    print(("Time to compute record length metrics: " + str(end_time - start_time) + "s"))
    print(record_length)
    
if __name__ == "__main__":
    main()



