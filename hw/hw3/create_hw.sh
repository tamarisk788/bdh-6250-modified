#!/bin/bash

# =============================================================#
# Description:
#   This script creates the tar.gz file to deliver to students!
#
# Notes:
#  * COPYFILE_DISABLE=1 on the tar is needed for MacoS to prevent
#    ._ files from appearing on non-MacOS systems
#
# =============================================================#

# Clean-up any existing deployment
rm -rf hw3_student_release
rm -rf hw3.tar.gz

# Create folder structure
mkdir hw3

# Copy all phenotyping resources
cp -R phenotyping_resources hw3/

# Copy Phenotype Knowledge Base into deployment
cp -R phekb_criteria hw3/

# Copy incomplete student code into deployment
cp -R stu_code hw3/
mv hw3/stu_code hw3/code
# Clean useless generated file
rm -rf hw3/code/logs
rm -rf hw3/code/project/{project,target}
rm -rf hw3/code/target
find hw3/code/ | grep "\.class$" | xargs -n 1 rm -rf

# Copy instructions PDF and TeX file into deployment
cp writeup/homework3.pdf hw3/
cp writeup/homework3.tex hw3/

# Create tar.gz
COPYFILE_DISABLE=1 tar cvzf hw3.tar.gz hw3/

mv hw3 hw3_student_release
