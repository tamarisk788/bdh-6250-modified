package edu.gatech.cse6250.phenotyping

import java.text.SimpleDateFormat

import edu.gatech.cse6250.helper.{CSVHelper, SparkHelper}
import edu.gatech.cse6250.main.Main
import edu.gatech.cse6250.model.{Diagnostic, LabResult, Medication}
import edu.gatech.cse6250.util.LocalClusterSparkContext
import org.apache.log4j.{Level, Logger}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.scalatest.concurrent.TimeLimitedTests
import org.scalatest.time.{Seconds, Span}
import org.scalatest.{BeforeAndAfter, FunSuite, Matchers}

/**
 * @author Sungtae An <stan84@gatech.edu>.
 */
class T2dmPhenotypeTest extends FunSuite with LocalClusterSparkContext with TimeLimitedTests with BeforeAndAfter with Matchers {
  var spark: SparkSession = _

  before {
    Logger.getRootLogger.setLevel(Level.WARN)
    Logger.getLogger("org").setLevel(Level.WARN)
    spark = SparkHelper.createSparkSession(appName = "Test PheKBPhenotype")
  }

  after {
    spark.stop()
  }
  org.slf4j.LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME)
    .asInstanceOf[ch.qos.logback.classic.Logger]
    .setLevel(ch.qos.logback.classic.Level.WARN)
  val timeLimit = Span(600, Seconds)

  test("phenotyping with your data loader") {
    val sc = spark.sparkContext

    /** Ground truth data */
    val truePhenotypeLabel = sc.textFile("data/phenotypeLabels.csv")
      .map(line => line.split(","))
      .map(row => (row(0), row(1).toInt))

    val trueCasePatients = truePhenotypeLabel.filter(_._2 == 1) //.map(_._1)
    val trueCtrlPatients = truePhenotypeLabel.filter(_._2 == 2) //.map(_._1)
    val trueOtherPatients = truePhenotypeLabel.filter(_._2 == 3) //.map(_._1)

    val numTrueAll = truePhenotypeLabel.count()
    val numTrueCase = trueCasePatients.count()
    val numTrueCtrl = trueCtrlPatients.count()
    val numTrueOther = trueOtherPatients.count()

    //assert(numTrueAll == 3688)
    //assert(numTrueCase == 976)
    //assert(numTrueCtrl == 948)
    //assert(numTrueOther == 1764)

    /** initialize loading of data */
    val (medication, labResult, diagnostic) = loadRddRawData(spark)
    val phenotypeLabel = T2dmPhenotype.transform(medication, labResult, diagnostic)

    /** test size */
    val casePatients = phenotypeLabel.filter(_._2 == 1) //.map(_._1)
    val ctrlPatients = phenotypeLabel.filter(_._2 == 2) //.map(_._1)
    val otherPatients = phenotypeLabel.filter(_._2 == 3) //.map(_._1)

    val numAll = phenotypeLabel.count()
    val numCase = casePatients.count()
    val numCtrl = ctrlPatients.count()
    val numOther = otherPatients.count()

    //assert(numAll == numTrueAll)
    //assert(numCase == numTrueCase)
    //assert(numCtrl == numTrueCtrl)
    //assert(numOther == numTrueOther)

    //    val sampleCase = List("666338433-01", "870008777-01", "514007438-01")
    //    val sampleCtrl = List("571990000-01", "212003333-01", "180330000-01")
    //    val sampleOther = List("993003107-01", "446005646-01", "514001689-01")

    //    assert(casePatients.intersect(caseTest).length == 3)
    //    assert(casePatients.intersect(ctrlTest).length == 0)
    //    assert(casePatients.intersect(otherTest).length == 0)
    //
    //    assert(ctrlPatients.intersect(caseTest).length == 0)
    //    assert(ctrlPatients.intersect(ctrlTest).length == 3)
    //    assert(ctrlPatients.intersect(otherTest).length == 0)
    //
    //    assert(otherPatients.intersect(caseTest).length == 0)
    //    assert(otherPatients.intersect(ctrlTest).length == 0)
    //    assert(otherPatients.intersect(otherTest).length == 3)

    val recallCase = trueCasePatients.join(casePatients).count().toDouble / numTrueCase.toDouble
    val recallCtrl = trueCtrlPatients.join(ctrlPatients).count().toDouble / numTrueCtrl.toDouble
    val recallOther = trueOtherPatients.join(otherPatients).count().toDouble / numTrueOther.toDouble

    val scoreCase = Math.floor(recallCase * 7.0)
    val scoreCtrl = Math.floor(recallCtrl * 7.0)
    val scoreOther = Math.floor(recallOther * 7.0)

    //println(s"Recall of Case   : $recallCase Score: $scoreCase")
    //println(s"Recall of Control: $recallCtrl Score: $scoreCtrl")
    //println(s"Recall of Other  : $recallOther Score: $scoreOther")
    println(s"FOR_PARSE Q1b1\t$scoreCase\tRecall of Case: $recallCase")
    println(s"FOR_PARSE Q1b2\t$scoreCtrl\tRecall of Control: $recallCtrl")
    println(s"FOR_PARSE Q1b3\t$scoreOther\tRecall of Other: $recallOther")

    val (case_mean_true, control_mean_true, other_mean_true) = stat_calc(labResult, truePhenotypeLabel)
    //    val case_mean_true = 92.9746
    //    val control_mean_true = 86.05467
    //    val other_mean_true = 92.7916
    val (case_mean, control_mean, other_mean) = T2dmPhenotype.stat_calc(labResult, truePhenotypeLabel)

    val rela_err_case = ((case_mean - case_mean_true) / case_mean_true).abs
    val rela_err_other = ((other_mean - other_mean_true) / other_mean_true).abs
    val rela_err_control = ((control_mean - control_mean_true) / control_mean_true).abs

    var score_case_stat = 0.0
    var score_control_stat = 0.0
    var score_other_stat = 0.0

    if (rela_err_case < 0.01) {score_case_stat = 3.0}
    if (rela_err_control < 0.01) {score_control_stat = 3.0}
    if (rela_err_other < 0.01) {score_other_stat = 3.0}

    println(s"FOR_PARSE Q1c1\t$score_case_stat\tRelative Error of Case:$rela_err_case")
    println(s"FOR_PARSE Q1c2\t$score_control_stat\tRelative Error of Control:$rela_err_control")
    println(s"FOR_PARSE Q1c3\t$score_other_stat\tRelative Error of Unknown:$rela_err_other")

  }

  def loadRddRawData(spark: SparkSession): (RDD[Medication], RDD[LabResult], RDD[Diagnostic]) = {
    import spark.implicits._

    val sqlContext = spark.sqlContext

    val dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX")
    //val path = "../../../../bdh/hw/hw3/code/"
    List("data/encounter_INPUT.csv", "data/encounter_dx_INPUT.csv", "data/lab_results_INPUT.csv", "data/medication_orders_INPUT.csv")
      .foreach(CSVHelper.loadCSVAsTable(spark, _))

    val labResult = sqlContext.sql(
      """
        |SELECT Member_ID, Date_Resulted, Result_Name, Numeric_Result
        |FROM lab_results_INPUT
        |WHERE Numeric_Result IS NOT NULL and Numeric_Result<>''
      """.stripMargin)
      //.filter(r => !r(3).toString.isEmpty)
      .map(r => LabResult(r(0).toString, Main.sqlDateParser(r(1).toString), r(2).toString.trim.toLowerCase(), r(3).toString.replaceAll("[,\\s]+", "").toDouble))

    val diagnostic = sqlContext.sql(
      """
        |SELECT Member_ID, Encounter_DateTime, code
        |FROM encounter_dx_INPUT JOIN encounter_INPUT
        |ON encounter_dx_INPUT.Encounter_ID = encounter_INPUT.Encounter_ID
      """.stripMargin)
      .map(r => Diagnostic(r(0).toString, Main.sqlDateParser(r(1).toString), r(2).toString))

    val medication = sqlContext.sql(
      """
        |SELECT Member_ID, Order_Date, Drug_Name
        |FROM medication_orders_INPUT
      """.stripMargin)
      .map(r => Medication(r(0).toString, Main.sqlDateParser(r(1).toString), r(2).toString.toLowerCase().trim))

    (medication.rdd, labResult.rdd, diagnostic.rdd)
  }

  def stat_calc(labResult: RDD[LabResult], phenotypeLabel: RDD[(String, Int)]): (Double, Double, Double) = {

    val cases = phenotypeLabel.filter { case (x, t) => t == 1 }.map { case (x, t) => x }.collect.toSet
    val controls = phenotypeLabel.filter { case (x, t) => t == 2 }.map { case (x, t) => x }.collect.toSet
    val others = phenotypeLabel.filter { case (x, t) => t == 3 }.map { case (x, t) => x }.collect.toSet
    val case_mean = labResult.filter(r => r.testName.toLowerCase() == "glucose" && cases.contains(r.patientID)).map(r => r.value).mean()
    val control_mean = labResult.filter(r => r.testName.toLowerCase() == "glucose" && controls.contains(r.patientID)).map(r => r.value).mean()
    val other_mean = labResult.filter(r => r.testName.toLowerCase() == "glucose" && others.contains(r.patientID)).map(r => r.value).mean()

    (case_mean, control_mean, other_mean)
  }

  test("transform get give expected results") {
    val sqlContext = spark.sqlContext

    val (med, lab, diag) = loadRddRawData(spark)
    val rdd = T2dmPhenotype.transform(med, lab, diag)
    val cases = rdd.filter { case (x, t) => t == 1 }.map { case (x, t) => x }.collect.toSet
    val controls = rdd.filter { case (x, t) => t == 2 }.map { case (x, t) => x }.collect.toSet
    val others = rdd.filter { case (x, t) => t == 3 }.map { case (x, t) => x }.collect.toSet
    cases.size should be(427 + 255 + 294)
    controls.size should be(948)
    others.size should be(3688 - cases.size - controls.size)

  }
  test("stat_calc get give expected results") {
    val sqlContext = spark.sqlContext

    val (med, lab, diag) = loadRddRawData(spark)

    val sc = spark.sparkContext

    val case_mean_true = 92.9746
    val control_mean_true = 86.05467
    val other_mean_true = 92.7916
    /** Ground truth data */
    val truePhenotypeLabel = sc.textFile("data/phenotypeLabels.csv")
      .map(line => line.split(","))
      .map(row => (row(0), row(1).toInt))

    val (case_mean, control_mean, other_mean) = T2dmPhenotype.stat_calc(lab, truePhenotypeLabel)

    val rela_err_case = ((case_mean - case_mean_true) / case_mean_true).abs
    val rela_err_other = ((other_mean - other_mean_true) / other_mean_true).abs
    val rela_err_control = ((control_mean - control_mean_true) / control_mean_true).abs

    rela_err_case should be <= 0.01
    rela_err_other should be <= 0.01
    rela_err_control should be <= 0.01

  }
}
