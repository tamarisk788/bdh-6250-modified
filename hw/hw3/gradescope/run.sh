GRADING_DIR=/autograder/source
# GRADING_DIR=/mnt/host/Users/spark/Desktop/bdh/hw/hw3

cp -a /autograder/submission/. /autograder/source/submission/
# cp -a $GRADING_DIR/submission/. $GRADING_DIR/gradescope/submission/

RECORD_FILE=$GRADING_DIR/output
# RECORD_FILE=$GRADING_DIR/gradescope/output
# echo "BEGIN MASTER LOG..." > $RECORD_FILE

cd $GRADING_DIR/submission
# cd $GRADING_DIR/gradescope/submission

cp -r ../code/src/test/ src/test/
cp -r ../code/data/ data/
# rsync -ahq --delete ../code/src/test/ src/test/
# rsync -ahq --delete ../code/data/ data/

$GRADING_DIR/code/sbt/sbt -Dsbt.log.noformat=true test | tee output.log
scala $GRADING_DIR/parser.scala $RECORD_FILE
# $GRADING_DIR/gradescope/code/sbt/sbt -Dsbt.log.noformat=true test | tee output.log
# scala $GRADING_DIR/gradescope/parser.scala $RECORD_FILE