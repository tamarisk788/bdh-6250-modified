import unittest
import json

if __name__ == '__main__':
    in_file = '/autograder/source/output'
    out_file = '/autograder/results/results.json'
    # in_file = './output'
    # out_file = './autograder/results/results.json'

    in_txt = open(in_file, "r")
    lines = in_txt.readlines()
    record = lines[0]
    all_rows = record.split(',')

    try:
        q1a_score = float(all_rows[1])
        q1a_comments = all_rows[2]
    except:
        q1a_score = 0.0
        q1a_comments = 'No Results Found for 1.a'
    try:
        q1b1_score = float(all_rows[4])
        q1b1_comments = all_rows[5]
    except:
        q1b1_score = 0.0
        q1b1_comments = 'No Results Found for 1.b.1'
    try:
        q1b2_score = float(all_rows[7])
        q1b2_comments = all_rows[8]
    except:
        q1b2_score = 0.0
        q1b2_comments = 'No Results Found for 1.b.2'
    try:
        q1b3_score = float(all_rows[10])
        q1b3_comments = all_rows[11]
    except:
        q1b3_score = 0.0
        q1b3_comments = 'No Results Found for 1.b.3'
    try:
        q21_score = float(all_rows[13])
        q21_comments = all_rows[14]
    except:
        q21_score = 0.0
        q21_comments = 'No Results Found for 2.1'
    try:
        q22_score = float(all_rows[16])
        q22_comments = all_rows[17]
    except:
        q22_score = 0.0
        q22_comments = 'No Results Found for 2.2'

    total_score = q1a_score + q1b1_score + q1b2_score + q1b3_score + q21_score + q22_score
    output_dict = {"score": total_score, 
                "output": "Homework 3 Autograder Test Results",
                "stdout_visibility": "visible",
                "tests": [
                    {
                        "score": q1a_score,
                        "max_score": 5.0,
                        "number": "1.a",
                        "output": q1a_comments,
                        "visibility": "visible"
                    }, 
                    {
                        "score": q1b1_score,
                        "max_score": 10.0,
                        "number": "1.b.1",
                        "output": q1b1_comments,
                        "visibility": "visible"
                    },
                    {
                        "score": q1b2_score,
                        "max_score": 10.0,
                        "number": "1.b.2",
                        "output": q1b2_comments,
                        "visibility": "visible"
                    },
                    {
                        "score": q1b3_score,
                        "max_score": 10.0,
                        "number": "1.b.3",
                        "output": q1b3_comments,
                        "visibility": "visible"
                    },
                    {
                        "score": q21_score,
                        "max_score": 20.0,
                        "number": "2.1",
                        "output": q21_comments,
                        "visibility": "visible"
                    },
                    {
                        "score": q22_score,
                        "max_score": 8.0,
                        "number": "2.2",
                        "output": q22_comments,
                        "visibility": "visible"
                    }
                ]}
    with open(out_file, 'w') as fp:
        json.dump(output_dict, fp)