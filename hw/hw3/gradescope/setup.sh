# apt-get update
# apt-get install default-jdk
# apt-get install scala

# export JAVA_HOME=/usr/lib/jvm/java-1.11.0-openjdk-amd64

# wget https://archive.apache.org/dist/hadoop/core/hadoop-2.7.3/hadoop-2.7.3.tar.gz
# tar -xvf /hadoop-2.7.3-src.tar.gz

# export HADOOP_HOME=~/hadoop-2.7.3
# export HADOOP_MAPRED_HOME=$HADOOP_HOME
# export HADOOP_COMMON_HOME=$HADOOP_HOME
# export HADOOP_HDFS_HOME=$HADOOP_HOME
# export HADOOP_YARN_HOME=$HADOOP_HOME
# export HADOOP_CONF_DIR=$HADOOP_HOME/etc/hadoop
# export YARN_CONF_DIR=$HADOOP_HOME/etc/hadoop
# export PATH=$PATH:$HADOOP_HOME/bin

# wget http://d3kbcqa49mib13.cloudfront.net/spark-2.1.0-bin-hadoop2.7.tgz
# tar –xvf /spark-2.1.0-bin-hadoop2.7.tgz

# export SPARK_HOME=~/spark-2.1.0-bin-hadoop2.7
# export PATH=$PATH:$SPARK_HOME/bin

# apt-get install -y python python-pip python-dev

apt-get update

apt-get install -y python3-pip python3-dev
pip3 install -r /autograder/source/requirements.txt
# apt-get install -y python-pip python-dev
# pip install -r /autograder/source/requirements.txt

# install java
apt-get install -y openjdk-8-jdk

# install scala
wget https://downloads.lightbend.com/scala/2.11.8/scala-2.11.8.deb
dpkg -i scala-2.11.8.deb

# install sbt
wget --progress=dot:mega https://github.com/sbt/sbt/releases/download/v1.1.0/sbt-1.1.0.tgz
tar -xzf sbt-1.1.0.tgz
rm -rf sbt-1.1.0.tgz 
mv sbt /usr/lib/ 
ln -sf /usr/lib/sbt/bin/sbt /usr/bin/