package edu.gatech.cse6250.features

import edu.gatech.cse6250.model.{ Diagnostic, LabResult, Medication }
import org.apache.spark.SparkContext
import org.apache.spark.mllib.linalg.{ Vector, Vectors }
import org.apache.spark.rdd.RDD

/**
 * @author Hang Su
 */
object FeatureConstruction {

  /**
   * ((patient-id, feature-name), feature-value)
   */
  type FeatureTuple = ((String, String), Double)

  /**
   * Aggregate feature tuples from diagnostic with COUNT aggregation,
   *
   * @param diagnostic RDD of diagnostic
   * @return RDD of feature tuples
   */
  def constructDiagnosticFeatureTuple(diagnostic: RDD[Diagnostic]): RDD[FeatureTuple] = {
    /**
     * TODO implement your own code here and remove existing
     * placeholder code
     */
    diagnostic.map(t => ((t.patientID, t.code), 1.0)).reduceByKey(_ + _)
  }

  /**
   * Aggregate feature tuples from medication with COUNT aggregation,
   *
   * @param medication RDD of medication
   * @return RDD of feature tuples
   */
  def constructMedicationFeatureTuple(medication: RDD[Medication]): RDD[FeatureTuple] = {
    /**
     * TODO implement your own code here and remove existing
     * placeholder code
     */
    medication.map(t => ((t.patientID, t.medicine), 1.0)).reduceByKey(_ + _)
  }

  /**
   * Aggregate feature tuples from lab result, using AVERAGE aggregation
   *
   * @param labResult RDD of lab result
   * @return RDD of feature tuples
   */
  def constructLabFeatureTuple(labResult: RDD[LabResult]): RDD[FeatureTuple] = {
    /**
     * TODO implement your own code here and remove existing
     * placeholder code
     */
    val sum = labResult.map(t => ((t.patientID, t.testName), t.value)).reduceByKey(_ + _)
    val num = labResult.map(t => ((t.patientID, t.testName), 1.0)).reduceByKey(_ + _)
    sum.join(num).map(t => (t._1, t._2._1 / t._2._2))
  }

  /**
   * Aggregate feature tuple from diagnostics with COUNT aggregation, but use code that is
   * available in the given set only and drop all others.
   *
   * @param diagnostic   RDD of diagnostics
   * @param candiateCode set of candidate code, filter diagnostics based on this set
   * @return RDD of feature tuples
   */
  def constructDiagnosticFeatureTuple(diagnostic: RDD[Diagnostic], candiateCode: Set[String]): RDD[FeatureTuple] = {
    /**
     * TODO implement your own code here and remove existing
     * placeholder code
     */
    val fullset = diagnostic.map(t => ((t.patientID, t.code), 1.0)).reduceByKey(_ + _)
    fullset.filter(t => candiateCode.contains(t._1._2))
  }

  /**
   * Aggregate feature tuples from medication with COUNT aggregation, use medications from
   * given set only and drop all others.
   *
   * @param medication          RDD of diagnostics
   * @param candidateMedication set of candidate medication
   * @return RDD of feature tuples
   */
  def constructMedicationFeatureTuple(medication: RDD[Medication], candidateMedication: Set[String]): RDD[FeatureTuple] = {
    /**
     * TODO implement your own code here and remove existing
     * placeholder code
     */
    val fullset = medication.map(t => ((t.patientID, t.medicine), 1.0)).reduceByKey(_ + _)
    fullset.filter(t => candidateMedication.contains(t._1._2))
  }

  /**
   * Aggregate feature tuples from lab result with AVERAGE aggregation, use lab from
   * given set of lab test names only and drop all others.
   *
   * @param labResult    RDD of lab result
   * @param candidateLab set of candidate lab test name
   * @return RDD of feature tuples
   */
  def constructLabFeatureTuple(labResult: RDD[LabResult], candidateLab: Set[String]): RDD[FeatureTuple] = {
    /**
     * TODO implement your own code here and remove existing
     * placeholder code
     */
    val sum = labResult.map(t => ((t.patientID, t.testName), t.value)).reduceByKey(_ + _)
    val num = labResult.map(t => ((t.patientID, t.testName), 1.0)).reduceByKey(_ + _)
    val fullset = sum.join(num).map(t => (t._1, t._2._1 / t._2._2))
    fullset.filter(t => candidateLab.contains(t._1._2))
  }

  /**
   * Given a feature tuples RDD, construct features in vector
   * format for each patient. feature name should be mapped
   * to some index and convert to sparse feature format.
   *
   * @param sc      SparkContext to run
   * @param feature RDD of input feature tuples
   * @return
   */
  def construct(sc: SparkContext, feature: RDD[FeatureTuple]): RDD[(String, Vector)] = {

    /** save for later usage */
    feature.cache()

    val feature_names = feature.map(t => t._1._2).distinct()

    val feature_num = feature_names.count().toInt

    val feat_idx_map = feature_names.zipWithIndex

    val fat_table = feature.map(t => (t._1._2, (t._1._1, t._2))).join(feat_idx_map)

    val idxed_features = fat_table.map(t => (t._2._1._1, (t._2._2.toInt, t._2._1._2)))

    val grouped_features = idxed_features.groupByKey()

    val result = grouped_features.map(t => (t._1, Vectors.sparse(feature_num, t._2.map(r => r._1).toArray, t._2.map(r => r._2).toArray)))

    result
  }
}

