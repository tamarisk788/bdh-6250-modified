package edu.gatech.cse6250.clustering

/**
 * @author Hang Su <hangsu@gatech.edu>
 */

import breeze.linalg.{ sum, DenseMatrix => BDM, DenseVector => BDV }
import breeze.numerics._
import org.apache.spark.mllib.linalg.distributed.RowMatrix
import org.apache.spark.mllib.linalg.{ Vector, Vectors }

object NMF {
  def run(A: RowMatrix, k: Int, iters: Int, tol: Double = 1e-4): (RowMatrix, BDM[Double]) = {

    var h = BDM.rand[Double](k, A.numCols().toInt)
    var w = new RowMatrix(A.rows.map(_ => BDV.rand[Double](k)).map(fromBreeze).cache)

    val aNorm = sqrt(sum(getDenseMatrix(A).mapValues(d => d * d)))
    val wh: BDM[Double] = getDenseMatrix(w) * h
    val fNorm = sqrt(sum((wh - getDenseMatrix(A)).mapValues(d => d * d)))
    var relativeErr = fNorm / aNorm
    println(s"0,$fNorm,$relativeErr")
    var update = tol + 1.0

    var i = 0
    while (i < iters && update > tol) {
      //println("iter -> " + i)
      w.rows.cache()

      /** update H = H.* W^T^A ./ (W^T^W H)  */
      val wTw = computeWTV(w, w)

      val hDenominator = (wTw * h).toDenseMatrix.mapValues(_ + 2.0e-15)

      val hNumerator = computeWTV(w, A).toDenseMatrix

      h = h *:* hNumerator
      h = h /:/ hDenominator

      /** update W = W.* AH^T^ ./ (WHH^T^) */

      val wNumerator = multiply(A, h.t)

      val wDenominator = multiply(w, h * h.t)

      val newW = dotDiv(dotProd(w, wNumerator), wDenominator)
      val _ = newW.rows.cache.count

      val wh: BDM[Double] = getDenseMatrix(newW) * h
      val fNorm = sqrt(sum((wh - getDenseMatrix(A)).mapValues(d => d * d)))
      val newRelativeErr = fNorm / aNorm

      update = abs(relativeErr - newRelativeErr)
      relativeErr = newRelativeErr

      println(s"[$i,$fNorm,$relativeErr,$update]")

      w.rows.unpersist(false)
      w = newW

      i = i + 1
    }
    (w, h)
  }

  def multiply(X: RowMatrix, d: BDM[Double]): RowMatrix = {
    val scD = X.rows.sparkContext.broadcast(d)
    val rows = X.rows.mapPartitions { iter =>
      val d = scD.value
      iter.map { v: Vector =>
        val m = new BDM(1, v.size, v.toArray)
        val tmp: BDM[Double] = m * d
        Vectors.dense(tmp.toArray)
      }
    }
    new RowMatrix(rows)
  }

  def getDenseMatrix(X: RowMatrix): BDM[Double] = {
    val m = X.numRows().toInt
    val n = X.numCols().toInt
    val mat = BDM.zeros[Double](m, n)
    var i = 0
    X.rows.collect().foreach { vector =>
      vector.toArray.zipWithIndex.foreach {
        case (v, j) =>
          mat(i, j) = v
      }
      i += 1
    }
    mat
  }

  def computeWTV(W: RowMatrix, V: RowMatrix): BDM[Double] = {
    val nCol = V.numCols().toInt
    val nRow = W.numCols().toInt

    W.rows.zip(V.rows).map {
      case (w: Vector, v: Vector) =>
        val w1 = new BDM(nRow, 1, w.toArray)
        val v1 = new BDM(1, nCol, v.toArray)
        val tmp: BDM[Double] = w1 * v1
        tmp
    }.reduce(_ + _)
  }

  def dotProd(X: RowMatrix, Y: RowMatrix): RowMatrix = {
    val rows = X.rows.zip(Y.rows).map {
      case (v1: Vector, v2: Vector) =>
        toBreezeVector(v1) *:* toBreezeVector(v2)
    }.map(fromBreeze)
    new RowMatrix(rows)
  }

  def dotDiv(X: RowMatrix, Y: RowMatrix): RowMatrix = {
    val rows = X.rows.zip(Y.rows).map {
      case (v1: Vector, v2: Vector) =>
        toBreezeVector(v1) /:/ toBreezeVector(v2).mapValues(_ + 2.0e-15)
    }.map(fromBreeze)
    new RowMatrix(rows)
  }
}
