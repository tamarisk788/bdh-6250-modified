package edu.gatech.cse6250.phenotyping

import edu.gatech.cse6250.model.{ Diagnostic, LabResult, Medication }
import org.apache.spark.rdd.RDD

/**
 * @author Hang Su <hangsu@gatech.edu>,
 * @author Sungtae An <stan84@gatech.edu>,
 */
object T2dmPhenotype {
  /**
   * Transform given data set to a RDD of patients and corresponding phenotype
   *
   * @param medication medication RDD
   * @param labResult  lab result RDD
   * @param diagnostic diagnostic code RDD
   * @return tuple in the format of (patient-ID, label). label = 1 if the patient is case, label = 2 if control, 3 otherwise
   */
  def transform(medication: RDD[Medication], labResult: RDD[LabResult], diagnostic: RDD[Diagnostic]): RDD[(String, Int)] = {

    /**
     * Remove the place holder and implement your code here,
     * hard code the medication, lab, icd code etc for
     * phenotype as while testing your code we expect
     * your function have no side effect,
     * i.e. Do NOT read from file or write file
     */

    val sc = medication.sparkContext

    val allPatients = medication.map(med => med.patientID)
      .union(labResult.map(lab => lab.patientID))
      .union(diagnostic.map(diag => diag.patientID)).distinct()
    allPatients.cache()

    /** Hard code the criteria */
    val T1DM_DX = Set("250.01", "250.03", "250.11", "250.13", "250.21", "250.23", "250.31", "250.33", "250.41", "250.43",
      "250.51", "250.53", "250.61", "250.63", "250.71", "250.73", "250.81", "250.83", "250.91", "250.93")

    val T2DM_DX = Set("250.3", "250.32", "250.2", "250.22", "250.9", "250.92", "250.8", "250.82", "250.7", "250.72", "250.6",
      "250.62", "250.5", "250.52", "250.4", "250.42", "250.00", "250.02")

    val T1DM_MED = Set("lantus", "insulin glargine", "insulin aspart", "insulin detemir", "insulin lente", "insulin nph", "insulin reg", "insulin,ultralente")

    val T2DM_MED = Set("chlorpropamide", "diabinese", "diabanase", "diabinase", "glipizide", "glucotrol", "glucotrol xl",
      "glucatrol ", "glyburide", "micronase", "glynase", "diabetamide", "diabeta", "glimepiride", "amaryl",
      "repaglinide", "prandin", "nateglinide", "metformin", "rosiglitazone", "pioglitazone", "acarbose",
      "miglitol", "sitagliptin", "exenatide", "tolazamide", "acetohexamide", "troglitazone", "tolbutamide",
      "avandia", "actos", "actos", "glipizide")

    val had_t1_diag = diagnostic.filter(r => T1DM_DX.contains(r.code)).map(r => r.patientID).distinct()
    val no_t1_diag = allPatients.subtract(had_t1_diag)
    no_t1_diag.cache()
    println("Type 1 Dx (YES): " + had_t1_diag.count()) // 3,002
    println("Type 1 Dx  (NO): " + no_t1_diag.count()) // 686

    val had_t2_diag = diagnostic.filter(r => T2DM_DX.contains(r.code)).map(r => r.patientID).distinct()
    val no_t2_diag = allPatients.subtract(had_t2_diag)
    println("Type 2 Dx (YES): " + had_t2_diag.count()) // 1,265
    println("Type 2 Dx  (NO): " + no_t2_diag.count()) // 2,423

    val order_t1_med = medication.filter(r => T1DM_MED.contains(r.medicine)).map(r => r.patientID).distinct()
    val no_t1_med = allPatients.subtract(order_t1_med)
    println("Type 1 Rx (YES): " + order_t1_med.count()) // 1,328
    println("Type 1 Rx  (NO): " + no_t1_med.count()) // 2,360

    val order_t2_med = medication.filter(r => T2DM_MED.contains(r.medicine)).map(r => r.patientID).distinct()
    val no_t2_med = allPatients.subtract(order_t2_med)
    println("Type 2 Rx (YES): " + order_t2_med.count()) // 907
    println("Type 2 Rx  (NO): " + no_t2_med.count()) // 2,781

    val abnormal_ctrl_1 = labResult.filter(r => r.testName == "HbA1c".toLowerCase() && r.value >= 6.0).map(r => r.patientID).distinct()
    val abnormal_ctrl_2 = labResult.filter(r => r.testName == "Hemoglobin A1c".toLowerCase() && r.value >= 6.0).map(r => r.patientID).distinct()
    val abnormal_ctrl_3 = labResult.filter(r => r.testName == "Fasting Glucose".toLowerCase() && r.value >= 110).map(r => r.patientID).distinct()
    val abnormal_ctrl_4 = labResult.filter(r => r.testName == "Fasting blood glucose".toLowerCase() && r.value >= 110).map(r => r.patientID).distinct()
    val abnormal_ctrl_5 = labResult.filter(r => r.testName == "fasting plasma glucose".toLowerCase() && r.value >= 110).map(r => r.patientID).distinct()
    val abnormal_ctrl_6 = labResult.filter(r => r.testName == "Glucose".toLowerCase() && r.value > 110).map(r => r.patientID).distinct()
    val abnormal_ctrl_7 = labResult.filter(r => r.testName == "Glucose, Serum".toLowerCase() && r.value > 110).map(r => r.patientID).distinct()
    val abnormal_ctrl_all = sc.union(abnormal_ctrl_1, abnormal_ctrl_2, abnormal_ctrl_3, abnormal_ctrl_4, abnormal_ctrl_5, abnormal_ctrl_6, abnormal_ctrl_7).distinct()
    println("Abnormal Lab Value for Control: " + abnormal_ctrl_all.count()) // 1,927
    println()

    /** Find CASE Patients */
    println("Classifying CASE Patients...")

    val stage1_had_t1_diag = had_t1_diag
    val stage1_no_t1_diag = no_t1_diag
    println("Stage1 - Type 1 Dx (YES): " + stage1_had_t1_diag.count()) // 686
    println("Stage1 - Type 1 Dx (NO): " + stage1_no_t1_diag.count()) // 3,002

    val stage2_had_t2_dx = no_t1_diag.intersection(had_t2_diag)
    val stage2_no_t2_dx = no_t1_diag.intersection(no_t2_diag)
    println("Stage2 - Type 2 Dx (YES): " + stage2_had_t2_dx.count()) // 1,265
    println("Stage2 - Type 2 Dx (NO): " + stage2_no_t2_dx.count()) // 1,737

    val stage3_order_t1_med = stage2_had_t2_dx.intersection(order_t1_med)
    val stage3_no_t1_med = stage2_had_t2_dx.intersection(no_t1_med)
    println("Stage3 - Type 1 Rx (YES): " + stage3_order_t1_med.count()) // 838
    println("Stage3 - Type 1 Rx (NO): " + stage3_no_t1_med.count()) // 427

    val stage4_order_t1_med_order_t2_med = stage3_order_t1_med.intersection(order_t2_med)
    val stage4_order_t1_med_no_t2_med = stage3_order_t1_med.intersection(no_t2_med)
    println("Stage4 - Had T1 Rx and T2 RX (YES): " + stage4_order_t1_med_order_t2_med.count()) // 583
    println("Stage4 - Had T1 Rx and T2 RX (NO): " + stage4_order_t1_med_no_t2_med.count()) // 255

    val t2_med_earliest = medication.filter(r => T2DM_MED.contains(r.medicine)).map(r => (r.patientID, r.date.getTime())).reduceByKey(Math.min)
    val t1_med_earliest = medication.filter(r => T1DM_MED.contains(r.medicine)).map(r => (r.patientID, r.date.getTime())).reduceByKey(Math.min)
    val stage5_t2_med_earlier_than_t1_med = t1_med_earliest.join(t2_med_earliest).filter({ case (pid, (t1_time, t2_time)) => t2_time < t1_time }).map(t => t._1).distinct()
    println("Stage5 - T2 Rx earlier than T1 Rx: " + stage5_t2_med_earlier_than_t1_med.count()) // 294

    val case1 = stage3_no_t1_med
    println("Case 1: " + case1.count()) // 427
    case1.take(10).foreach(println)
    val case2 = stage4_order_t1_med_no_t2_med
    println("Case 2: " + case2.count()) // 255
    case2.take(10).foreach(println)
    val case3 = stage5_t2_med_earlier_than_t1_med
    println("Case 3: " + case3.count()) // 294
    case3.take(10).foreach(println)
    val casePatients = sc.union(case1, case2, case3).distinct()
    println("Final Case: " + casePatients.count()) // 976
    println()

    //======================================control==========================================================
    println("Classifying CONTROL Patients...")
    println()

    val stage1_glucose_measure = labResult.filter(t => t.testName.contains("glucose")).map(t => t.patientID).distinct()
    println("Stage1 - Glucose measure: " + stage1_glucose_measure.count()) // 1,823

    val stage2_no_abnormal_ctrl = stage1_glucose_measure.subtract(abnormal_ctrl_all)
    println("Stage3 - No Abnormal lab value: " + stage2_no_abnormal_ctrl.count()) // 953

    val DM_RELATED = Set("790.21", "790.22", "790.2", "790.29", "648.81", "648.82", "648.83", "648.84", "648", "648",
      "648.01", "648.02", "648.03", "648.04", "791.5", "277.7", "V77.1", "256.4")

    val had_dm_related_1 = diagnostic.filter(r => DM_RELATED.contains(r.code)).map(r => r.patientID).distinct()
    val had_dm_related_2 = diagnostic.filter(r => r.code.startsWith("250")).map(r => r.patientID).distinct()
    val had_dm_related_dx = had_dm_related_1.union(had_dm_related_2).distinct()
    val stage3_no_dm_dx = stage2_no_abnormal_ctrl.subtract(had_dm_related_dx)
    println("Stage3 - No DM Dx: " + stage3_no_dm_dx.count()) // 948

    val controlPatients = stage3_no_dm_dx.subtract(casePatients)
    println("Final Control: " + controlPatients.count()) // 948

    val others = allPatients.subtract(casePatients).subtract(controlPatients)

    println()
    println("Case: " + casePatients.count() + " Control: " + controlPatients.count() + " Others: " + others.count() + " All: " + allPatients.count())
    // Before Reduce: 975, 948, 1765, 3688
    // After  Reduce: 976, 948, 1764, 3688

    /** Once you find patients for each group, make them as a single RDD[(String, Int)] */
    val phenotypeLabel = casePatients.map(patientID => (patientID, 1))
      .union(controlPatients.map(patientID => (patientID, 2)))
      .union(others.map(patientID => (patientID, 3)))

    /** Return */
    phenotypeLabel
  }
}