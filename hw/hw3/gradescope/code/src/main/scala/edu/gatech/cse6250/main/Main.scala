package edu.gatech.cse6250.main

import java.text.SimpleDateFormat

import edu.gatech.cse6250.clustering.Metrics
import edu.gatech.cse6250.features.FeatureConstruction
import edu.gatech.cse6250.helper.{ CSVHelper, SparkHelper }
import edu.gatech.cse6250.model.{ Diagnostic, LabResult, Medication }
import edu.gatech.cse6250.phenotyping.T2dmPhenotype
import org.apache.spark.mllib.clustering.{ GaussianMixture, KMeans, StreamingKMeans }
import org.apache.spark.mllib.feature.StandardScaler
import org.apache.spark.mllib.linalg.{ DenseMatrix, Matrices, Vector, Vectors }
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

import scala.io.Source

/**
 * @author Hang Su <hangsu@gatech.edu>,
 * @author Yu Jing <yjing43@gatech.edu>,
 * @author Ming Liu <mliu302@gatech.edu>
 */
object Main {
  def main(args: Array[String]) {
    import org.apache.log4j.{ Level, Logger }

    Logger.getLogger("org").setLevel(Level.WARN)
    Logger.getLogger("akka").setLevel(Level.WARN)

    val spark = SparkHelper.spark
    val sc = spark.sparkContext
    //  val sqlContext = spark.sqlContext

    /** initialize loading of data */
    val (medication, labResult, diagnostic) = loadRddRawData(spark)
    val (candidateMedication, candidateLab, candidateDiagnostic) = loadLocalRawData

    /** conduct phenotyping */
    val phenotypeLabel = T2dmPhenotype.transform(medication, labResult, diagnostic)

    /** feature construction with all features */
    val featureTuples = sc.union(
      FeatureConstruction.constructDiagnosticFeatureTuple(diagnostic),
      FeatureConstruction.constructLabFeatureTuple(labResult),
      FeatureConstruction.constructMedicationFeatureTuple(medication)
    )

    // =========== USED FOR AUTO GRADING CLUSTERING GRADING =============
    // phenotypeLabel.map{ case(a,b) => s"$a\t$b" }.saveAsTextFile("data/phenotypeLabel")
    // featureTuples.map{ case((a,b),c) => s"$a\t$b\t$c" }.saveAsTextFile("data/featureTuples")
    // return
    // ==================================================================

    val rawFeatures = FeatureConstruction.construct(sc, featureTuples)

    val (kMeansPurity, gaussianMixturePurity, streamingPurity) = testClustering(phenotypeLabel, rawFeatures)
    println(f"[All feature] purity of kMeans is: $kMeansPurity%.5f")
    println(f"[All feature] purity of GMM is: $gaussianMixturePurity%.5f")
    println(f"[All feature] purity of StreamingKmeans is: $streamingPurity%.5f")
    // println(f"[All feature] purity of NMF is: $nmfPurity%.5f")

    /** feature construction with filtered features */
    val filteredFeatureTuples = sc.union(
      FeatureConstruction.constructDiagnosticFeatureTuple(diagnostic, candidateDiagnostic),
      FeatureConstruction.constructLabFeatureTuple(labResult, candidateLab),
      FeatureConstruction.constructMedicationFeatureTuple(medication, candidateMedication)
    )

    val filteredRawFeatures = FeatureConstruction.construct(sc, filteredFeatureTuples)

    val (kMeansPurity2, gaussianMixturePurity2, streamingPurity2) = testClustering(phenotypeLabel, filteredRawFeatures)
    println(f"[Filtered feature] purity of kMeans is: $kMeansPurity2%.5f")
    println(f"[Filtered feature] purity of GMM is: $gaussianMixturePurity2%.5f")
    println(f"[Filtered feature] purity of StreamingKmeans is: $streamingPurity2%.5f")
    // println(f"[Filtered feature] purity of NMF is: $nmfPurity2%.5f")
  }

  def testClustering(phenotypeLabel: RDD[(String, Int)], rawFeatures: RDD[(String, Vector)]): (Double, Double, Double) = {
    import org.apache.spark.mllib.linalg.Matrix
    import org.apache.spark.mllib.linalg.distributed.RowMatrix

    println("phenotypeLabel: " + phenotypeLabel.count)
    /** scale features */
    val scaler = new StandardScaler(withMean = true, withStd = true).fit(rawFeatures.map(_._2))
    val features = rawFeatures.map({ case (patientID, featureVector) => (patientID, scaler.transform(Vectors.dense(featureVector.toArray))) })
    println("features: " + features.count)
    val rawFeatureVectors = features.map(_._2).cache()
    println("rawFeatureVectors: " + rawFeatureVectors.count)

    /** reduce dimension */
    val mat: RowMatrix = new RowMatrix(rawFeatureVectors)
    val pc: Matrix = mat.computePrincipalComponents(10) // Principal components are stored in a local dense matrix.
    val featureVectors = mat.multiply(pc).rows

    val densePc = Matrices.dense(pc.numRows, pc.numCols, pc.toArray).asInstanceOf[DenseMatrix]

    def transform(feature: Vector): Vector = {
      val scaled = scaler.transform(Vectors.dense(feature.toArray))
      Vectors.dense(Matrices.dense(1, scaled.size, scaled.toArray).multiply(densePc).toArray)
    }

    /** let's train a k-means model from mllib */
    val kMeansModel = KMeans.train(featureVectors.cache(), 3, 20, "k-means||", 6250L)
    val kMeansClusterAssignmentAndLabel = features.join(phenotypeLabel).map({
      case (patientID, (feature, realClass)) =>
        (kMeansModel.predict(transform(feature)), realClass)
    })
    val kMeansPurity = Metrics.purity(kMeansClusterAssignmentAndLabel)

    /** let's train a gmm model from mllib */
    val gaussianMixtureModel = new GaussianMixture().setK(3).setSeed(6250L).run(featureVectors)
    val rddOfVectors = features.join(phenotypeLabel).map({ case (patientID, (feature, realClass)) => transform(feature) })
    val labels = features.join(phenotypeLabel).map({ case (patientID, (feature, realClass)) => realClass })
    val gaussianMixtureClusterAssignmentAndLabel = gaussianMixtureModel.predict(rddOfVectors).zip(labels)
    //val gaussianMixtureClusterAssignmentAndLabel = features.join(phenotypeLabel).map({ case (patientID, (feature, realClass)) => (gaussianMixtureModel.predict(feature), realClass)})
    val gaussianMixturePurity = Metrics.purity(gaussianMixtureClusterAssignmentAndLabel)

    /** let's train a streamingKmeans model from mllib */
    val streamingKmeansModel = new StreamingKMeans().setK(3).setDecayFactor(1.0).setRandomCenters(10, 0.5, 6250L).latestModel().update(featureVectors, 1.0, "points")
    val streamingClusterAssignmentAndLabel = features.join(phenotypeLabel).map({ case (patientID, (feature, realClass)) => (streamingKmeansModel.predict(transform(feature)), realClass) })
    val streamingPurity = Metrics.purity(streamingClusterAssignmentAndLabel)

    // /** NMF */
    // val (w, _) = NMF.run(new RowMatrix(rawFeatureVectors), 3, 200)
    // val assignments = w.rows.map(_.toArray.zipWithIndex.maxBy(_._1)._2)
    // val nmfClusterAssignmentAndLabel = assignments.zipWithIndex().map(_.swap).join(labels.zipWithIndex().map(_.swap)).map(_._2)
    // val nmfPurity = Metrics.purity(nmfClusterAssignmentAndLabel)

    (kMeansPurity, gaussianMixturePurity, streamingPurity)
  }

  /**
   * load the sets of string for filtering of medication
   * lab result and diagnostics
   *
   * @return
   */
  def loadLocalRawData: (Set[String], Set[String], Set[String]) = {
    val candidateMedication = Source.fromFile("data/med_filter.txt").getLines().map(_.toLowerCase).toSet[String]
    val candidateLab = Source.fromFile("data/lab_filter.txt").getLines().map(_.toLowerCase).toSet[String]
    val candidateDiagnostic = Source.fromFile("data/icd9_filter.txt").getLines().map(_.toLowerCase).toSet[String]
    (candidateMedication, candidateLab, candidateDiagnostic)
  }

  def sqlDateParser(input: String, pattern: String = "yyyy-MM-dd'T'HH:mm:ssX"): java.sql.Date = {
    val dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX")
    new java.sql.Date(dateFormat.parse(input).getTime)
  }

  def loadRddRawData(spark: SparkSession): (RDD[Medication], RDD[LabResult], RDD[Diagnostic]) = {
    import spark.implicits._
    val sqlContext = spark.sqlContext

    List("data/encounter_INPUT.csv", "data/encounter_dx_INPUT.csv", "data/lab_results_INPUT.csv", "data/medication_orders_INPUT.csv")
      .foreach(CSVHelper.loadCSVAsTable(spark, _))

    val labResult = sqlContext.sql(
      """
        |SELECT Member_ID, Date_Resulted, Result_Name, Numeric_Result
        |FROM lab_results_INPUT
        |WHERE Numeric_Result IS NOT NULL and Numeric_Result<>''
      """.stripMargin)
      //.filter(r => !r(3).toString.isEmpty)
      .map(r => LabResult(
        r(0).toString,
        sqlDateParser(r(1).toString),
        r(2).toString.trim.toLowerCase(),
        r(3).toString.replaceAll("[,\\s]+", "").toDouble))

    val diagnostic = sqlContext.sql(
      """
        |SELECT Member_ID, Encounter_DateTime, code
        |FROM encounter_dx_INPUT JOIN encounter_INPUT
        |ON encounter_dx_INPUT.Encounter_ID = encounter_INPUT.Encounter_ID
      """.stripMargin)
      .map(r => Diagnostic(r(0).toString, sqlDateParser(r(1).toString), r(2).toString))

    val medication = sqlContext.sql(
      """
        |SELECT Member_ID, Order_Date, Drug_Name
        |FROM medication_orders_INPUT
      """.stripMargin)
      .map(r => Medication(r(0).toString, sqlDateParser(r(1).toString), r(2).toString.toLowerCase().trim))

    (medication.rdd, labResult.rdd, diagnostic.rdd)
  }

}
