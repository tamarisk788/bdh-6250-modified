import java.io.{File, PrintWriter, FileOutputStream}

val input = io.Source.fromFile("output.log")
val lines = input.getLines.toList.filter(_.startsWith("FOR_PARSE")).map(_.drop(10)).filter(_.nonEmpty)
    .map(_.split("\t"))
    .sortBy(_(0))

val path = args(0)

val pw = new PrintWriter(new FileOutputStream(new File(path), true))

pw.append(lines.map(_.mkString(",")).mkString(","))
pw.close()