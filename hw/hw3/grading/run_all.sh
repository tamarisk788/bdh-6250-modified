#!/bin/bash
# run_all.sh
#
# Grades all submissions located in the current folder
#  - Should contain student archives or already extracted folders

# CONFIGURATION (change this to match your setup)
GRADING_DIR=/bdh/hw/hw3
echo $GRADING_DIR

# Get current directory and push to top of directory stack
DIR=$(pwd)

echo "PROCESSING DIRECTORY: $DIR"
echo "Delete ALL folders in directory and re-untar student submissions?"
echo " --> rm -Rf -- $DIR/*/"
select yn in "Yes" "No"; do
    case $yn in
        Yes )
            echo "Clearing ALL Folders"
            rm -Rf -- */
            echo "Un-taring all archives in directory"
            # tar*.gz handles Canvas resubmit naming, e.g. tar-2.gz
            ls *.tar*.gz | xargs -i tar -zxf {}
            break;;
        No )
            echo "Leaving ALL Folders"
            break;;
    esac
done

# Create new log file
RECORD_FILE=$DIR/MASTER_RECORD.log
echo "BEGIN MASTER LOG..." > $RECORD_FILE

# Loop through and grade every student directory
for STUDENT_DIR in */; do
    #$GRADING_DIR/run_one.sh $STUDENT_DIR
    echo "Grading: ${STUDENT_DIR}"
    STUDENT_GTID=$(echo "$STUDENT_DIR" | cut -d '-' -f 1)
    STUDENT_USERNAME=$(echo "$STUDENT_DIR" | cut -d '-' -f 2)
    pushd $STUDENT_DIR
        rsync -ahq --delete $GRADING_DIR/code/src/test/ src/test/
        rsync -ahq --delete $GRADING_DIR/code/data/ data/
        $GRADING_DIR/code/sbt/sbt -Dsbt.log.noformat=true test | tee output.log
        scala $GRADING_DIR/grading/parser.scala $RECORD_FILE $STUDENT_GTID $STUDENT_USERNAME
        #rm -rf data/
    popd
    # Output student results to the master record
    #cat $STUDENT_DIR/output.log >> $RECORD_FILE
done

exit 0

