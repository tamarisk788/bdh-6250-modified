#!/bin/bash

# Get current directory and push to top of directory stack
DIR=$(pwd)
pushd $DIR

STUDENT_DIR=$1
LOG_DIR=$DIR/$STUDENT_DIR

echo "LOG DIR: $LOG_DIR"

# Create new log file
RECORD_FILE=$LOG_DIR/RUN_RECORD.log
> $RECORD_FILE

# Configuration Variables (change this to match your setup)
SCORING_ROOT=/hw2_grading
EXPECTED_OUTPUT_DIR=$SCORING_ROOT/expected_output
GRADING_DIR=/bdh/hw/hw2/grading
PYTHON=/usr/local/conda2/bin/python 

# Break apart folder name (format is $GTID-$USERNAME-$HWNUM )
STUDENT_GTID=$(echo "$STUDENT_DIR" | cut -d '-' -f 1)
STUDENT_USERNAME=$(echo "$STUDENT_DIR" | cut -d '-' -f 2)

echo "GRADING: $STUDENT_USERNAME ($STUDENT_GTID)"
pushd "$STUDENT_DIR"
if [ -d "code/" ]; then
    pushd "$SCORING_ROOT/code"

    # Clean-up files from grading directory
    rm -Rf -- pig/*/
    rm -f pig/etl.pig
    rm -f pig/etl_section*
    rm -f pig/utils.py
    rm -f lr/lrsgd.py
    rm -f lr/lrsgd_fast.py
    rm -f lr/mapper.py
    rm -f lr/testensemble.py
    rm -f map.out
    unset OUTPUT22ORIG OUTPUT22SPLIT OUTPUT23AB OUTPUT23C OUTPUT24AB
    #rm -Rf ../models_out/

    popd

    pushd "code/"

    # Copy student files to the grading directory
    cp pig/etl.pig $SCORING_ROOT/code/pig/
    cp pig/utils.py $SCORING_ROOT/code/pig/
    cp lr/lrsgd.py $SCORING_ROOT/code/lr/
    cp lr/lrsgd_fast.py $SCORING_ROOT/code/lr/
    cp lr/mapper.py $SCORING_ROOT/code/lr/
    cp lr/testensemble.py $SCORING_ROOT/code/lr/

    pushd "$SCORING_ROOT/code"


    # ---------------------------- #
    #        GRADING Q2.1          #
    # ---------------------------- #

    # TBD #

    # ---------------------------- #
    #  Q2.2: ORIGINAL SUBMISSION   #
    # ---------------------------- #
    pushd "pig"

    {
    cp $GRADING_DIR/2.2_pig/grade.py .

    # Remove any existing folders (from previous output)
    rm -Rf -- */

    # Run entire student code
    pig -x local etl.pig
    
    # Run grading on student output and store in $OUTPUT
    OUTPUT22ORIG=$($PYTHON grade.py $EXPECTED_OUTPUT_DIR/pig/)

    rm -f grade.py
    } > "$LOG_DIR/q22orig-$STUDENT_GTID-$STUDENT_USERNAME.log" 2>&1

    popd # EXIT pig/
    sleep 1

    # ---------------------------- #
    #  Q2.2: SPLIT/PIECEWISE       #
    # ---------------------------- #
    pushd "pig"

    {
    cp $GRADING_DIR/2.2_pig/grade.py .

    # Remove any existing folders (from previous output)
    rm -Rf -- */

    # Run the Pig splitting logic
    python $GRADING_DIR/2.2_pig/pig_splitter.py etl.pig $SCORING_ROOT/data/ $EXPECTED_OUTPUT_DIR/pig/

    # Run each step of the split scripts
    ls etl_section_*.pig | xargs -i pig -x local {}
    
    # Run grading on student output and store in $OUTPUT
    OUTPUT22SPLIT=$($PYTHON grade.py $EXPECTED_OUTPUT_DIR/pig/)

    rm -f grade.py
    } > "$LOG_DIR/q22split-$STUDENT_GTID-$STUDENT_USERNAME.log" 2>&1

    popd # EXIT pig/
    sleep 1

 
    # ---------------------------- #
    #   GRADING Q2.3a AND Q2.3b    #
    # ---------------------------- #
    pushd "lr"

    {
    cp $GRADING_DIR/2.3_sgd_basic/grade.py .

    OUTPUT23AB=$($PYTHON grade.py $EXPECTED_OUTPUT_DIR/pig/training/part-r-00000 $EXPECTED_OUTPUT_DIR/pig/testing/part-r-00000 $EXPECTED_OUTPUT_DIR/lr)

    rm -f grade.py
    } > "$LOG_DIR/q23ab-$STUDENT_GTID-$STUDENT_USERNAME.log" 2>&1

    popd # EXIT lr/
    sleep 1

    # ---------------------------- #
    #        GRADING Q2.3c         #
    # ---------------------------- #
    pushd "lr"

    {
    cp $GRADING_DIR/2.3_sgd_extra/grade.py .

    OUTPUT23C=$($PYTHON grade.py $SCORING_ROOT/data/fast_training.data $SCORING_ROOT/data/fast_testing.data)

    rm -f grade.py
    } > "$LOG_DIR/q23c-$STUDENT_GTID-$STUDENT_USERNAME.log" 2>&1

    popd # EXIT lr/
    sleep 1

    # ---------------------------- #
    #   GRADING Q2.4a AND Q2.4b    #
    # ---------------------------- #
    pushd "lr"

    {
    hdfs dfs -rm -r hw2/mapper

    # Run Hadoop job with identity reducer
    hadoop jar \
      /usr/lib/hadoop-mapreduce/hadoop-streaming.jar \
      -D mapreduce.job.reduces=0 \
      -files . \
      -mapper "$PYTHON mapper.py -n 3 -r 0.5 " \
      -input hw2/training \
      -output hw2/mapper

    # Combine all files in hw2/mapper to one file mapper.out
    hdfs dfs -getmerge hw2/mapper/ ../map.out

    # Replace map.out with a sorted version
    sort -o ../map.out ../map.out

    #hdfs dfs -rm -r hw2/models

    #hadoop jar \
    #  /usr/lib/hadoop-mapreduce/hadoop-streaming.jar \
    #  -D mapreduce.job.reduces=3 \
    #  -files . \
    #  -mapper "$PYTHON mapper.py -n 3 -r 0.5" \
    #  -reducer "$PYTHON reducer.py -f 3618" \
    #  -input hw2/training \
    #  -output hw2/models

    #hdfs dfs -get hw2/models ../models_out

    cp $GRADING_DIR/2.4_hadoop/grade.py .

    OUTPUT24AB=$($PYTHON grade.py ../map.out $EXPECTED_OUTPUT_DIR/lr/map.out ../models_out/ $EXPECTED_OUTPUT_DIR/pig/training/part-r-00000)

    rm -f grade.py
    } > "$LOG_DIR/q24ab-$STUDENT_GTID-$STUDENT_USERNAME.log" 2>&1

    popd # EXIT lr/
    sleep 1

    # ---------------------------- #
    #   GRADING Q2.4a AND Q2.4b    #
    # ---------------------------- #

    # Log results to the log file
    echo "$STUDENT_GTID,$STUDENT_USERNAME,$OUTPUT22ORIG,$OUTPUT22SPLIT,$OUTPUT23AB,$OUTPUT23C,$OUTPUT24AB" >> $RECORD_FILE

    popd # EXIT scoring environment
    popd # EXIT student grading folder
else
    echo "$STUDENT_GTID,$STUDENT_USERNAME,0.0,code/ folder not found" >> $RECORD_FILE
fi
popd # EXIT student folder

exit 0
