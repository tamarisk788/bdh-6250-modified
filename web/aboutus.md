---
layout: page
title: About Us
subtitle: Class instructor and teaching assistants
---
<!--[sunlab-team]({{ site.baseurl }}/img/aboutus.jpg "Sunlab team")-->

# WebEx Office Hour
We will host office hour through WebEx or bluejean **every weekday**. See below table for detailed schedule

| Photo| Name|Time (EST)             | Location or Web Link |
| :-------------: | :-------------: | ---------------- | --------------------------------------------------------------------------------------|
|![minipic]({{ site.baseurl }}/img/Jimeng.png)   |  Jimeng Sun, instructor jsun<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;cc.gatech.edu     |      |       Request by email         |
|![minipic]({{ site.baseurl }}/img/andys.png )| Andy Soobrian, TA asoobian3<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| Mon 6PM | [https://gatech.webex.com/gatech/j.php?MTID=m5b49fe73fec154663176adf45f7b6afd](https://gatech.webex.com/gatech/j.php?MTID=m5b49fe73fec154663176adf45f7b6afd)
|![minipic]({{ site.baseurl }}/img/MingLiu.jpg) | Ming Liu , PhD,  TA mliu302<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| Mon 11AM |Open space next to Klaus 1332
|![minipic]({{ site.baseurl }}/img/ed.jpg) | Edward Choi,  Sunlab PhD,  TA echoi48<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| Tue 2PM | Open space next to Klaus 1332
|![minipic]({{ site.baseurl }}/img/Patrick.jpeg) | Patrick Manion,  TA pmanion<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| Wed 8PM | [https://gatech.webex.com/meet/pmanion3](https://gatech.webex.com/meet/pmanion3)
|![minipic]({{ site.baseurl }}/img/Balaji.png) |     Balaji Sundaresan, TA bsundaresan3<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu | Wed 9PM | [https://gatech.webex.com/meet/bsundaresan3](https://gatech.webex.com/meet/bsundaresan3)
|![minipic]({{ site.baseurl }}/img/kimis.png) | Ioakeim (Kimis) Perros,  TA perros<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| Wed 10AM | Klaus 1211
|![minipic]({{ site.baseurl }}/img/SungtaeAn.jpg) | Sungtae An, Sunlab PhD,  TA  stan84<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| Thu 2PM | Klaus 1211
|![minipic]({{ site.baseurl }}/img/YuJing.jpg) | Yu Jing,  TA yujing<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| Fri 3PM | Klaus 1211



