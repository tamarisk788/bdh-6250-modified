---
layout: page
title: Course Syllabus
---
# Overview
<!--Both on-campus and OMS student should watch  Udacity course videos. On-campus student should watch video before class. Deliverable due dates apply to both OMS and on-campus student.-->
Students should watch Udacity course videos according to the following schedule. It is **recommended** for students to do lab sessions on the schedule by yourself **as early as possible** since some of homework may cover the lab materials scheduled later than the homework.

# Schedule

<!-- **Guest lecture by Noemie Elhadad (Columbia)**   -->

| Week # | Dates     | In-class lesson                                                     | Video lessons                       | Lab                              | Deliverable Due                                                          | 
|--------|-----------|---------------------------------------------------------------------|-------------------------------------|----------------------------------|--------------------------------------------------------------------------| 
| 1      | 1/9/2018  | Intro to the BDH class                                              | [1. Intro to Big Data Analytics](https://www.udacity.com/course/viewer#!/c-ud758/l-6311012028)      |                                  |                                                                          | 
| 1      | 1/11/2018 | Sunlab's research by Jimeng Sun                                     | [2. Course Overview](https://www.udacity.com/course/viewer#!/c-ud758/l-5046828066)                  | [Scala Basic](/teaching/cse6250/spring2018/lab/scala-basic/)                      |                                                                          | 
| 2      | 1/16/2018 | Deep learning for healthcare by Edward Choi                         | [3. Predictive Modeling](https://www.udacity.com/course/viewer#!/c-ud758/l-5484251492)              |                                  |                                                                          | 
| 2      | 1/18/2018 | **Guest lecture by Mark Braunstein**                                    |                                     |                                  |                                                                          | 
| 3      | 1/23/2018 | Deep learning for healthcare by Edward Choi - Cont.                 | [4.MapReduce](https://www.udacity.com/course/viewer#!/c-ud758/l-6298155413)& [HBase](/teaching/cse6250/spring2018/lab/hadoop-hbase/)                  |                                  | HW1 (1/21/2018)                                                          | 
| 3      | 1/25/2018 |                                                                     |                                     | [Hadoop & HDFS Basics](/teaching/cse6250/spring2018/lab/hdfs-basic/)             |                                                                          | 
| 4      | 1/30/2018 | **Guest lecture by Chunhua Wen (Columbia)**                             | [5.Classification evaluation metrics](https://www.udacity.com/course/viewer#!/c-ud758/l-5505090946) |                                  |                                                                          | 
| 4      | 2/1/2018  |                                                                     | [6.Classification ensemble methods](https://www.udacity.com/course/viewer#!/c-ud758/l-5615268587)   | [Hadoop Pig](/teaching/cse6250/spring2018/lab/hadoop-pig/) & [Hive](/teaching/cse6250/spring2018/lab/hadoop-hive/)                |                                                                          | 
| 5      | 2/6/2018  | **Guest lecture by Jon Duke**                                           | [7. Phenotyping](https://www.udacity.com/course/viewer#!/c-ud758/l-6363218753)                      |                                  |                                                                          | 
| 5      | 2/8/2018  |                                                                     | [8. Clustering](https://www.udacity.com/course/viewer#!/c-ud758/l-6343118554)                       | "[Spark Basic](/teaching/cse6250/spring2018/lab/spark-basic/), [Spark SQL](/teaching/cse6250/spring2018/lab/spark-sql/) "         | HW2 (2/11/2018)                                                          | 
| 6      | 2/13/2018 | **Guest lecture by Rachel Patzer (Emory)**                              | [9. Spark](https://www.udacity.com/course/viewer#!/c-ud758/l-6376189383/m-6861062716)                            |                                  |                                                                          | 
| 6      | 2/15/2018 |                                                                     |                                     | [Spark Application](/teaching/cse6250/spring2018/lab/spark-application/) & [Spark MLlib](/teaching/cse6250/spring2018/lab/spark-mllib/)  |                                                                          | 
| 7      | 2/20/2018 | Computational phenotyping with tensor factorization by Kimis Perros (I)     | [10. Medical ontology](https://www.udacity.com/course/viewer#!/c-ud758/l-6370309670)                 |                                  |                                                                          | 
| 7      | 2/22/2018 |  |                                     |  [NLP Lab by Charity Hilton](/teaching/cse6250/spring2018/lab/nlp/)      | HW3 (2/25/2018)                                                          | 
| 8      | 2/27/2018 | **Guest lecture by David Page (UW Madison)**                            | [11. Graph analysis](https://www.udacity.com/course/viewer#!/c-ud758/l-6374209610/m-6842807731)                  |                                  |                                                                          | 
| 8      | 3/1/2018  |                                                                     |                                     | [Spark GraphX](/teaching/cse6250/spring2018/lab/spark-graphx/)                     | Project Group Formation (3/1/2018)                                       | 
| 9      | 3/6/2018  | **Guest lecture by Jim Rehg**                                           | [12. Dimensionality Reduction](https://www.udacity.com/course/viewer#!/c-ud758/l-6334098665)        |                                  |                                                                          | 
| 9      | 3/8/2018  |                                                                     |                                     | [Deep Learning Lab by Sungtae An](teaching/cse6250/spring2018/lab/dl-setup/)  | Project Proposal (3/11/2018)                                             | 
| 10     | 3/13/2018 |                                                                | [13. Patient similairty](https://www.udacity.com/course/viewer#!/c-ud758/l-6375269344/m-6857168643)              | [Deep Learning Lab by Sungtae An - Cont.](http://www.sunlab.org/teaching/cse6250/spring2018/lab/dl-setup/)    |                                                                          | 
| 10     | 3/15/2018 |                                                                     |                                     |         | HW4 (3/18/2018)                                                          | 
| 11     | 3/20/2018 | Spring break                                                        |                                     |                                  |                                                                          | 
| 11     | 3/22/2018 | Spring break                                                        |                                     |                                  | | 
| 12     | 3/27/2018 | Project Discussion                                                  |                                     |                                  |Peer Review Bidding (3/27/2018) | 
| 12     | 3/29/2018 | Project Discussion                                                  |                                     |                                  | Project Draft (4/1/2018)                                                 | 
| 13     | 4/3/2018  | Computational phenotyping with tensor factorization by Kimis Perros - Cont.   |                                     |                                  |                                                                          | 
| 13     | 4/5/2018  | **Guest lecture by Greg Cooper (UPitt)**                                |                                     |                                  | Peer Review on Draft (4/8/2018)                                          | 
| 14     | 4/10/2018 | **Guest**                                                               |                                     |                                  |                                                                          | 
| 14     | 4/12/2018 | **Guest lecture: S. Joshua Swamidass (Wash U.)**                        |                                     |                                  |                                                                          | 
| 15     | 4/17/2018 |                                                                |                                     |                                  |                                                                          | 
| 15     | 4/19/2018 | **Guest lecture by Walter “Buzz” Stewart (Sutter Health)**    |                                     |                                  |                                                                          | 
| 16     | 4/24/2018 |                                                                |                                     |                                  |                                                                          | 
| 16     | 4/26/2018 |                                                                     |                                     |                                  | Final Project with code, presentation, and the final paper (4/24/2018) | 



# Previous Guest Lectures

See [RESOURCE]({{ site.baseurl }}/resource) section.
