# How to deploy the project

### Introduction

This is a [jekyll](https://jekyllrb.com/) project, you may install this application via command:

```
gem install jekyll bundler redcarpet
```

I use "spring2018" as example in this doc, but you may change this name to `spring2018` or some name else.

## Update Content

#### 1. Update Sub-directory lab

If you want to update [http://www.sunlab.org/teaching/cse6250/spring2018/lab/](http://www.sunlab.org/teaching/cse6250/spring2018/lab/), 
you should edit web directory in [realsunlab/bigdata-bootcamp](https://bitbucket.org/realsunlab/bigdata-bootcamp/src/) first.

After finish editing, you should run 

```
jekyll build
```

in web, which will generate html files in `_site`.

and copy the content from `_site` to `web/lab/` in [realsunlab/bdh](https://bitbucket.org/realsunlab/bdh/src).

If you want to test out the site locally, you can run 
```
jekyll serve
```
Make sure _config.yml in both [realsunlab/bdh](https://bitbucket.org/realsunlab/bdh/src) and the one in [realsunlab/bigdata-bootcamp](https://bitbucket.org/realsunlab/bigdata-bootcamp/src/) 
refer to the same url and baseurl, e.g., 
```
url: "http://www.sunlab.org"
baseurl: "/teaching/cse6250/spring2018/lab"
```

#### 2. Update Root directory for the course

Edit content in 'web' in [realsunlab/bdh](https://bitbucket.org/realsunlab/bdh/src), and run

```
jekyll build
```

to generate html files.

upload the content in `_site` to `jimeng@chemawa.dreamhost.com`

You may execute `./deploy.sh` in `_site` directory directly, which equals execute command

```
#rsync -urltv --delete -e ssh _site/ jimeng@chemawa.dreamhost.com:~/sunlab.org/teaching/cse8803/spring2018/
rsync -urltv --delete -e ssh _site/ jimeng@chemawa.dreamhost.com:~/sunlab.org/teaching/cse6250/spring2018/
```

