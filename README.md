# TA GUIDE FOR CSE6250 #

### What is this repository for? ###

* Course website
* Homework
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Lecture ###

* Lecture should always be recorded via http://screencast-o-matic.com/cse6250
    * Make sure start your video via the above link otherwise the free version will have  15min limit per video
    * PW for access is bdh
* Note that only 10 machines can be used per month due to the license constraint
    * This videos should be recorded by oncampus TA
    * THe videos should be hosted on youtube channel

### Contribution guidelines ###

* Document everything and create frequent pull requests
    * Every TA should frequently create their branch and then merge into the master (via pull requests), e.g. each TA should create a branch for a single homework and merge into the master before homework release.

### Who do I talk to? ###

* Your fellow TAs via slack
* Previous TAs listed in the previous course site
* Instructor
* Your best friend Google